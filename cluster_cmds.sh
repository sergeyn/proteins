setenv LD_LIBRARY_PATH  /shared/home/snepomny/blast/

#for concurrency do: 
echo "test.sh [arg]" | qsub -l walltime=99:01:00,mem=6gb #for 99 hours of running with 6gb:)
node-01, node-02 ...

#to get CA seq data
getSeqFromHhpGuysPdbs.rb  

#get CA related xyzng data then 
getXYZpadFromHhpGuysPdbs.rb 

#deprecated?
#ruby /shared/home/snepomny/newScripts/atomsParsing.rb /shared/home/snepomny/entDomains/d12asa_.ent 

#cat *.fasta > unionoffasta
#build db from union of fasta
formatdb  -t master -i <sourceFasta> -p T -V -n sync_master <nameForDb>

#run blast on seqs
blastall -p blastp -d  blastdb_sync_master -i $1 -e 0.1 -m 0 -o $1.blast -F F

#build PSI profiles from seqs and nrs db
blastpgp -i $1 -d '/shared/home/snepomny/runs/nr/nr.00 /shared/home/snepomny/runs/nr/nr.01 /shared/home/snepomny/runs/nr/nr.02 /shared/home/snepomny/runs/nr/nr.03'  -j 6 -h 0.0001 -u 1 -J T -C $1.prf

#run psi-blast with seq,profile and db of seqs
blastpgp -i $1 -d blastdb_sync_master -h 0.0001 -q 1 -R $1.prf -o $1.psi

ruby -e "require 'scripts_rb/hhpred_lib.rb'; parse_hhp(ARGV[0])" $1 > $1.flat
ruby newHHMRebuildAlignment.rb runs/hhrs_new_flat/1ry9_A.hhr.flat

#run hhsearch
/shared/home/snepomny/hhpredNew/bin/hhsearch -i $1 -d /shared/home/snepomny/hhmDomainsDB -b 30123 -nocons -nopred -nodssp

#hhp parsing
ruby -e "require 'hhpred_lib.rb'; parse_hhp(ARGV[0])" $1 > $1.flat

#tmscore blast/psi - input flat file
ruby newBlastRmsd_lib.rb $1

#tmscore hhm flat file
ruby newHHMRebuildAlignment.rb $1

#run gesamt
/shared/home/snepomny/gesamt_1.2/bin/gesamt /shared/home/snepomny/entDomains/d1daba_.ent /shared/home/snepomny/entDomains/d12asa_.ent

