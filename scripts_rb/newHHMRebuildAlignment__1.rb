#flat:
#-82.462,-2.374,89.540,SER,1

kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'
require kernel_path+'log_lib'
require kernel_path+'localEnv.rb'
require kernel_path+'utils'
require kernel_path+'blosum'
require kernel_path+'astrals'

$xyzng = '/'
$tmp = '/tmp/'
$tmscore =  '/shared/home/snepomny/TMscore_32'
$query_hash = {}
$query_aln
XYZR_SPLITTER = ' ' 

$query_hash = {}
$query_aln = '/'

#-------------------------------------------------------------------
def buildXYZgHash(fname)
  raise fname unless File.exist? fname

	hsh = {}
	File.new(fname).each { |l| 
		arr = l.split(XYZR_SPLITTER)
		hsh[arr[-1].to_i] = arr[0..-2]
	}
	raise fname unless hsh.size
	return hsh
end
#-------------------------------------------------------------------
def daaaA_to_dAAAA(name)
	return name[0..3].upcase + name[4..-1]
end
#-------------------------------------------------------------------
def loadAln(name)
	raise fname unless File.exist? name
	ret = File.new(name).each.map { |l| l.to_i }
#	puts ret.join ' '
	ret
end
#-------------------------------------------------------------------
def handleSingleAlignmentLine(line)
	arr = line.split($flatdelim)
	eval = arr[2].to_f
	if(eval>1.0) 
		puts eval
		return
	end
#	puts arr[0..1].join(',')
	#begin
	#pessimization for rand stuff:
	$query = arr[0]
	$query_hash = buildXYZgHash($xyzng+daaaA_to_dAAAA(arr[0])+'.ent.xyzr')
	$query_aln = loadAln($xyzng+daaaA_to_dAAAA(arr[0])+'.ent.aln')
	hsh_left = (arr[0]==$query) ? $query_hash : buildXYZgHash($xyzng+daaaA_to_dAAAA(arr[0])+'.ent.xyzr')
	hsh_right = (arr[1]==$query) ? $query_hash : buildXYZgHash($xyzng+daaaA_to_dAAAA(arr[1])+'.ent.xyzr')
	return if hsh_left == nil or hsh_right == nil
	aln_left = (arr[0]==$query) ? $query_aln : loadAln($xyzng+daaaA_to_dAAAA(arr[0])+'.ent.aln')
	aln_right = (arr[1]==$query) ? $query_aln : loadAln($xyzng+daaaA_to_dAAAA(arr[1])+'.ent.aln')
	return if aln_right==nil or aln_left==nil
	#rescue
	#	STDERR.puts "skipping due to exception: #{arr[0..1].join(',')}" #unless arr[0].match(/^d/) or arr[1].match(/^d/)
	#	return
	#end
	offset_left = arr[7].to_i 
	offset_right = arr[9].to_i 
	seq_l = arr[11].chomp.split('')
	seq_r= arr[12].chomp.split('')
	eval = arr[2].to_f
	return if seq_l.size < 35
	xyz_l = []
	xyz_r = []
	new_seq_l = []
	new_seq_r = []
	throw 'alignment length is wrong' unless seq_l.size == seq_r.size

	(0...seq_l.size).each { |i|
		dataLeft = xyzForResidue(offset_left,hsh_left,aln_left)
		dataRight = xyzForResidue(offset_right,hsh_right,aln_right)
		if(dataLeft and dataRight) #had both cas
			if(seq_l[i]!='-' and seq_r[i]!='-')
				#throw "at left  #{i}/#{offset_left}: #{arr[0..1].join(',')} |#{seq_l[i]}|!= |#{$astrals[dataLeft[-1]]}|"  
				return if seq_l[i]!= $astrals[dataLeft[-1]] and seq_l[i]!='X'
				#throw "at right #{i}/#{offset_right}: #{arr[0..1].join(',')}  |#{seq_r[i]}| != |#{$astrals[dataRight[-1]]}| "  
				return if seq_r[i]!= $astrals[dataRight[-1]] and seq_r[i]!='X'
				xyz_l << dataLeft #hsh_left[offset_left]
				xyz_r << dataRight #hsh_right[offset_right]
			end
			new_seq_l << seq_l[i]
			new_seq_r << seq_r[i]
		end
		offset_left+=1 if(seq_l[i]!='-')
		offset_right+=1 if(seq_r[i]!='-')
	}

	#recalculate stats for new alignment
	length,ident,similar,gaps = getAlignmentMetrics(new_seq_l,new_seq_r)
	news = [ length, (ident.to_f*100.0/length.to_f).round, (similar.to_f*100.0/length.to_f).round, gaps]
	#store xyzs in tmp files
	prot1 = $tmp + arr[0..1].join('_') + '_left'
	prot2 = $tmp + arr[0..1].join('_') + '_right' 
	tmpxyzl = File.new(prot1,'w')
	tmpxyzr = File.new(prot2,'w')
	(1..xyz_l.size).each { |i| 
		tmpxyzl.puts form_legal_tmscore_line(i,xyz_l[i-1]) 
		tmpxyzr.puts form_legal_tmscore_line(i,xyz_r[i-1])
	}
	tmpxyzl.close
	tmpxyzr.close
	#get tmscore data
	#puts "#{$tmscore} #{prot1} #{prot2}"
	tm = parse_tmscore_lines(%x[#{$tmscore} #{prot1} #{prot2} | grep -e RMSD -e '-score' | grep -v RMSD= | grep -v predict | cut -d'=' -f2 ]) 
	%x[rm -f #{prot1} #{prot2}]
	#write new alignment,stats and tmscore data to result file
	return line.chomp + " " + news.join(" ") + " " + tm
end



#==============================================================================
if __FILE__ == $0
  #flat name:
  fname=ARGV[0] #'/home/snepomny/rsync/hhm_res_july12/1b35C.hhr.flat'
  $query = fname.split('/')[-1].split('.hhr')[0] 

  #$query_hash =  buildXYZgHash($xyzng+daaaA_to_dAAAA($query)+'.xyzng')
  
  #ofname = File.new(fname+'.tmscore','w')
  #File.new(fname).each { |line|
  # out = handleSingleAlignmentLine(line) rescue "ERROR #{line}" 
  # ofname.puts out
  #}
  #ofname.close

  #File.new(fname).each { |line|
#	#line = '1b35C,3napC,0,262,32,0.519,0,1,266,1,277,SKPTVQGKIGECKLRGQGRMANFDGMDMSHKMALSSTNEIETNEGLAGTSLDVMDLSRVLSIPNYWDRFTWKTSDVINTVLWDNYVSPFKVKP-YSATITDRFRCTHMGKVANAFTYWRGSMVYTFKFVKTQYHSGRLRISFIPYYYNTTISTGT---PDVSRTQKIVVDLRTSTAVSFTVPYIGSRPWLYCI----RP---ESSWLSKDN---TDGALMYNCVSGIVRVEVLNQLVAAQN-VFSEIDVICEVNGGPDLEFAGPTCPRYVPYAGDFTLADT,SKPLTTIPPTIVVQRPSQYFNNADGVDQGLPLSLKYGNEVILKTPFAGTSSDEMALEYVLKIPNYFSRFKYSSTSLPKQVLWTSPVHPQIIRNHVTVVD-APGQPTLLAYATGFFKYWRGGLVYTFRFVKTNYHSGRVQITFHPFVGYDDVMDSDGKIVRDEYVYRVVVDLRDQTEATLVVPFTSLTPYKVCADVFNSANRPKYNYEPRDFKVYDNTTDQF--FTGTLCVSALTPLVSSSAVVSSTIDVLVEVKASDDFEVAVPNTPLWLPVD-SLTERPS,100.00,414.03'
	#handleSingleAlignmentLine(line)
 # }


  #$query_hash =  buildXYZgHash($xyzng+daaaA_to_dAAAA($query)+'.xyzng')

  #ofname = File.new(fname+'.tmscore','w')
  #File.new(fname).each { |line|
	##line = '1b35C,3napC,0,262,32,0.519,0,1,266,1,277,SKPTVQGKIGECKLRGQGRMANFDGMDMSHKMALSSTNEIETNEGLAGTSLDVMDLSRVLSIPNYWDRFTWKTSDVINTVLWDNYVSPFKVKP-YSATITDRFRCTHMGKVANAFTYWRGSMVYTFKFVKTQYHSGRLRISFIPYYYNTTISTGT---PDVSRTQKIVVDLRTSTAVSFTVPYIGSRPWLYCI----RP---ESSWLSKDN---TDGALMYNCVSGIVRVEVLNQLVAAQN-VFSEIDVICEVNGGPDLEFAGPTCPRYVPYAGDFTLADT,SKPLTTIPPTIVVQRPSQYFNNADGVDQGLPLSLKYGNEVILKTPFAGTSSDEMALEYVLKIPNYFSRFKYSSTSLPKQVLWTSPVHPQIIRNHVTVVD-APGQPTLLAYATGFFKYWRGGLVYTFRFVKTNYHSGRVQITFHPFVGYDDVMDSDGKIVRDEYVYRVVVDLRDQTEATLVVPFTSLTPYKVCADVFNSANRPKYNYEPRDFKVYDNTTDQF--FTGTLCVSALTPLVSSSAVVSSTIDVLVEVKASDDFEVAVPNTPLWLPVD-SLTERPS,100.00,414.03'
	#out = handleSingleAlignmentLine(line) #rescue "ERROR #{line.split(',')[0..1].join}" 
	#ofname.puts out
  #}
  #ofname.close
end
