#1 -  6         Record name   "ATOM  "
#7 - 11         Integer       serial       Atom  serial number.
#13 - 16        Atom          name         Atom name.
#17             Character     altLoc       Alternate location indicator.
#18 - 20        Residue name  resName      Residue name.
#22             Character     chainID      Chain identifier.
#23 - 26        Integer       resSeq       Residue sequence number.
#27             AChar         iCode        Code for insertion of residues.
#31 - 38        Real(8.3)     x            Orthogonal coordinates for X in Angstroms.
#39 - 46        Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.
#47 - 54        Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.
#55 - 60        Real(6.2)     occupancy    Occupancy.
#61 - 66        Real(6.2)     tempFactor   Temperature  factor.
#77 - 78        LString(2)    element      Element symbol, right-justified.
#79 - 80        LString(2)    charge       Charge  on the atom.

#ATOM   1183  CA  GLY B 118       9.448 -13.112 115.394  1.00  1.00           C 

def buildResidueNamesConverter
	tlas=["ALA", "CYS", "ASP", "GLU", "PHE", "GLY", "HIS", "ILE", "LYS",  
                   "LEU", "MET", "ASN", "PRO", "GLN", "ARG", "SER", "THR", "VAL", 
                    "TRP", "TYR", 'UNK','GLX','ASX'] 
	as="ACDEFGHIKLMNPQRSTVWYXZB".split '' 
	
	hsh_tla2a = {}
	hsh_a2tla = {}
	(0..tlas.size).each { |i| 
		hsh_tla2a[tlas[i]]=as[i]
		hsh_a2tla[as[i]]=tlas[i]
	}
	return hsh_tla2a,hsh_a2tla
end	
 
def parseAtomsFromLines lines
	dummy = {}
	atoms = []
	lines.each {|a|
		next unless a.match(/^ATOM/) 
		next unless a[12..14].include? 'CA'	
		x = a[30..37].strip
		y = a[38..45].strip
		z = a[46..53].strip
		tla = a[17..19]
		id = a[22..25].to_i
		#dummy[a[12..14]]=1
		dummy[a[21]]=1
		atoms << [x,y,z,tla,id]
	}
	 puts  'more than one chain: '+lines[0] if dummy.size > 2
	atoms
end

hsh_tla2a,hsh_a2tla = buildResidueNamesConverter
arr = parseAtomsFromLines(File.new(ARGV[0]))
seqf = File.new(ARGV[0]+".bseq",'w')
seqf.puts '>'+ARGV[0].split('/')[-1]
xyzf = File.new(ARGV[0]+".xyzr",'w')
arr.each { |a| 
	#throw "can't convert tla #{a.inspect} @ #{ARGV[0]}" unless   hsh_tla2a[a[3]]
	seqf.print hsh_tla2a[a[3]]
	xyzf.puts a.join ' '
}
seqf.puts ''

#dump all seq files to one tmp
# %x[#{formatdb_bin}  -t master -i #{fasta_fname} -p T -V -n #{storage_path}/sync_master]
