def deltasForArrInt arr
	res = [arr[0]]
	arr[1..-1].each_with_index { |v,i| res << v-arr[i] }
	res
end
#-----------------------------------
def prefixSum arr
	arr[1..-1].each_with_index { |v,i| arr[i+1] = v+arr[i] }
	arr
end
#-----------------------------------
def encode_one n,res, mask=0
        if n<128
                res << (mask | (n%128))
        else
                encode_one(n>>7,res,128)
                res << ((n & 127)|mask)
        end     
end
#-----------------------------------
def encode(numbers)
        res = []
        numbers.each { |n| encode_one(n,res) }
        res
end
#-----------------------------------
def decode(bytes_arr)
        res = []
        acc = 0
        bytes_arr.each do |n|
                acc = ((acc<<7)+(n&127))
                if n<128 
                        res << acc
                        acc = 0
                end
        end             
        res 
end
#-----------------------------------
def getName x,str
	return eval(str)
end
#-----------------------------------
def compressOne out,name1,name2,rmsd,seq1,seq2,seq1A,seq2A
	prefix = [name1,name2,rmsd,seq1.size].join ' '
	out.write [prefix.size].pack('S')
	out.write prefix
	suffix = [
		encode(prefixSum(seq1)).pack('C*'),
		encode(prefixSum(seq2)).pack('C*'),
		seq1A.join(''),
		seq2A.join('')
	].join ''
	out.write [suffix.size].pack('S')
	out.write suffix
end
#-----------------------------------
def splitRI seqWithI
	ind = []
	as = []
	seqWithI.each { |v| 
		a=v[0].split(':')
		ind << a[1].to_i
		as << a[0]
	}
	[ind,as]
end
#-----------------------------------
#000002762.pdbnum.pdb(*) 000000004.pdbnum.pdb(*) 4.0737 E:131 V:1 T:132 T:2 Q:133 Y:3 D:134 N:4 E:137 L:5
def convertOldStyleLine out,line,strNameCode1,strNameCode2
	arr = line.split
	seqs = arr[3..-1].each_with_index.partition { |v,i| i.even? }
	seqs[0] = splitRI seqs[0]
	seqs[1] = splitRI seqs[1]
	if arr[0]<arr[1]
		compressOne out, getName(arr[0],strNameCode1),getName(arr[1],strNameCode2),arr[2],seqs[0][0],seqs[1][0],seqs[0][1],seqs[1][1]
	else
	    compressOne out, getName(arr[1],strNameCode2),getName(arr[0],strNameCode1),arr[2],seqs[1][0],seqs[0][0],seqs[1][1],seqs[0][1]
	end
end
#-----------------------------------

arg = 0
strNameCode1 = ARGV[arg] || 'x.split(".")[0]'
strNameCode2 = ARGV[arg+1] || 'x.split(".")[0]'

line = "000002762.pdbnum.pdb(*) 000000004.pdbnum.pdb(*) 4.0737 E:131 V:1 T:132 T:2 Q:133 Y:3 D:134 N:4 E:137 L:5 Q:138 P:9 L:139 T:10 F:140 T:11 S:142 N:21 L:143 V:22 T:144 L:23 Q:148 D:25 Q:149 A:28 Q:152 D:29 L:153 T:30 T:154 S:31 N:160 Y:48 K:161 Q:49 A:164 G:50 G:165 T:51 E:166 G:52 L:167 I:53 G:168 E:61 I:169 T:62 A:170 F:63 E:171 S:64 V:172 T:65 K:175 P:66 R:178 V:67 H:179 T:68 M:182 I:69 Q:183 K:70 L:185 K:71 N:186 K:72 V:187 R:74 R:188 H:75 S:189 I:76 L:190 T:78 L:193 I:94 V:194 Q:96 H:195 D:97 L:196 L:98 V:197 G:99 E:198 A:100 K:199 K:101 Y:200 L:102"

STDERR.puts line.size
convertOldStyleLine STDOUT,line,strNameCode1,strNameCode2


