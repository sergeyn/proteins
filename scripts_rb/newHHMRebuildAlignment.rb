#flat:
#-82.462,-2.374,89.540,SER,1

kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'
require kernel_path+'log_lib'
require kernel_path+'localEnv.rb'
require kernel_path+'utils'
require kernel_path+'blosum'
require kernel_path+'astrals'

$xyzng = '/'
$tmp = '/tmp/'
$tmscore =  '/shared/home/snepomny/TMscore_32'
$query_hash = {}
$query_aln
XYZR_SPLITTER = ' ' 

$query_hash = {}
$query_aln = '/'

#-------------------------------------------------------------------
def buildXYZgHash(fname)
  raise fname unless File.exist? fname

	hsh = {}
	File.new(fname).each { |l| 
		arr = l.split(XYZR_SPLITTER)
		hsh[arr[-1].to_i] = arr[0..-2]
	}
	raise fname unless hsh.size
	return hsh
end
#-------------------------------------------------------------------
def daaaA_to_dAAAA(name)
	return name[0..3].upcase + name[4..-1]
end
#-------------------------------------------------------------------
def loadAln(name)
	raise fname unless File.exist? name
	ret = File.new(name).each.map { |l| l.to_i }
#	puts ret.join ' '
	ret
end
#-------------------------------------------------------------------
def handleSingleAlignmentLine(line)
	arr = line.split($flatdelim)

	begin
	hsh_left = (arr[0]==$query) ? $query_hash : buildXYZgHash($xyzng+daaaA_to_dAAAA(arr[0])+'.xyzng')
	hsh_right = (arr[1]==$query) ? $query_hash : buildXYZgHash($xyzng+daaaA_to_dAAAA(arr[1])+'.xyzng')
	rescue
		STDERR.puts "skipping due to exception: #{arr[0..1].join(',')}" unless arr[0].match(/^d/) or arr[1].match(/^d/)
		return
	end
	offset_left = arr[7].to_i 
	offset_right = arr[9].to_i 
	seq_l = arr[11].chomp.split('')
	seq_r= arr[12].chomp.split('')
	return if seq_l.size < 50
	xyz_l = []
	xyz_r = []
	new_seq_l = []
	new_seq_r = []
	throw 'alignment length is wrong' unless seq_l.size == seq_r.size

	(0...seq_l.size).each { |i|
		if(hsh_left[offset_left] and hsh_right[offset_right]) #had both cas
			if(seq_l[i]!='-' and seq_r[i]!='-')
				throw "at left  #{i}/#{offset_left}: #{arr[0..1].join(',')} |#{seq_l[i]}|!= |#{$astrals[hsh_left[offset_left][-1]]}| != |#{hsh_left[offset_left][-1]}|"  if seq_l[i]!= $astrals[hsh_left[offset_left][-1]] and seq_l[i]!='X'
				throw "at right #{i}/#{offset_right}: #{arr[0..1].join(',')}  |#{seq_r[i]}| != |#{$astrals[hsh_right[offset_right][-1]]}| != |#{hsh_right[offset_right][-1]}|"  if seq_r[i]!= $astrals[hsh_right[offset_right][-1]] and seq_r[i]!='X'
				xyz_l << hsh_left[offset_left]
				xyz_r << hsh_right[offset_right]
			end
			new_seq_l << seq_l[i]
			new_seq_r << seq_r[i]
		end
		offset_left+=1 if(seq_l[i]!='-')
		offset_right+=1 if(seq_r[i]!='-')
	}
	return if new_seq_l.size<50
	#recalculate stats for new alignment
	length,ident,similar,gaps = getAlignmentMetrics(new_seq_l,new_seq_r)
	news = [ length, (ident.to_f*100.0/length.to_f).round, (similar.to_f*100.0/length.to_f).round, gaps]
	#store xyzs in tmp files
	prot1 = $tmp + arr[0..1].join('_') + '_left'
	prot2 = $tmp + arr[0..1].join('_') + '_right' 
	tmpxyzl = File.new(prot1,'w')
	tmpxyzr = File.new(prot2,'w')
	(1..xyz_l.size).each { |i| 
		tmpxyzl.puts form_legal_tmscore_line(i,xyz_l[i-1]) 
		tmpxyzr.puts form_legal_tmscore_line(i,xyz_r[i-1])
	}
	tmpxyzl.close
	tmpxyzr.close
	#get tmscore data
	#puts "#{$tmscore} #{prot1} #{prot2}"
	tm = parse_tmscore_lines(%x[#{$tmscore} #{prot1} #{prot2} | grep -e RMSD -e '-score' | grep -v RMSD= | grep -v predict | cut -d'=' -f2 ]) 
	%x[rm -f #{prot1} #{prot2}]
	#write new alignment,stats and tmscore data to result file
	return line.chomp + " " + news.join(" ") + " " + tm
end



#==============================================================================
if __FILE__ == $0
  #flat name:
  fname=ARGV[0] #'/home/snepomny/rsync/hhm_res_july12/1b35C.hhr.flat'
  $query = fname.split('/')[-1].split('.hhr')[0] 

  #$query_hash =  buildXYZgHash($xyzng+daaaA_to_dAAAA($query)+'.xyzng')
  
  #ofname = File.new(fname+'.tmscore','w')
  #File.new(fname).each { |line|
  # out = handleSingleAlignmentLine(line) rescue "ERROR #{line}" 
  # ofname.puts out
  #}
  #ofname.close

  #File.new(fname).each { |line|
#	#line = '1b35C,3napC,0,262,32,0.519,0,1,266,1,277,SKPTVQGKIGECKLRGQGRMANFDGMDMSHKMALSSTNEIETNEGLAGTSLDVMDLSRVLSIPNYWDRFTWKTSDVINTVLWDNYVSPFKVKP-YSATITDRFRCTHMGKVANAFTYWRGSMVYTFKFVKTQYHSGRLRISFIPYYYNTTISTGT---PDVSRTQKIVVDLRTSTAVSFTVPYIGSRPWLYCI----RP---ESSWLSKDN---TDGALMYNCVSGIVRVEVLNQLVAAQN-VFSEIDVICEVNGGPDLEFAGPTCPRYVPYAGDFTLADT,SKPLTTIPPTIVVQRPSQYFNNADGVDQGLPLSLKYGNEVILKTPFAGTSSDEMALEYVLKIPNYFSRFKYSSTSLPKQVLWTSPVHPQIIRNHVTVVD-APGQPTLLAYATGFFKYWRGGLVYTFRFVKTNYHSGRVQITFHPFVGYDDVMDSDGKIVRDEYVYRVVVDLRDQTEATLVVPFTSLTPYKVCADVFNSANRPKYNYEPRDFKVYDNTTDQF--FTGTLCVSALTPLVSSSAVVSSTIDVLVEVKASDDFEVAVPNTPLWLPVD-SLTERPS,100.00,414.03'
	#handleSingleAlignmentLine(line)
 # }


  #$query_hash =  buildXYZgHash($xyzng+daaaA_to_dAAAA($query)+'.xyzng')

  #ofname = File.new(fname+'.tmscore','w')
  #File.new(fname).each { |line|
	##line = '1b35C,3napC,0,262,32,0.519,0,1,266,1,277,SKPTVQGKIGECKLRGQGRMANFDGMDMSHKMALSSTNEIETNEGLAGTSLDVMDLSRVLSIPNYWDRFTWKTSDVINTVLWDNYVSPFKVKP-YSATITDRFRCTHMGKVANAFTYWRGSMVYTFKFVKTQYHSGRLRISFIPYYYNTTISTGT---PDVSRTQKIVVDLRTSTAVSFTVPYIGSRPWLYCI----RP---ESSWLSKDN---TDGALMYNCVSGIVRVEVLNQLVAAQN-VFSEIDVICEVNGGPDLEFAGPTCPRYVPYAGDFTLADT,SKPLTTIPPTIVVQRPSQYFNNADGVDQGLPLSLKYGNEVILKTPFAGTSSDEMALEYVLKIPNYFSRFKYSSTSLPKQVLWTSPVHPQIIRNHVTVVD-APGQPTLLAYATGFFKYWRGGLVYTFRFVKTNYHSGRVQITFHPFVGYDDVMDSDGKIVRDEYVYRVVVDLRDQTEATLVVPFTSLTPYKVCADVFNSANRPKYNYEPRDFKVYDNTTDQF--FTGTLCVSALTPLVSSSAVVSSTIDVLVEVKASDDFEVAVPNTPLWLPVD-SLTERPS,100.00,414.03'
	#out = handleSingleAlignmentLine(line) #rescue "ERROR #{line.split(',')[0..1].join}" 
	#ofname.puts out
  #}
  #ofname.close
end
