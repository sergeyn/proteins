kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'
require kernel_path+'utils'

$hsh = {}
$similarity_col = (ARGV[1] || 5).to_i

def processFile f
	#first path
	fopen_gz(f).each_with_index do |line,i|
		STDERR.puts i if i%100000 == 0
		ar = line.split()
		$hsh[ar[0..1]] = [] unless $hsh[ar[0..1]]
		$hsh[ar[0..1]] << ar[$similarity_col].to_f
	end
	#second path
	fopen_gz(f).each do |line|
		ar = line.split()
		similars = $hsh[ar[0..1]]
		if similars.size == 1 #there was no sym. pair
			puts line
		elsif similars.size > 1 and similars[0]>=similars[1] #we have the better pair
			puts line
			$hsh[ar[0..1]].clear
		else #will print the 2nd
			$hsh[ar[0..1]][0] = similars[1]
		end
	end
end

Dir[ARGV[0]].each do |flat|
	processFile(flat)
end
