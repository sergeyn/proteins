kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'
require kernel_path+'utils'

$threshold = (ARGV[1] || 35).to_f / 100.0

#left right rmsd similarity R:8 A:13 ...
#000000004.pdbnum.pdb(*) 000000004.pdbnum.pdb(*) 0.0000 0.34 V:1 V:1 T:2 T:2 ...
def one(f)
  fopen_gz(f).each do |line|
	ar = line.split
	puts line if ar[3].to_f >= $threshold
  end
end

Dir[ARGV[0]].each {|f| one f }
