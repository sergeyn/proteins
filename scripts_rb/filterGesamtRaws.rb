kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'
require kernel_path+'utils'
require kernel_path+'blosum'

$b = Blosum.new(getRawBlosumNCBIStyle)
$threshold = (ARGV[1] || 30).to_f / 100.0

#left right rmsd R:8 A:13 ...
#000000004.pdbnum.pdb(*) 000000004.pdbnum.pdb(*) 0.0000 V:1 V:1 T:2 T:2 ...
def one(f)
  fopen_gz(f).each do |line|
	ar = line.chomp.split
	next if ar[0] == ar[1]
	cntSimilars = ar[3..-1].each_slice(2).count do |pair|
		r1,r2 = pair[0].split(':')[0],pair[1].split(':')[0]
		(r1 != '-' and r2 != '-') ? ($b[r1,r2]>0) : false
	end
	similarP = cntSimilars.to_f / ((ar.size-3)/2).to_f
	puts "#{ar[0..2].join ' '} #{similarP.round(3)} #{ar[3..-1].join ' '}" if similarP >= $threshold
  end
end

Dir[ARGV[0]].each {|f| one f }
