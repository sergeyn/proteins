kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'
require kernel_path+'utils'
require kernel_path+'globals'

f=fopen_gz ARGV[0]

#HHsearch
#NAME  e16vpA1 A:1-303,A:349-356 000007584
#FILE  e16vpA1
#...
#>e16vpA1 A:1-303,A:349-356 000007584
#SRMPSPPMPVPPAALFNRLLDDLGFSAGPALCTMLDTWNEDLFSALPTNADLYRECKFLSTLPSDVVEWGDAYVPERTQIDIRAHGDVAFPTLPATRDGL
#GLYYEALSRFFHAELRAREESYRTVLANFCSALYRYLRASVRQLHRQAHMRGRDRDLGEMLRATIADRYYRETARLARVLFLHLYLFLTREILWAAYAEQ
#...
#>

nameLine = skip_io_until(f,'NAME')
name = nameLine.chomp.split()[1]
fasta=skip_io_until(f,'>'+name)
arr = get_lines_until_delim_from_f f,'>'

w = File.new(name+'.fasta','w')
w.puts fasta.chomp
w.puts arr.join ''
w.close
