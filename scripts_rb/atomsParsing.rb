DEPRECATED

kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'
require kernel_path+'utils'
#==============================================================================
#input is an .ent file with lines:
#0123456789 123456789 123456789 123456789 123456789 123456789 123456789
#ATOM      1  N   ALA A   4      11.751  37.846  29.016  1.00 44.65           N         
#ATOM      2  CA  ALA A   4      12.501  39.048  28.539  1.00 30.68           C

#domain names can be d6rlx.1 or even d6rxn__
#in pdb the chains are case sensitive -- because there are cases where >26 chains

$fieldDelimiters = ' '

#==============================================================================
def joinMatrix mat, delim = $fieldDelimiters
  (mat.map { |row| row.join($fieldDelimiters)}).join "\n"
end
#==============================================================================
def getAllResiduesWithIndsFromEnt entFile #even when we don't have CAs
  runningResidue = 0
  lastIndex = ''
  residues = []
  indices = []
  File.new(entFile).each { |l|
          break if l.include? 'ENDMDL'
          next unless (l.include? 'ATOM' or l.include? 'HETATM')
          if(lastIndex!=l[$rangeIndex]) #new index
                  runningResidue+=1
                  lastIndex=l[$rangeIndex]
                  indices << l[$rangeIndex].to_i 
                  residues << tlaToOneLetter(l[$rangeTlaResidue])
          end
  }
  return residues,indices
end
#==============================================================================
#read the residues from .hhm header
def getResFromHHM hhmFile
  start = false
  hhRes = []
  bname = getNameFromFile(hhmFile)
  prefix = '>'+bname
  File.new(hhmFile).each { |l| 
          break if start and l.include? '>' or l.include? '#'
          hhRes << l.chomp if start
          start = true if start==false and l.include?(prefix)
  }
  hhRes.join('').split('')
end  
#==============================================================================
#get masks from .hhm header
def getMasksFromHHM hhmFile
  start = false
  hhMaskRes = []
  File.new(hhmFile).each { |l| 
          break if start and l.include? '>' 
          hhMaskRes << l.chomp if start
          start = true if start==false and l.include?('>ss_dssp')
  }
  hhMaskRes.join('').split('')
end
#==============================================================================
def onlyCAsXYZri(fname) #get xyzri of those with CAs
  atoms = []
  File.new(fname).each { |l|  
      next unless l.include? 'ATOM' or l.include? 'HETATM'
      if(l[$rangeCA] == 'CA')
        one_letter = tlaToOneLetter(l[$rangeTlaResidue])
        one_letter = 'X' unless one_letter
        atoms << [l[$rangeX].strip,l[$rangeY].strip,l[$rangeZ].strip,one_letter,l[$rangeIndex].to_i]                      
      end
  }
  atoms
end
#==============================================================================
def validateHHM_Res resHHM, mask, resFromAtoms
  mask = resHHM if mask.empty?
  res = (resHHM.size == mask.size and  (resFromAtoms.size +  mask.count('-')) == mask.size)
  print "#{mask.count('-')} + #{resFromAtoms.size} =?= #{resHHM.size} =?= #{mask.size} " unless res
  res
end
#==============================================================================
#bug-bug-bug: forgot to use entInds!!!
def mapHhmResiduesToRangeIndex entRes, entInds, hhmRes, hhmMask
  hhmMask = hhmRes if hhmMask.empty? #sometimes it is empty
  throw 'wrong masked pair ' + ARGV[0] unless hhmRes.size == hhmMask.size
  hhmResMasked = hhmRes.each_with_index.map { |r,i| hhmMask[i]=='-' ? '*' : r } #replace missing residues by '*' via mask
  if hhmResMasked-['*'] == entRes #it is the easy case!
     return hhmResMasked.each_with_index.map { |r,i| (r == '*' or r!=entRes[i])? -1 : i }
  end 
  
  #now the hard case -- we need to produce an alignment with entRes
  entAl,hhmAl = needle(entRes, hhmResMasked)
  #after alignment possible situations: match, gap, mismatch of some kind
  # ent: ab-d-e
  # hhm: a-c**f
  throw 'wrong alignment ' + ARGV[0] unless entAl.size == hhmAl.size
  stepEnt = 0
  finalEntInds = []
  entAl.each_with_index {|r,i|
      if r == hhmAl[i] #match 
         finalEntInds << stepEnt
         stepEnt += 1
      else   
        stepEnt +=1 if r != '-'  
        finalEntInds << -1 if hhmAl[i] != '-'
      end 
  }
  #validate:
  finalEntInds.each_with_index { |r,i| throw 'bad fix' if r!=-1 and hhmRes[i]!=entRes[r]  }
  finalEntInds
end
#==============================================================================
def maskOffIndsWithNoCA! mappingR, casxyzri, allinds
  require 'set'
  casSet = Set.new
  casxyzri.each { |t| casSet.add(t[4]) }
  (0..mappingR.size).each { |i| mappingR[i]=-1 if mappingR[i]!=-1 and (not casSet.include?(allinds[i])) }
end
#==============================================================================
def convertBasicAlnToIndexAln fname
    name = getNameFromFile(fname)
    aln = File.new(fname).map { |l| l.to_i} #read aln to array
    seq,ind = getAllResiduesWithIndsFromEnt(entFromName name) #get all inds
    w = File.new(fname+'i','w')
    aln.each { |n| w.puts((n==-1 or ind[n]==nil) ?-99999 : ind[n] )}
end

convertBasicAlnToIndexAln ARGV[0]
#name = getNameFromFile(ARGV[0])
#puts joinMatrix(onlyCAsXYZri(entFromName name))

# hhmRes = getResFromHHM(hhmFromName name)
# hhmMask = getMasksFromHHM(hhmFromName name)
# seq,ind = getAllResiduesWithIndsFromEnt(entFromName name)
# mappingR = mapHhmResiduesToRangeIndex seq,ind,hhmRes,hhmMask
# casxyzri = onlyCAsXYZri(entFromName name)
# maskOffIndsWithNoCA!(mappingR,casxyzri,ind)
# seqRes = casxyzri.map { |row| row[3]}
# 
# out = File.new(ARGV[0]+'.aln','w')
# out.puts mappingR.join("\n")
# 
# out = File.new(ARGV[0]+'.xyzr','w')
# out.puts joinMatrix(casxyzri)
# 
# out =  File.new(ARGV[0]+'.bseq','w')
# out.puts ">#{name}"
# out.puts seqRes.join '' 


