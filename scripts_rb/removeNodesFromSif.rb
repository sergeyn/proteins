if(ARGV.size<2)
	puts 'Usage: remove src.sif fileWithNames'
	exit
end

require 'set'
def getNamesRaw(fname)
	names = File.new(ARGV[1]).map { |l| l.chomp }
	return Set.new(names)
end

namesS = getNamesRaw(ARGV[1])
File.new(ARGV[0]).each { |l|
	a = l.split
	next if namesS.include? a[0] or namesS.include? a[2]
	print l
}
