kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'
require kernel_path +'env'
require kernel_path +'pdb_my_lib.rb'
require kernel_path +'log_lib'
require kernel_path +'db_lib'
require kernel_path +'blast_lib'

$astrals= {
'ALA'=>'A','VAL'=>'V','PHE'=>'F','PRO'=>'P','MET'=>'M','ILE'=>'I','LEU'=>'L','ASP'=>'D','GLU'=>'E','LYS'=>'K','ARG'=>'R','SER'=>'S','THR'=>'T','TYR'=>'Y','HIS'=>'H','CYS'=>'C','ASN'=>'N','GLN'=>'Q','TRP'=>'W','GLY'=>'G','UNK'=>'X','2AS'=>'D','3AH'=>'H','5HP'=>'E','ACL'=>'R','AGM'=>'R','AIB'=>'A','ALM'=>'A','ALO'=>'T','ALY'=>'K','ARM'=>'R','ASA'=>'D','ASB'=>'D','ASK'=>'D','ASL'=>'D','ASQ'=>'D','ASX'=>'B','AYA'=>'A','BCS'=>'C','BHD'=>'D','BMT'=>'T','BNN'=>'A','BUC'=>'C','BUG'=>'L','C5C'=>'C','C6C'=>'C','CCS'=>'C','CEA'=>'C','CGU'=>'E','CHG'=>'A','CLE'=>'L','CME'=>'C','CSD'=>'A','CSO'=>'C','CSP'=>'C','CSS'=>'C','CSW'=>'C','CSX'=>'C','CXM'=>'M','CY1'=>'C','CY3'=>'C','CYG'=>'C','CYM'=>'C','CYQ'=>'C','DAH'=>'F','DAL'=>'A','DAR'=>'R','DAS'=>'D','DCY'=>'C','DGL'=>'E','DGN'=>'Q','DHA'=>'A','DHI'=>'H','DIL'=>'I','DIV'=>'V','DLE'=>'L','DLY'=>'K','DNP'=>'A','DPN'=>'F','DPR'=>'P','DSN'=>'S','DSP'=>'D','DTH'=>'T','DTR'=>'W','DTY'=>'Y','DVA'=>'V','EFC'=>'C','FLA'=>'A','FME'=>'M','GGL'=>'E','GL3'=>'G','GLX'=>'Z','GLZ'=>'G','GMA'=>'E','GSC'=>'G','HAC'=>'A','HAR'=>'R','HIC'=>'H','HIP'=>'H','HMR'=>'R','HPQ'=>'F','HTR'=>'W','HYP'=>'P','IIL'=>'I','IYR'=>'Y','KCX'=>'K','LLP'=>'K','LLY'=>'K','LTR'=>'W','LYM'=>'K','LYZ'=>'K','MAA'=>'A','MEN'=>'N','MHS'=>'H','MIS'=>'S','MLE'=>'L','MPQ'=>'G','MSA'=>'G','MSE'=>'M','MVA'=>'V','NEM'=>'H','NEP'=>'H','NLE'=>'L','NLN'=>'L','NLP'=>'L','NMC'=>'G','OAS'=>'S','OCS'=>'C','OMT'=>'M','PAQ'=>'Y','PCA'=>'E','PEC'=>'C','PHI'=>'F','PHL'=>'F','PR3'=>'C','PRR'=>'A','PTR'=>'Y','SAC'=>'S','SAR'=>'G','SCH'=>'C','SCS'=>'C','SCY'=>'C','SEL'=>'S','SEP'=>'S','SET'=>'S','SHC'=>'C','SHR'=>'K','SMC'=>'C','SOC'=>'C','STY'=>'Y','SVA'=>'S','TIH'=>'A','TPL'=>'W','TPO'=>'T','TPQ'=>'A','TRG'=>'K','TRO'=>'W','TYB'=>'Y','TYQ'=>'Y','TYS'=>'Y','TYY'=>'Y'
}

$bl2seq = get_path('bl2seq') #'/home/snepomny/structures/progs/blast-2.2.22/bin/bl2seq'
$seq_files = get_path('seq_files') #'/home/snepomny/structures/data/new_seq_files'
$tmp = '/tmp/'

$cache_newxyz = {}
#-------------------------------------------------------------------
def rmsd_one_hhp_hit(l_name,r_name,blast_offset_q,blast_offset_s,blast_q,blast_s)
        l_name = l_name.sub("_","")
        r_name = r_name.sub("_","")
        newxyzs = '/home/snepomny/rsync/preMarchStuff/boolCAvectors/'
        xyz_q = File.new(newxyzs+l_name+".vec").map { |r| r}
        xyz_s = File.new(newxyzs+r_name+".vec").map { |r| r}
	#dbrq = dbref_hsh[l_name]
	#dbrs = dbref_hsh[r_name]
	construct_temp_hhp_xyz(xyz_q,xyz_s,blast_offset_q.to_i-1,blast_offset_s.to_i-1,blast_q.split(''),blast_s.split(''),l_name,r_name) #note the change in q,s is on purpose
        #tm = parse_tmscore_lines(%x[#{get_path('TMscore')}  #{$tmp}#{l_name}#{r_name}q.xyz #{$tmp}#{l_name}#{r_name}s.xyz  | grep -e RMSD -e '-score' |  tail -6 | head -5 | cut -d'=' -f2])
#        %x[rm #{$tmp}#{l_name}#{r_name}q.xyz #{$tmp}#{l_name}#{r_name}s.xyz ]
	
end
#-------------------------------------------------------------------
#will syntesizze TMScore friendly ATOMS line fro raw xyz nd a counter
def form_legal_tmscore_line(counter,xyz_arr)
		return 'xyz_arr is nil in form_legal_tmscore_line' if !xyz_arr
		xyz = xyz_arr.split(',').map { |i|  sprintf("%8.3f",i.to_f)}
					
		spaces_0 = ' ' if counter < 1000
		spaces_0 = '  ' if counter < 100
		spaces_0 = '   ' if counter < 10

		return sprintf('ATOM      1  CA  ILE A%s%d    %s%s%s',spaces_0,counter,xyz[0],xyz[1],xyz[2]) 
end
#--------------------------------------------------------------------
def align_str2xyz(offset,str,xyz,sgap='-',xgap='@')
	#offset -= 1
	arr = []
	str.each { |c| 
		if ( c.include? sgap)
			arr << xgap
		else
			arr << xyz[offset].chomp rescue arr<<'#'
			offset += 1
		end
	}
	arr
end
#-------------------------------------------------------------------
#use the padded xyz structures as boolean vectors to mask the alignment 
def removeMissingResiduesFromAlignment(offset_q,offset_s,seq_q,seq_s,xyz_q,xyz_s,sgap='-')
	throw "misaligned!input #{seq_q.size} - #{seq_s.size}" if seq_q.size != seq_s.size
	new_s = []
	new_q = []
	(0...seq_q.size).each { |i|
		#skip when gap
		 
		#skip when residue missing
		unless xyz_q[offset_q]==nil or xyz_q[offset_q].include?('#') or xyz_s[offset_s]==nil or xyz_s[offset_s].include?('#') 
			#sanity check
			unless(seq_q[i].include?(sgap) or seq_s[i].include?(sgap)) 
				if $astrals[xyz_q[offset_q].split(',')[-1].chomp]!=seq_q[i]
					print seq_q[i],'|',xyz_q[offset_q].split(',')[-1].chomp,"|",i,"\n" unless seq_q[i].include? 'X'
					return [],[] unless seq_q[i].include? 'X'
				elsif $astrals[xyz_s[offset_s].split(',')[-1].chomp]!=seq_s[i]
					print seq_s[i],'|',xyz_s[offset_s].split(',')[-1].chomp,"|",i,"\n" unless seq_s[i].include? 'X'
	                                return [],[] unless seq_s[i].include? 'X'
				end
			end
			
			
			new_s << seq_s[i]
			new_q << seq_q[i]
	      	end
		offset_q += 1 unless seq_q[i].include?(sgap)
	        offset_s += 1 unless seq_s[i].include?(sgap)
	

	}
	return new_q,new_s
end

def construct_temp_hhp_xyz(xyz_q,xyz_s,offset_q,offset_s,seq_q,seq_s,l_name,r_name)
	
	throw "misaligned!input #{seq_q.size} - #{seq_s.size}" if seq_q.size != seq_s.size
	#first we align strings to xyzs
	aligned_q = align_str2xyz(offset_q,seq_q,xyz_q)
	aligned_s= align_str2xyz(offset_s,seq_s,xyz_s)
	throw "misaligned!aligned" if aligned_q.size != aligned_s.size
  	n_xyz_q = []
	n_xyz_s = []	
#	puts "#{offset_q},#{offset_s}"
#	puts aligned_q.join
#	puts aligned_s.join
	ind_q = 0
        ind_s = 0
        while ind_q < aligned_q.size and ind_s < aligned_s.size do 
		if ( aligned_q[ind_q].include? '.' and aligned_s[ind_s].include? '.') #aa
                        n_xyz_q << form_legal_tmscore_line(n_xyz_q.size+1, aligned_q[ind_q].chomp)
                        n_xyz_s << form_legal_tmscore_line(n_xyz_q.size, aligned_s[ind_s].chomp)
			ind_q += 1
	                ind_s += 1
			#print  aligned_q[ind_q-1].split(',')[-1],',',seq_q[ind_q-1],$astrals[aligned_q[ind_q-1].split(',')[-1]]
			#print  ' ',aligned_s[ind_s-1].split(',')[-1],',',seq_s[ind_s-1],$astrals[aligned_s[ind_s-1].split(',')[-1]]
			#puts $astrals[aligned_q[ind_q-1].split(',')[-1]]
			#puts aligned_q[ind_q-1].split(',')[-1]
			#puts seq_q[ind_q-1]
			#throw "tla didn't fit one letter" unless $astrals[aligned_q[ind_q-1].split(',')[-1]] == seq_q[ind_q-1]
			#throw "tla didn't fit one letter" unless $astrals[aligned_s[ind_s-1].split(',')[-1]] == seq_s[ind_s-1]	
			return -1  unless $astrals[aligned_q[ind_q-1].split(',')[-1]] == seq_q[ind_q-1]
			return -2  unless $astrals[aligned_s[ind_s-1].split(',')[-1]] == seq_s[ind_s-1]
		#elsif ( aligned_q[ind_q].include? '@' and aligned_s[ind_s].include? ',') # @a
                #        ind_q += 1
	#		ind_s += 1
	        #elsif ( aligned_q[ind_q].include? ',' and aligned_s[ind_s].include? '@') # a@
	#		ind_s += 1
#			ind_q += 1
#		elsif ( aligned_q[ind_q].include? '#' or aligned_s[ind_s].include? '#')  # a# or #a or ##
 #                       ind_q += 1
#			ind_s += 1
#		 elsif ( aligned_q[ind_q].include? '@' and aligned_s[ind_s].include? '@')
		else
			#throw "gap against gap in alignment?"
			ind_q += 1
                        ind_s += 1
		end
	end
	throw "misaligned!!output" if n_xyz_q.size != n_xyz_s.size
	return n_xyz_q.size
begin
	fs = File.new("#{$tmp}#{l_name}#{r_name}s.xyz","w")
        fq = File.new("#{$tmp}#{l_name}#{r_name}q.xyz","w")
rescue
	puts "couldnt' save tempfile!!"
	sleep(1)
	begin 
		fs = File.new("#{$tmp}#{l_name}#{r_name}s.xyz","w")
        	fq = File.new("#{$tmp}#{l_name}#{r_name}q.xyz","w")
	rescue
		return 
	end
end
	fs.puts n_xyz_s.join("\n")
	fq.puts n_xyz_q.join("\n")
	fq.close
	fs.close
end

#-------------------------------------------------------------------
#will index xyz data of 2 chains with blast alignment
def save_temp_xyz(xyz_q,xyz_s,ind_arrays,l_name,r_name)
	raise "misaligned!" if ind_arrays[0].size != ind_arrays[1].size
#  	puts "#{xyz_q.size},#{xyz_s.size}"
# 	puts xyz_q
#	puts xyz_s

# 	puts ind_arrays[0].size
# 	print ind_arrays[0]
#  	puts ""
#  	print ind_arrays[1]
# 	puts ""
	
	fs = File.new("#{$tmp}#{l_name}#{r_name}s.xyz","w")
	fq = File.new("#{$tmp}#{l_name}#{r_name}q.xyz","w")
	index_for_fs = ind_arrays[1]
	index_for_fq = ind_arrays[0]
	
	counter = 0
	(0..index_for_fs.size-1).each do |i|
		 if xyz_s[index_for_fs[i]]!="-" and xyz_q[index_for_fq[i]]!="-"
			counter += 1
			fs.puts form_legal_tmscore_line(counter,xyz_s[index_for_fs[i]])
			fq.puts form_legal_tmscore_line(counter,xyz_q[index_for_fq[i]])
		end
	end
	fs.flush
	fs.close
	fq.flush
	fq.close
end
#-----------------------------------------------
def tmscore_RachelCode prot1, prot2
	#anyway Rachel works only on home env.
	str_files = '/home/snepomny/structures/data/new_str_files' 
	
	blast_pdbs = '/home/snepomny/structures/pdb_utils/bin/blast_pdbs'
	tmscore = '/home/snepomny/structures/progs/TMscore_32'
	
	cmd = " #{$bl2seq} -i #{$seq_files}/#{prot1}.seq -j #{$seq_files}/#{prot2}.seq -p blastp -F F > /tmp/blast"
	#puts cmd
	%x[#{cmd}]
	
	cmd = "#{blast_pdbs} #{str_files}/#{prot1}.brk  #{str_files}/#{prot2}.brk -B blast > /tmp/_output_"
	%x[#{cmd}]
	
	sline1 = `grep -n MOLECULE /tmp/_output_ | head -n 1 | awk -F: '{print $1}'`
	sline2 = `grep -n MOLECULE /tmp/_output_ | tail -n 1 | awk -F: '{print $1}'`
	eline1 = `grep -n END /tmp/_output_ | head -n 1 | awk -F: '{print $1}'`
	eline2 = `grep -n END /tmp/_output_ | tail -n 1 | awk -F: '{print $1}'`
	sline1.chomp!
	eline1.chomp!
	sline2.chomp!
	eline2.chomp!
	# 
	len1 = eline1.to_i - sline1.to_i
	len2 = eline2.to_i - sline2.to_i
	
	%x[head -n #{eline1} /tmp/_output_ | tail -n #{len1} > /tmp/#{prot1}]
	%x[head -n #{eline2} /tmp/_output_ | tail -n #{len2} > /tmp/#{prot2}]
	tm = parse_tmscore_lines(%x[#{tmscore} /tmp/#{prot1} /tmp/#{prot2} | grep -e RMSD -e '-score' | grep -v RMSD= | grep -v predict | cut -d'=' -f2 ]) #| grep -e ^RMSD -e ^TM-score -e ^GDT-TS -e ^GDT-HA -e ^Max -e ^Structure
	log_me "#{prot1} |#{prot2} |" + tm
	return tm.split("|")[0].to_f
end




def getXYZ(name)
	l_name = name.sub("_","")
	newxyzs = '/home/snepomny/rsync/padRXYZ/'
        $cache_newxyz[l_name] = File.new(newxyzs+l_name).map { |r| r} unless $cache_newxyz[l_name] rescue return []
	return $cache_newxyz[l_name]
end

def fix_one_hhp_hit(l_name,r_name,blast_offset_q,blast_offset_s,blast_q,blast_s)
        l_name = l_name.sub("_","")
        r_name = r_name.sub("_","")
        newxyzs = '/home/snepomny/rsync/padRXYZ/' #'/home/snepomny/rsync/preMarchStuff/boolCAvectors/' #/s9999hared/home/snepomny/runs/boolCAvectors/'
	$cache_newxyz[l_name] = File.new(newxyzs+l_name).map { |r| r} unless $cache_newxyz[l_name]
	$cache_newxyz[r_name] = File.new(newxyzs+r_name).map { |r| r} unless $cache_newxyz[r_name]
        #dbrq = dbref_hsh[l_name]
        #dbrs = dbref_hsh[r_name]
        return construct_temp_hhp_xyz($cache_newxyz[l_name],$cache_newxyz[r_name],blast_offset_q.to_i-1,blast_offset_s.to_i-1,blast_q.split(''),blast_s.split(''),l_name,r_name) #note$
        #tm = parse_tmscore_lines(%x[#{get_path('TMscore')}  #{$tmp}#{l_name}#{r_name}q.xyz #{$tmp}#{l_name}#{r_name}s.xyz  | grep -e RMSD -e '$
#        %x[rm #{$tmp}#{l_name}#{r_name}q.xyz #{$tmp}#{l_name}#{r_name}s.xyz ]

end

require 'blosum'

def getAlignmentMetrics(seq_q, seq_s)
	raise "unequal alignment strings\n#{seq_q}\n#{seq_s}\n" unless seq_q.size == seq_s.size
	blsm62 = Blosum.new(getRawBlosumNCBIStyle)
	length = seq_q.size
	ident = 0
	similar = 0
	gaps = 0
	(0...seq_q.size).each { |i| 
		gapflag = (seq_q[i].include? '-' or seq_s[i].include? '-') #if gap
		gaps +=1 if gapflag
		next if gapflag 
		ident +=1 if seq_q[i] == seq_s[i]
		similar += 1 if blsm62[seq_q[i],seq_s[i]]>0
		#print seq_s[i],seq_q[i]
	}
	return length,ident,similar,gaps
end

def dumpBlastTableToFile(table,where)
	outf = "/tmp/dump_#{table}"
	%x[#{$msql} 'select * from #{table} #{where}' > #{outf}]
	arr = []
	File.new(outf).each { 
		|l| arr = l.split  #gather column names
		break
	}
	names = {}
	(0...arr.size).each {|i| names[arr[i]] = i }
	puts names.inspect
	%x[sed -i 1d #{outf}]
	names['outf'] = outf
	return names
end

where = ARGV[2] || ''
columns = dumpBlastTableToFile(ARGV[0],where)
fname = columns['outf']
outf = File.new(fname+'.fixed','w')
nextscount = 0
File.new(fname).each { |l|
	arr = l.split
	seq_q = arr[columns['seq_q']].chomp.gsub('\n','').split('')
	seq_s = arr[columns['seq_s']].chomp.gsub('\n','').split('')
	xyz_q = getXYZ(arr[0])
	xyz_s = getXYZ(arr[1])
	offset_q = -1 + arr[columns['left_q']].to_i 
	offset_s = -1 + arr[columns['left_s']].to_i 
	new_s=[]
	new_q = []
	if(ARGV[1] and ARGV[1].include? 'hhp')
		new_q,new_s=removeMissingResiduesFromAlignment(offset_q,offset_s,seq_q,seq_s,xyz_q,xyz_s)
	else
		new_q=seq_q
		new_s=seq_s
	end
	
	#nextscount +=1 if new_s.empty?
	
	puts arr[0..1].join(",") if new_s.empty? or new_q.empty?
	next if new_s.empty? or new_q.empty?

	length,ident,similar,gaps = getAlignmentMetrics(new_q,new_s)#(seq_q,seq_s)
	news = [ length, (ident.to_f*100.0/length.to_f).to_i, (similar.to_f*100.0/length.to_f).to_i, gaps]
	#arr[columns['seq_q']]=new_q.join('')
	#arr[columns['seq_s']]=new_s.join('')
	arr[columns['length']] = news[0]
	arr[columns['ident']] = news[1]
	positivesCol = columns['positives'] || columns['similar'] #in some tables we call it differently :(
	arr[positivesCol] = news[2]
	arr[columns['gaps']] = news[3]
	outf.puts arr.join(' ')
}
puts nextscount
outf.flush
outf.close
