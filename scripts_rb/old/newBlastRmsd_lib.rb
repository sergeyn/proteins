kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'
require kernel_path+'log_lib'
require kernel_path+'utils'
require kernel_path+'localEnv'
require kernel_path +'naming.rb'
load kernel_path +'astrals.rb'

$tmp = '/tmp/'
$xyzng_dir = '/shared/home/snepomny/propellers/'
$tmscore = '/shared/home/snepomny/TMscore_32 '
$xyzrdelim = ','
#-------------------------------------------------------------------
def align_str2xyz(offset,str,xyz,sgap='-',xgap='@')
	offset = offset.to_i - 1 # ? why had to add it?
  arr = []
  str.each_with_index { |c,i| 
    if ( c.include? sgap)
      arr << xgap
    else
      arr << xyz[offset].chomp rescue arr<<'#'
      unless xyz[offset].split($xyzrdelim)[3] == c
        raise "@#{i}: #{c} <-> #{ xyz[offset].split($xyzrdelim)[3]}"
      end 
      offset += 1
    end
  }
  arr
end
#-------------------------------------------------------------------
def namexyz(left,right,n)
	"#{$tmp}#{left}#{right}#{n}.xyz"	
end
#-------------------------------------------------------------------
def construct_temp_not_hhp_xyz(xyz_q,xyz_s,offset_q,offset_s,seq_q,seq_s,l_name,r_name)
        throw "misaligned!input #{seq_q.size} - #{seq_s.size}\n #{seq_q}\n #{seq_s}" if seq_q.size != seq_s.size
        #first we align strings to xyzs
        aligned_q = align_str2xyz(offset_q,seq_q,xyz_q)
        aligned_s= align_str2xyz(offset_s,seq_s,xyz_s)

        throw "misaligned!aligned" if aligned_q.size != aligned_s.size
        n_xyz_q = []
        n_xyz_s = []
        final_q = []
        final_s = []
        
        ind_q = 0
        ind_s = 0
        while ind_q < aligned_q.size and ind_s < aligned_s.size do
                if ( aligned_q[ind_q].include? $xyzrdelim and aligned_s[ind_s].include? $xyzrdelim) #aa
                        final_q << aligned_q[ind_q].split($xyzrdelim)[3]
                        final_s << aligned_s[ind_s].split($xyzrdelim)[3]
                        n_xyz_q << form_legal_tmscore_line(n_xyz_q.size+1, aligned_q[ind_q].chomp)
                        n_xyz_s << form_legal_tmscore_line(n_xyz_q.size, aligned_s[ind_s].chomp)
                        
                        ind_q += 1
                        ind_s += 1
                else 
                        ind_q += 1
                        ind_s += 1
                end
        end
        throw "misaligned!!output" if n_xyz_q.size != n_xyz_s.size
	begin
        	fs = File.new(namexyz(l_name,r_name,'s'),"w")
	        fq = File.new(namexyz(l_name,r_name,'q'),"w")
	rescue
        	sleep(1)
        	begin
        		fs = File.new(namexyz(l_name,r_name,'s'),"w")
		        fq = File.new(namexyz(l_name,r_name,'q'),"w")
	        rescue
        	        return
        	end
	end
        fs.puts n_xyz_s.join("\n")
        fq.puts n_xyz_q.join("\n")
        fq.close
        fs.close
        
        
        return getAlignmentMetrics(final_q,final_s)
end
#------------------------------------
def rmsd_one_not_hhp_hit(l_name,r_name,blast_offset_q,blast_offset_s,blast_q,blast_s)
        #puts "#{blast_offset_q},#{blast_offset_s}"
#        arr = l_name.gsub("_","").split('')
#        l_name = arr[0..-2].join('').downcase + arr[-1] 
#        arr = r_name.gsub("_","").split('')
#        r_name = arr[0..-2].join('').downcase + arr[-1]
        newxyzs = $xyzng_dir
        xyz_q = []
        xyz_s = []

	#domains specific
#	a = ((7-l_name.size).times.map {|i| '_'}).join
#	l_suffix = a+'.ent.xyzr' 
#        a = ((7-r_name.size).times.map {|i| '_'}).join
#	r_suffix = a+'.ent.xyzr'
	namingPattern = "#{newxyzs}/pattern.ent.xyzng"
	l_path = putBaseNameInPattern(l_name,namingPattern)
	r_path = putBaseNameInPattern(r_name,namingPattern)

        File.new(l_path).each { |r| xyz_q << r unless r.include? '#'}
        File.new(r_path).each { |r| xyz_s << r unless r.include? '#'}

	#puts xyz_q.join;    puts xyz_s.join
        construct_temp_not_hhp_xyz(xyz_q,xyz_s,blast_offset_q.to_i,blast_offset_s.to_i,blast_q.split(''),blast_s.split(''),
l_name,r_name) #note the change in q,s
        cmd = " #{$tmscore}  #{namexyz(l_name,r_name,'q')} #{namexyz(l_name,r_name,'s')}  | grep -e RMSD -e '-score' |  tail -6 | head -5 | cut -d'=' -f2"       

#        puts cmd
        tm = parse_tmscore_lines(%x[#{cmd}])
	%x[rm  #{namexyz(l_name,r_name,'q')} #{namexyz(l_name,r_name,'s')} ]
        return tm
end

#-------------------------------------------------------------------
def work_on_flat_not_hhp_pairs(flat_fname)
        f = File.new(flat_fname)
        out = File.new(flat_fname.gsub('new','_new')+'.res','w')
        while (line=f.gets)
                arr = line.split(' ')
		#arr[1].gsub!('.ent','')
		#arr[0].gsub!('.ent','')
                #next if arr.size < 13
                #puts "#{arr[7]},#{arr[9]},#{arr[11]},#{arr[12]}"
                tmscore_arr = rmsd_one_not_hhp_hit(arr[0], arr[1],arr[7],arr[9],arr[11].strip,arr[12].strip)
		#(arr[0], arr[1],arr[8],arr[10],arr[12].strip,arr[13].strip) #arr[7],arr[9],arr[11].strip,arr[12].strip)
                out.puts "#{arr[0]},#{arr[1]},#{tmscore_arr.split().join(',')}"
        end
        out.flush
        out.close
end

if __FILE__ == $0
work_on_flat_not_hhp_pairs(ARGV[0])
end 

