load 'astrals.rb'

dest = File.new(ARGV[0]+'.aln','w')
#ATOM      2  CA  ALA A   4      12.501  39.048  28.539  1.00 30.68           C 
runningResidue = 0
lastIndex = ''
File.new(ARGV[0]).each { |l|
	next unless l.include? 'ATOM' or l.include? 'HETATM'
	if(lastIndex!=l[22..25]) #new index
		runningResidue+=1
		lastIndex=l[22..25]
		dest.puts "#{runningResidue} #{l[22..25].to_i}"
		puts "#{runningResidue} #{l[17..19]} #{$astrals[l[17..19]]}"
	end
}
