from Bio.PDB.PDBParser import PDBParser
import sys
 
parser=PDBParser()

name=sys.argv[1]
chainL=sys.argv[2]
structure=parser.get_structure("test", name)
model=structure[0]
chain=model[chainL]
flag = 1
if chain.id==chainL:
   for residue in chain.get_list():
      flag=1
      #special code for disordered pairs
      if residue.is_disordered():
	try:  
	  for name in residue.disordered_get_id_list():
            residue.disordered_select(name)
            if "A" == residue["CA"].get_altloc():
	      flag = 1	
	except:
          flag = 0
          print "#"
      if flag==1:     
        if residue.has_id("CA"):
          ca=residue["CA"]
	  lst=ca.get_coord()
          print round(lst[0],3),
	  print round(lst[1],3),
	  print round(lst[2],3),
	  print residue.get_resname()
        else:
          print "#",
	  print residue.get_resname()
