$astrals= {
'ALA'=>'A','VAL'=>'V','PHE'=>'F','PRO'=>'P','MET'=>'M','ILE'=>'I','LEU'=>'L','ASP'=>'D','GLU'=>'E','LYS'=>'K','ARG'=>'R','SER'=>'S','THR'=>'T','TYR' =>'Y','HIS'=>'H','CYS'=>'C','ASN'=>'N','GLN'=>'Q','TRP'=>'W','GLY'=>'G','UNK'=>'X','2AS'=>'D','3AH'=>'H','5HP'=>'E','ACL'=>'R','AGM'=>'R','AIB'=>'A','ALM'=>'A','ALO'=>'T','ALY'=>'K','ARM'=>'R','ASA'=>'D','ASB'=>'D','ASK'=>'D','ASL'=>'D','ASQ'=>'D','ASX'=>'B','AYA'=>'A','BCS'=>'C','BHD'=>'D','BMT'=>'T','BNN'=>'A','BUC'=>'C','BUG'=>'L','C5C'=>'C','C6C'=>'C','CCS'=>'C','CEA'=>'C','CGU'=>'E','CHG'=>'A','CLE'=>'L','CME'=>'C','CSD'=>'A','CSO'=>'C','CSP'=>'C','CSS'=>'C','CSW'=>'C','CSX'=>'C','CXM'=>'M','CY1'=>'C','CY3'=>'C','CYG'=>'C','CYM'=>'C','CYQ'=>'C','DAH'=>'F','DAL'=>'A','DAR'=>'R','DAS'=>'D','DCY'=>'C','DGL'=>'E','DGN'=>'Q','DHA'=>'A','DHI'=>'H','DIL'=>'I','DIV'=>'V','DLE'=>'L','DLY'=>'K','DNP'=>'A','DPN'=>'F','DPR'=>'P','DSN'=>'S','DSP'=>'D','DTH'=>'T','DTR'=>'W','DTY'=>'Y','DVA'=>'V','EFC'=>'C','FLA'=>'A','FME'=>'M','GGL'=>'E','GL3'=>'G','GLX'=>'Z','GLZ'=>'G','GMA'=>'E','GSC'=>'G','HAC'=>'A','HAR'=>'R','HIC'=>'H','HIP'=>'H','HMR'=>'R','HPQ'=>'F','HTR'=>'W','HYP'=>'P','IIL'=>'I','IYR'=>'Y','KCX'=>'K','LLP'=>'K','LLY'=>'K','LTR'=>'W','LYM'=>'K','LYZ'=>'K','MAA'=>'A','MEN'=>'N','MHS'=>'H','MIS'=>'S','MLE'=>'L','MPQ'=>'G','MSA'=>'G','MSE'=>'M','MVA'=>'V','NEM'=>'H','NEP'=>'H','NLE'=>'L','NLN'=>'L','NLP'=>'L','NMC'=>'G','OAS'=>'S','OCS'=>'C','OMT'=>'M','PAQ'=>'Y','PCA'=>'E','PEC'=>'C','PHI'=>'F','PHL'=>'F','PR3'=>'C','PRR'=>'A','PTR'=>'Y','SAC'=>'S','SAR'=>'G','SCH'=>'C','SCS'=>'C','SCY'=>'C','SEL'=>'S','SEP'=>'S','SET'=>'S','SHC'=>'C','SHR'=>'K','SMC'=>'C','SOC'=>'C','STY'=>'Y','SVA'=>'S','TIH'=>'A','TPL'=>'W','TPO'=>'T','TPQ'=>'A','TRG'=>'K','TRO'=>'W','TYB'=>'Y','TYQ'=>'Y','TYS'=>'Y','TYY'=>'Y'
}

#ATOM      2  CA  ALA A   4      12.501  39.048  28.539  1.00 30.68           C 
runningResidue = 0
lastIndex = ''
residues = []
File.new(ARGV[0]).each { |l|
	break if l.include? 'ENDMDL'
        next unless l.include? 'ATOM' or l.include? 'HETATM'
        if(lastIndex!=l[22..25]) #new index
                runningResidue+=1
                lastIndex=l[22..25]
                residues << $astrals[l[17..19]]
        end
}
f = ARGV[0].split('/')[-1].split('.ent')[0]
start = false
hhRes = []
File.new("../hhmDomains/#{f}.hhm").each { |l| 
        break if start and l.include? '>' or l.include? '#'
        hhRes << l.chomp if start
        start = true if start==false and l.include?('>'+f)
}

#get masks:
start = false
hhMaskRes = []
File.new("../hhmDomains/#{f}.hhm").each { |l| 
        break if start and l.include? '>' 
        hhMaskRes << l.chomp if start
        start = true if start==false and l.include?('>ss_dssp')
}
hhMaskRes = hhMaskRes.join('').split('')
hhRes = hhRes.join('').split('')
unless hhMaskRes.size == hhRes.size
	puts f
	STDERR.puts hhRes.join('')
	STDERR.puts hhMaskRes.join('')
	exit
end	

maskedRes = []
(0..hhRes.size).each {|i|
	maskedRes << hhRes[i].to_s unless hhMaskRes[i] == '-'
}

unless residues.join('') == maskedRes.join('')
        puts f
        puts "|#{residues.join('')}|"
        puts "|#{maskedRes.join('')}|"
	puts "|#{hhMaskRes}|"
end


