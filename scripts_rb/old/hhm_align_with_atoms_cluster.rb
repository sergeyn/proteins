def map_fasta_to_atoms_seq fpath
hhm = File.basename(fpath).split('.')[0]
file = File.new(fpath)
line = file.gets
until line.include? '>' + hhm 
	line = file.gets
end
arr = []
until line.include? '>gi' or line.include? '#'
	line = file.gets
	raise fpath unless line
	line = line.chomp.strip
	arr << line
end
file.close
hhmseq =  arr[0..-2].join().split('')
connect_to_db

fasta =  get_fasta_by_name(hhm.split('_')[0],hhm.split('_')[1])
#print fasta.inspect
#puts ''
pdbseq = get_fasta_by_name(hhm.split('_')[0],hhm.split('_')[1]).split('')

alignment = needle(hhmseq,pdbseq)
#puts hhmseq.join
#puts alignment.join
index = 0
(0..alignment.size).each { |i|
  if alignment[i]=='-'
        print '-1,'
  else
     unless alignment[i]==hhmseq[i]
		print ' error!'
		STDERR.puts alignment[i-1..i+1].join ''
		STDERR.puts hhmseq[i-1..i+1].join ''
		STDERR.puts alignment.join ''
		STDERR.puts hhmseq.join ''
		STDERR.puts pdbseq.join ''
		#raise 'error'
		return
	end

     print index
     print ','
     index +=1
end
}

end



def convert_some
['/home/snepomny/rsync/hhms/*.hhm'].each { |f|
  name = File.basename(f).split('.')[0].gsub('_','')
  if seqresall[name]
        #$connection.query "delete from hhp_blast_pairs where l_name='#{name}' or r_name='#{name}'"
        print name
	print ','
	map_fasta_to_atoms_seq f
	puts ''
  end
}
end

def align_new inds_arr, seq_arr, offset
	new_seq = []
	new_offset = offset
	strike = "\\"
	seq_arr.each { |c| 
		break if c == strike
		new_seq << c if inds_arr[offset] != -1
		new_seq << '-' if inds_arr[offset] == -1
		offset +=1
	}

	new_seq.each { |c|
		break if c != '-'
		new_offset += 1
	}
	return [new_seq.join(''), new_offset]
end

def do_all fname, inds_file
#read all index lines and put to hash
hsh = {}
f = File.new inds_file
while line = f.gets
	arr = line.split ','
	hsh[arr[0]] = arr[1..-1]
end
#hsh.each_pair { |k,v| puts "[#{k}] #{v.join ''}" }

f = File.new ffname
#puts f.gets
while line=f.gets
	arr = line.split
	q  = arr[0]
	s = arr[1]
        #puts q + s
	
	left_q = arr[8].to_i
	left_s = arr[10].to_i
	seq_q = arr[12].chomp.split('')
	seq_s = arr[13].chomp.split('')
        if hsh[q] and hsh[s]	
		ans = align_new(hsh[q], seq_q, left_q)
		arr[12] = ans[0]
		arr[8] = ans[1]
		ans = align_new(hsh[s], seq_s, left_s)
	        arr[13]= ans[0]
	        arr[10] = ans[1]
		puts arr.join(',')
	end
end

end 
