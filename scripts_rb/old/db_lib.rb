$msql = 'mysql -u rsyncuser1 -D rsync -ppdb4all -e '

def connect_to_db
    flag = false
    while ! flag
            begin
            connect_to_proteins_db if ! $connection
            flag = true if  $connection
            rescue => err
            flag = false
            puts err
            puts "retrying after sleep"
            sleep(rand(7000))
            end
    end 
end
#-------------------------------------------------------------------
#sets up the connection to mysql database. $connection global var holds the handle
def connect_to_proteins_db
	
	if $on_rachel_lx or $on_rachel2
		begin
		require "/usr/local/lib/site_ruby/1.8/mysql.rb"
		rescue
		    raise "Could not load mysql"
 		end
		begin
		$connection  = Mysql::new("localhost", "rsyncuser1", "pdb4all", "rsync")
		rescue => err
			puts 'check /tmp/mysql.sock - perhaps there was a restart and need to do ln -s  /var/run/mysqld/mysqld.sock /tmp/mysql.sock '
		    raise "Could not load mysql: #{err}"
		end


	else
		require "mysql"
		$connection  = Mysql::new("localhost", "snepomny", "kolobok", "proteins")
 
	end
end
#-------------------------------------------------------------------
#------------------------------------------------------------------
def create_table_closeones prefix=''
 create_blast_pair_clone prefix + 'closeones'
end
#------------------------------------------------------------------
def create_table_leaders_pairs prefix=''
  create_blast_pair_clone prefix + 'leaders_pairs'
end
#-------------------------------------------------------------------------
def create_new_pairs_table name
begin
$connection.query " CREATE TABLE `#{name}` (
  `l_name` char(6) NOT NULL,
  `r_name` char(6) NOT NULL,
  `evalue` char(10) default NULL,
  `length` int(11) NOT NULL,
  `ident` int(10) unsigned NOT NULL,
  `similar` int(10) unsigned NOT NULL,
  `gaps` int(10) unsigned NOT NULL,
  `left_q` int(3) unsigned NOT NULL,
  `left_s` int(3) unsigned NOT NULL,
  `seq_q` text NOT NULL,
  `seq_s` text NOT NULL,
  `rmsd` float NOT NULL,
  `tmscore` float NOT NULL,
  `maxsub` float NOT NULL,
  `gdtts` float NOT NULL,
  `gdtha` float NOT NULL,
  `probab` float NOT NULL,
  `score` float NOT NULL,
  `scoreb` float NOT NULL,
  PRIMARY KEY  USING BTREE (`l_name`,`r_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
"
rescue => err
        log_me err
end
end
#--------------------------------------------------------------------
def create_blast_pair_clone name
begin
$connection.query " CREATE TABLE `#{name}` (
  `l_name` char(6) NOT NULL,
  `r_name` char(6) NOT NULL,
  `part` int(11) NOT NULL,
  `evalue` char(10) default NULL,
  `length` int(11) NOT NULL,
  `ident` int(10) unsigned NOT NULL,
  `positives` int(10) unsigned NOT NULL,
  `gaps` int(10) unsigned NOT NULL,
  `left_q` int(3) unsigned NOT NULL,
  `right_q` int(3) unsigned NOT NULL,
  `left_s` int(3) unsigned NOT NULL,
  `right_s` int(3) unsigned NOT NULL,
  `seq_q` text NOT NULL,
  `seq_s` text NOT NULL,
  `rmsd` float NOT NULL,
  `gdmt1` float NOT NULL,
  `gdmt2` float NOT NULL,
  `gdmt3` float NOT NULL,
  `tmscore` float NOT NULL,
  PRIMARY KEY  USING BTREE (`l_name`,`r_name`,`part`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
"
rescue => err
        log_me err
end
end

def create_table_blast_pairs prefix=''
	create_blast_pair_clone prefix + 'blast_pairs'
end
#-----------------------------------------------------------------------
def create_table_clusters prefix=''
begin
$connection.query  "CREATE TABLE `#{prefix}clusters` (
  `chain` char(6) NOT NULL,
  `leader` char(6) NOT NULL,
  PRIMARY KEY  (`chain`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1"
rescue => err
        log_me err
end
end
#-----------------------------------------------------------------------
def create_tables
begin
$connection.query  "CREATE TABLE `seqres` (
  `pdbname` char(6) NOT NULL,
  `chain` char(1) NOT NULL,
  `seqres` text,
  `prec` float default NULL,
  PRIMARY KEY  (`pdbname`,`chain`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1"
rescue => err
        log_me err
end
psi = 'psi_'
create_table_blast_pairs 'hhp_' 
create_table_clusters 
create_table_clusters psi
create_table_clusters 'hhp_'
create_table_blast_pairs
create_table_blast_pairs psi
create_table_closeones
create_table_closeones psi
create_table_closeones 'hhp_'
create_table_leaders_pairs
create_table_leaders_pairs psi
create_table_leaders_pairs 'hhp_'
end
#-------------------------------------------------------------------
#-------------------------------------------------------------------
def get_fasta_by_name(name,chain)
	res =  $connection.query "select seqres from seqres where pdbname = '#{name}' and chain='#{chain}' "
	res.each do |r| 
		return r[0]
	end
end
#-------------------------------------------------------------------
def write_fasta_to_file(name)
	seq = get_fasta_by_name(name)
	f = File.new(name+'.seq',"w")
	f.puts ">"+name
	f.puts seq
	f.flush
	f.close
end
#keys_count - first columns are a composite key to table_name - how many of them?
def insert_or_update(table_name,columns_arr,values_arr,keys_count,update_on_fail=true)
	raise "columns_arr.size != values_arr.size" if columns_arr.size != values_arr.size
	#insert into table_name (column1, column2, ...) values (value1, value2 ...);
	#UPDATE table_name SET column1=value, column2=value2,... WHERE some_column=some_value;
        q_insert = "insert into #{table_name} (#{columns_arr.join(", ")}) values (#{values_arr.join(",")})" 
	begin
		$connection.query q_insert
	rescue => err
		STDERR.puts  "insert_failed on #{q_insert} with #{err}" 
		where_clause = []
		set_clause = []
		(0..keys_count).each do |i|
			where_clause << "#{columns_arr[i]}=#{values_arr[i]}"
		end
		(keys_count..columns_arr.size-1).each do |i|
			set_clause << "#{columns_arr[i]}=#{values_arr[i]}"
		end
		begin
			$connection.query "update #{table_name} set #{set_clause.join(", ")} where #{where_clause.join(" and ")}" if update_on_fail
		rescue => err
			err_log "update #{table_name} set #{set_clause.join(", ")} where #{where_clause.join(" and ")}"
		end
	end
end
#-------------------------------------------------------------------
def insert_or_update_seqres(name, chain, seqres, precision)
	insert_or_update("seqres", ['pdbname', 'chain', 'seqres','prec'], ["'#{name}'","'#{chain}'","'#{seqres}'","#{precision}"],1)
end

#-------------------------------------------------------------------
# def convert_from_fasta_name_to_filename name
# 	#101m_A => pdb101m.ent (and .gz if gzipped)
# 	return [ "pdb" + name.split("_")[0] + ".ent", name.split("_")[1].upcase]
# end
#-------------------------------------------------------------------
def get_some_chain_names(limit=123456)
	query_string = "select l_name from chain_pairs  group by l_name limit #{limit.to_s};"
	res = $connection.query query_string
	results_arr = []
	res.each do |line|
		results_arr << ( convert_from_fasta_name_to_filename line[0])
	end
	return results_arr
end
#-------------------------------------------------------------------
def chains_iterate(where_clause='', limit=99123456, &block)
	query_string = "select pdbname,chain,seqres from seqres #{where_clause} limit #{limit.to_s};"
	res = $connection.query query_string
	res.each do |line|
		# name,chain,sequence
		yield line[0],line[1],line[2]
	end
end
#-------------------------------------------------------------------
def get_table_count name
	query_string = "select count(*) from #{name}"
	res = $connection.query query_string
	res.each do |line|
		return line[0].to_i
	end
end
#-------------------------------------------------------------------
def update_len_in_pairs(l,r,len)
	 #UPDATE table_name SET column1=value, column2=value2,... WHERE some_column=some_value;
	$connection.query "update blast_pairs set length=#{len} where l_name='#{l}' and r_name='#{r}'" 

end
#-------------------------------------------------------------------
def full_new_blast_clone_iterator clonename
        limit=0
        blast_pairs_count=get_table_count clonename
#       STDERR.puts blast_pairs_count
        while limit<blast_pairs_count do
                STDERR.puts limit
                query_string = "select * from #{clonename} limit #{limit},100000"
                res = $connection.query query_string
                res.each do |line|
yield line[0],line[1],line[2],line[3],line[4],line[5],line[6],line[7],line[8],line[9],line[10],line[11],line[12],line[13],line[14],line[15],line[16],line[17],line[18] #,line[19],line[20]
        end
        limit +=100000
end #while
end
#------------------------------------------------------------
def full_blast_clone_iterator clonename
	limit=0
	blast_pairs_count=get_table_count clonename
#	STDERR.puts blast_pairs_count
	while limit<blast_pairs_count do
		STDERR.puts limit
		query_string = "select * from #{clonename} limit #{limit},100000"
		res = $connection.query query_string
		res.each do |line|
#l_name,r_name,part,evalue,length,ident,positives,gaps,left_q,right_q,left_s,right_s,seq_q,seq_s,rmsd,gdmt1,gdmt2,gdmt3
			yield line[0],line[1],line[2],line[3],line[4],line[5],line[6],line[7],line[8],line[9],line[10],line[11],line[12],line[13],line[14],line[15],line[16],line[17],line[18] #,line[19],line[20]
	end
	limit +=100000
end #while
end

#-------------------------------------------------------------------
def select_blast_clone_each clonename, where=''
        query_string = "select * from #{clonename} where #{where}"
        res = $connection.query query_string
        res.each do |line|
#l_name,r_name,part,evalue,length,ident,positives,gaps,left_q,right_q,left_s,right_s,seq_q,seq_s,rmsd,gdmt1,gdmt2,gdmt3
                yield line[0],line[1],line[2],line[3],line[4],line[5],line[6],line[7],line[8],line[9],line[10],line[11],line[12],line[13],line[14],line[15],line[16],line[17],line[18]
        end
end

#-------------------------------------------------------------------
def print_full_blast_clone clonename
	full_blast_clone_iterator(clonename) {

	|line0,line1,line2,line3,line4,line5,line6,line7,line8,line9,line10,line11,line12,line13,line14,line15,line16,line17,line18|

    puts [ line0.chomp,line1.chomp,line2.chomp,line3.chomp,line4.chomp,line5.chomp,line6.chomp,line7.chomp,line8.chomp,line9.chomp,line10.chomp,line11.chomp,line12.chomp,line13.chomp,line14.chomp,line15.chomp,line16.chomp,line17.chomp,line18.chomp ].join(",")
}
end
#-------------------------------------------------------------------

def blast_hits_iterate(limit=99123456, where_clause='', prefix='', &block)
	query_string = "select l_name,r_name,left_q,left_s,seq_q,seq_s from #{prefix}blast_pairs #{where_clause} limit #{limit.to_s};"
	res = $connection.query query_string
	res.each do |line|
		# l_name,r_name,left_q,left_s,seq_q,seq_s
		yield line[0],line[1],line[2],line[3],line[4],line[5]
	end
end
#-------------------------------------------------------------------
def insert_new_pair table_name,hit_hash
	insert_or_update(table_name, ['l_name', 'r_name', 'evalue','length','ident','similar','left_q','left_s','seq_q','seq_s','rmsd','tmscore','maxsub','gdtts','gdtha','probab','score','scoreb'], ["'#{hit_hash[:l_name]}'","'#{hit_hash[:r_name]}'","'#{hit_hash[:evalue]}'","#{hit_hash[:length].to_s}","#{hit_hash[:ident].to_s}", "#{hit_hash[:similar].to_s}","#{hit_hash[:left_q]}","#{hit_hash[:left_s]}","'#{hit_hash[:seq_q]}'", "'#{hit_hash[:seq_s]}'","#{hit_hash[:rmsd]}","#{hit_hash[:tmscore]}","#{hit_hash[:maxsub]}","#{hit_hash[:gdtts]}","#{hit_hash[:gdtha]}","#{hit_hash[:probab]}", "#{hit_hash[:score]}",  "#{hit_hash[:scoreb]}" ],1,false)
	
end
#-------------------------------------------------------------------

#will take hit_hash and insert the data in DB
def insert_pair(query_name,hit_hashs_arr,prefix='')
 	counter = -1
 	
  	hit_hashs_arr.each do |hit_hash|
  		pair = [query_name,hit_hash[:r_name]].sort #sort lex. the names of chains
		return if pair[0]==pair[1] #we don't want self-similarity

		left_q = hit_hash[:left_q]
		right_q = hit_hash[:right_q]
		left_s = hit_hash[:left_s]
		right_s = hit_hash[:right_s]
		seq_q = hit_hash[:seq_q]
		seq_s = hit_hash[:seq_s]
		#but, alas, the sorting creates the following problem - we maybe have to swap q and s data:
		if query_name==pair[1] #swap
			left_q = hit_hash[:left_s]
			right_q = hit_hash[:right_s]
			left_s = hit_hash[:left_q]
			right_s = hit_hash[:right_q]
			seq_q = hit_hash[:seq_s]
			seq_s = hit_hash[:seq_q]
		end
	
  		counter = counter+1
		insert_or_update("#{prefix}blast_pairs", ['l_name', 'r_name', 'part','evalue','length','ident','positives','gaps','left_q','right_q','left_s','right_s','seq_q','seq_s','rmsd','gdmt1','gdmt2','gdmt3','tmscore'], ["'#{pair[0]}'","'#{pair[1]}'","#{counter.to_s}","'#{hit_hash[:evalue]}'","#{hit_hash[:length].to_s}","#{hit_hash[:ident].to_s}", "#{hit_hash[:positives].to_s}","#{hit_hash[:gaps].to_s}","#{left_q}","#{right_q}","#{left_s}","#{right_s}","'#{seq_q}'", "'#{seq_s}'","#{hit_hash[:rmsd]}","#{hit_hash[:gdmt1]}","#{hit_hash[:gdmt2]}","#{hit_hash[:gdmt3]}","#{hit_hash[:tmscore]}" ],1,false)
			
#  			q_string = "insert into blast_pairs (l_name, r_name, part, evalue, length, ident, positives, gaps, left_q, right_q, left_s, right_s, seq_q, seq_s) values ('#{pair[0]}','#{pair[1]}',#{counter.to_s},#{hit_hash[:evalue]},#{hit_hash[:length]},#{hit_hash[:ident]}, #{hit_hash[:positives]}, #{hit_hash[:gaps]},#{left_q},#{right_q},#{left_s},#{right_s}, '#{seq_q}', '#{seq_s}' )"

 	end
end
#-------------------------------------------------------------------
def dump_blast_hits_to_file(fname,prefix='')
	f = File.new(fname,'w')
	counter = 0
	blast_hits_iterate(prefix) { |l_name,r_name,blast_offset_q,blast_offset_s,blast_q,blast_s|
		f.puts "#{l_name} #{r_name} #{blast_offset_q} #{blast_offset_s} #{blast_q} #{blast_s}"
		puts counter if 1 == 1001 % counter+=1
	}
	f.flush
	f.close
end
#-------------------------------------------------------------------
def dump_blast_as_flat_to_file(fname='',prefix='')
#flat is:
#l_name r_name eval length ident pos gaps left_s right_s left_q right_q seq_s seq_q 
query_string = "select l_name,r_name,evalue,length,ident,positives,gaps,left_s,right_s,left_q,right_q,seq_q,seq_q from #{prefix}blast_pairs where length>999;"
	res = $connection.query query_string
	res.each do |line|
		# l_name,r_name,left_q,left_s,seq_q,seq_s
		puts line.join(',')
	end

end
#-------------------------------------------------------------------
def closeones_iterate(where_clause='', limit=99123456, prefix='', &block)

	query_string = "select l_name,r_name from #{prefix}closeones #{where_clause} limit #{limit.to_s};"
	res = $connection.query query_string
	res.each do |line|
		# name,chain,sequence
		yield line[0],line[1]
	end
end
#-------------------------------------------------------------------
#
#  Functions for clustering
#
#-------------------------------------------------------------------
def add_parzen_pair parzen, length, ident, rmsd, similarity='0'
   (!parzen[ident]) and parzen[ident] = {}
   (!parzen[ident][length]) and parzen[ident][length] = {}
   (!parzen[ident][length][similarity]) and parzen[ident][length][similarity] = [0,0]

   node = parzen[ident][length][similarity]
   r = rmsd.to_f
   if r>4
      node[0]+=1
   else
      node[1]+=1
   end	
 end
#-------------------------------------------------------------------
def build_smallest_parzens prefix=''
	parzen = {}
counter= 0
	full_blast_clone_iterator(prefix+'leaders_pairs') { |l_name,r_name,part,evalue,length,ident,positives,gaps,left_q,right_q,left_s,right_s,seq_q,seq_s,rmsd,gdmt1,gdmt2,gdmt3,tmscore|
		add_parzen_pair parzen, length, ident, rmsd 
		counter+=1
		STDERR.puts counter if counter%100123 == 1
}
	parzen.each_pair do |ident,ihsh|
	    ihsh.each_pair do |length,shsh|
		shsh.each_pair do |sim,arr|
        		puts "#{arr[0]} #{arr[1]}" 
		end
	   end
	end
end
#-------------------------------------------------------------------
def get_closeones(chain, ident, rmsd,prefix)
	query = "select  l_name,r_name from #{preifx}blast_pairs where (l_name like '#{chain}%' or r_name like '#{chain}%') and ident > #{ident} and rmsd < #{rmsd} order by ident desc"
	res = $connection.query query
	arr = []
	arr << chain #itself is the closest
	res.each do |row|
		arr << ((row[0]==chain) ? row[1] : row[0])
		
	end
	
	return arr
end
#-------------------------------------------------------------------
def get_leaders(prefix='')
	query = "select  distinct leader as l,',' from #{prefix}clusters"
	res = $connection.query query
	arr = {}
	res.each do |row|
		arr[row[0]]=1
	end
	
	return arr	
end
#-------------------------------------------------------------------
def get_closeones_strict(chain,prefix='')
	query = "select  l_name,r_name from #{prefix}closeones where (l_name='#{chain}' or r_name='#{chain}') order by ident desc"
	res = $connection.query query
	arr = []
	arr << chain #itself is the closest
	res.each do |row|
		arr << ((row[0]==chain) ? row[1] : row[0])
		
	end
	
	return arr
end
#-------------------------------------------------------------------	
def select_leader_from_relatives(arr)
	return arr[0]
end
#-------------------------------------------------------------------
def update_clusters_pair(chain,leader,prefix='')
	insert_or_update("#{prefix}clusters", ['chain', 'leader'], ["'#{chain}'","'#{leader}'"],0)
end
#-------------------------------------------------------------------
def build_clusters!(prefix='',ident=99,rmsd=1)
	 $connection.query "delete from #{prefix}clusters"
	chain_leader = {}
	counter = 0
	chains_iterate { |pdb,chain|
		counter += 1
		log_me counter.to_s if counter%1024 == 0
 		next if chain_leader[pdb+chain]
 		arr = get_closeones_strict(pdb+chain,prefix)
 		leader = select_leader_from_relatives(arr)
 		arr.each do |a|
 			chain_leader[a] = leader
 		end
 	}
	
	chain_leader.each_pair { |c,l|
		update_clusters_pair(c,l,prefix)
	}
	


end
#-------------------------------------------------------------------
#make dundant set
def reduce_blast_pairs_to_leaders prefix=''
	hsh = get_leaders prefix
        cnt = 0
	full_blast_clone_iterator(prefix+'blast_pairs') { |l_name,r_name,part,evalue,length,ident,positives,gaps,left_q,right_q,left_s,right_s,seq_q,seq_s,rmsd,gdmt1,gdmt2,gdmt3,tmscore|
		cnt += 1
		puts cnt.to_s if cnt%1000==0
		insert_or_update("#{prefix}leaders_pairs", ['l_name', 'r_name', 'part','evalue','length','ident','positives','gaps','left_q','right_q','seq_q','seq_s','rmsd','gdmt1','gdmt2','gdmt3','tmscore'], ["'#{l_name}'","'#{r_name}'",part,"'#{evalue}'",length,ident,positives,gaps,left_q,right_q,"'#{seq_q}'","'#{seq_s}'",rmsd,gdmt1,gdmt2,gdmt3,tmscore],0) if ( hsh[r_name] and hsh[l_name])
	}
end	


#-------------------------------------------------------------------
def rebuild_db_after_blast_update prefix=''
	$connection.query "delete from #{prefix}closeones"
	$connection.query "insert into #{prefix}closeones (select * from #{prefix}blast_pairs where ident>99 and rmsd<=2)"
	build_clusters! prefix
	$connection.query "delete from #{prefix}leaders_pairs"
	reduce_blast_pairs_to_leaders prefix
end
#-------------------------------------------------------------------
def set_rfactor_for_protein(name,rfactor)
	$connection.query "update seqres set rfactor=#{rfactor} where pdbname='#{name.downcase}'"
end
#-------------------------------------------------------------------
def histogram_chains
 arr = File.new('list_of_hhms_that_we_have').map { |line| line.chomp.gsub('_','') }
 tbl = 'psi_hhp_fr_blast_pairs'
 arr.each { |chain|
        print chain + ','
	puts ($connection.query "select count(*) from #{tbl} where (l_name='#{chain}' or r_name='#{chain}') and rmsd<2.5").map{|row| row[0]}
 }
end

def reduce_blast_to_hhp_friendly(list_of_small, full_table, small_table)
	arr = File.new(list_of_small).map { |line| line.chomp.gsub('_','') }
	hsh = {}
	arr.each {|s| hsh[s]=0}
	create_blast_pair_clone small_table
	puts hsh.size
	puts hsh.keys[0..10].join("\n")
	cnt = 0
	full_blast_clone_iterator(full_table) { |l_name,r_name,part,evalue,length,ident,positives,gaps,left_q,right_q,left_s,right_s,seq_q,seq_s,rmsd,gdmt1,gdmt2,gdmt3,tmscore,scoreb,score|
                cnt += 1
                puts cnt.to_s if cnt%1000==0
		puts l_name
                insert_or_update(small_table, ['l_name', 'r_name', 'part','evalue','length','ident','positives','gaps','left_q','right_q','seq_q','seq_s','rmsd','gdmt1','gdmt2','gdmt3','tmscore','scoreb','score'], ["'#{l_name}'","'#{r_name}'",part,"'#{evalue}'",length,ident,positives,gaps,left_q,right_q,"'#{seq_q}'","'#{seq_s}'",rmsd,gdmt1,gdmt2,gdmt3,tmscore,score,scoreb],0) if ( hsh[r_name] and hsh[l_name])
        }
end
#-------------------------------------------------------------------
def reduce_blastclone_new_with_oracle(big_table,small_table,oracle)
	create_new_pairs_table small_table
        full_new_blast_clone_iterator(big_table) { |l_name,r_name,evalue,length,ident,similar,gaps,left_q,left_s,seq_q,seq_s,rmsd,tmscore,maxsub,gdtts,gdtha,probab,score,scoreb|
	insert_or_update(small_table, ['l_name', 'r_name', 'evalue','length','ident','similar','gaps','left_q','left_s','seq_q','seq_s','rmsd','tmscore','maxsub','gdtts','gdtha','probab','score','scoreb'], ["'#{l_name}'","'#{r_name}'","'#{evalue}'","#{length.to_s}","#{ident.to_s}", "#{similar.to_s}","#{gaps.to_s}","#{left_q}","#{left_s}","'#{seq_q}'", "'#{seq_s}'","#{rmsd}","#{tmscore}","#{maxsub}","#{gdtts}","#{gdtha}","#{probab}", "#{score}",  "#{scoreb}" ],1,false) if oracle.call(l_name,r_name)

        }

end
#----------------------------------------------------------------------------------
def reduce_blastclone_with_oracle(big_table,small_table,oracle)
	create_blast_pair_clone small_table
	full_blast_clone_iterator(big_table) { |l_name,r_name,part,evalue,length,ident,positives,gaps,left_q,right_q,left_s,right_s,seq_q,seq_s,rmsd,gdmt1,gdmt2,gdmt3,tmscore|
#insert_or_update(small_table, ['l_name', 'r_name', 'part','evalue','length','ident','positives','gaps','left_q','right_q','seq_q','seq_s','rmsd','gdmt1','gdmt2','gdmt3','tmscore'], ["'#{l_name}'","'#{r_name}'",part,"'#{evalue}'",length,ident,positives,gaps,left_q,right_q,"'#{seq_q}'","'#{seq_s}'",rmsd,gdmt1,gdmt2,gdmt3,tmscore],0)  if oracle.call(l_name,r_name) 
	puts "#{l_name},#{r_name},#{rmsd},#{tmscore},#{gdmt1},#{gdmt2},#{gdmt3}" if oracle.call(l_name,r_name)

	}

end
#-------------------------------------------------------------------
def join_blast_and_hhpfriendly
cnt = 0
flag = true
        full_blast_clone_iterator('blast_hhp_friendly') { |l_name,r_name,part,evalue,length,ident,positives,gaps,left_q,right_q,left_s,right_s,seq_q,seq_s,rmsd,gdmt1,gdmt2,gdmt3,tmscore|
                cnt += 1
                puts cnt.to_s if cnt%1000==0
                #print [l_name,r_name,evalue,length,ident,positives,gaps,left_q,right_q,rmsd,gdmt1,gdmt2,gdmt3,tmscore].join(',')
		select_blast_clone_each('hhp_blast_pairs',"l_name='#{l_name}' and r_name='#{r_name}'")  { |l_name,r_name,part,evalue,length,ident,positives,gaps,left_q,right_q,left_s,right_s,seq_q,seq_s,rmsd,gdmt1,gdmt2,gdmt3,tmscore|
			print [l_name,r_name,evalue,length,ident,positives,gaps,left_q,right_q,rmsd,gdmt1,gdmt2,gdmt3,tmscore].join(',')
			flag = true
		}
	puts [evalue,length,ident,positives,gaps,left_q,right_q,rmsd,gdmt1,gdmt2,gdmt3,tmscore].join(',') if flag
        flag = false		
 
        }
end

#-------------------------------------------------------------------
def print_hhp_friendly_seqres
        arr = File.new('list_of_hhms_that_we_have').map { |line| line.chomp.gsub('_','') }
	hsh = {}
        arr.each {|s| hsh[s]=0}
	chains_iterate {|pdbname,chain,seq|
		name = pdbname + chain
		puts ">#{name}\n#{seq}" if hsh[name]
	}
end
#-------------------------------------------------------------------
def delta_by_length
# "select length,rmsd from hhp_blast_pairs where l_name='1uylA' and r_name='1yc1A'"
File.new(ARGV[0]).each { |l| 
	arr = l.split #(',') 
	res =  $connection.query  "select '#{arr[0]}','#{arr[1]}',#{arr[2]},#{arr[3]},length,rmsd from hhp_blast_pairs where l_name='#{arr[0]}' and r_name='#{arr[1]}'"
	res.each { |line| puts line.join "," }
}


end
