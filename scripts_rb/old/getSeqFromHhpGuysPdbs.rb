#does not use bio ruby lib
$missingFiles = '/home/trachel/twilight3/missing_for_18k'
$pdbsDir = '/home/trachel/twilight3/pdb70_16Jun12.pdb/'

$startSeq = 'REMARK    >'
$atom = 'ATOM'

$astrals= {
'ALA'=>'A','VAL'=>'V','PHE'=>'F','PRO'=>'P','MET'=>'M','ILE'=>'I','LEU'=>'L','ASP'=>'D','GLU'=>'E','LYS'=>'K','ARG'=>'R','SER'=>'S','THR'=>'T','TYR'=>'Y','HIS'=>'H','CYS'=>'C','ASN'=>'N','GLN'=>'Q','TRP'=>'W','GLY'=>'G','UNK'=>'X','2AS'=>'D','3AH'=>'H','5HP'=>'E','ACL'=>'R','AGM'=>'R','AIB'=>'A','ALM'=>'A','ALO'=>'T','ALY'=>'K','ARM'=>'R','ASA'=>'D','ASB'=>'D','ASK'=>'D','ASL'=>'D','ASQ'=>'D','ASX'=>'B','AYA'=>'A','BCS'=>'C','BHD'=>'D','BMT'=>'T','BNN'=>'A','BUC'=>'C','BUG'=>'L','C5C'=>'C','C6C'=>'C','CCS'=>'C','CEA'=>'C','CGU'=>'E','CHG'=>'A','CLE'=>'L','CME'=>'C','CSD'=>'A','CSO'=>'C','CSP'=>'C','CSS'=>'C','CSW'=>'C','CSX'=>'C','CXM'=>'M','CY1'=>'C','CY3'=>'C','CYG'=>'C','CYM'=>'C','CYQ'=>'C','DAH'=>'F','DAL'=>'A','DAR'=>'R','DAS'=>'D','DCY'=>'C','DGL'=>'E','DGN'=>'Q','DHA'=>'A','DHI'=>'H','DIL'=>'I','DIV'=>'V','DLE'=>'L','DLY'=>'K','DNP'=>'A','DPN'=>'F','DPR'=>'P','DSN'=>'S','DSP'=>'D','DTH'=>'T','DTR'=>'W','DTY'=>'Y','DVA'=>'V','EFC'=>'C','FLA'=>'A','FME'=>'M','GGL'=>'E','GL3'=>'G','GLX'=>'Z','GLZ'=>'G','GMA'=>'E','GSC'=>'G','HAC'=>'A','HAR'=>'R','HIC'=>'H','HIP'=>'H','HMR'=>'R','HPQ'=>'F','HTR'=>'W','HYP'=>'P','IIL'=>'I','IYR'=>'Y','KCX'=>'K','LLP'=>'K','LLY'=>'K','LTR'=>'W','LYM'=>'K','LYZ'=>'K','MAA'=>'A','MEN'=>'N','MHS'=>'H','MIS'=>'S','MLE'=>'L','MPQ'=>'G','MSA'=>'G','MSE'=>'M','MVA'=>'V','NEM'=>'H','NEP'=>'H','NLE'=>'L','NLN'=>'L','NLP'=>'L','NMC'=>'G','OAS'=>'S','OCS'=>'C','OMT'=>'M','PAQ'=>'Y','PCA'=>'E','PEC'=>'C','PHI'=>'F','PHL'=>'F','PR3'=>'C','PRR'=>'A','PTR'=>'Y','SAC'=>'S','SAR'=>'G','SCH'=>'C','SCS'=>'C','SCY'=>'C','SEL'=>'S','SEP'=>'S','SET'=>'S','SHC'=>'C','SHR'=>'K','SMC'=>'C','SOC'=>'C','STY'=>'Y','SVA'=>'S','TIH'=>'A','TPL'=>'W','TPO'=>'T','TPQ'=>'A','TRG'=>'K','TRO'=>'W','TYB'=>'Y','TYQ'=>'Y','TYS'=>'Y','TYY'=>'Y'
}

def handleSingleFile(fname)
	#seqres = []
	atoms = []
	f = File.new(fname).map {|l| l.chomp }
	i = 0
	#while(not f[i].include?($startSeq)) 
	#	i+=1 
	#end
	#i += 1
	while(not f[i].index($atom)==0) 
		#seqres << f[i].split()[-1].chomp
		i +=1
	end
	while(i<f.size && f[i].include?($atom)) 
		atoms << f[i][17..19] if f[i].index('CA')==13
		i+=1
	end

	seqsFromAtoms = atoms.map { |atomTriad| $astrals[atomTriad] || '_'}
	return '>'+fname.split('/')[-1].split('.')[0].gsub('_','')+"\n"+seqsFromAtoms.join('')
end

def handleFileList(list=$missingFiles)
  File.new(list).each { |l|
	missingFile = l.chomp
	pdbFile = missingFile.insert(-2, '_')
	#$pdbsDir + pdbFile + '.pdb' 
	File.new(l.chomp.upcase + '.seq','w').puts(handleSingleFile $pdbsDir + pdbFile + '.pdb')
  }
end

def pdb2FastaSeq(file)
	puts handleSingleFile(file)
end

def compareFastaAndOwnConversion(fastafile,pdbfile)
	fasta = IO.readlines(fastafile)
	puts  "#{fastafile} #{pdbfile}"  unless fasta[1].chomp == handleSingleFile(pdbfile).split("\n")[1].chomp
end

Dir['*.fasta'].each { |fasta| compareFastaAndOwnConversion(fasta, '/home/trachel/twilight3/6k_pdbs/'+ fasta.split('.')[0]+'.pdb')}if ARGV.empty?
pdb2FastaSeq(ARGV[0]) if __FILE__ == $0 
