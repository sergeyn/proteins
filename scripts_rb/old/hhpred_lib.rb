kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'
#require kernel_path+'pdb_my_lib.rb'
require kernel_path+'log_lib'
#require kernel_path+'env'
#require kernel_path+'db_lib'
#load 'mystringio.rb'

#-----------------------------------------------------------------------------------
def do_with_lines(filename=ARGV[0],limit=123456789, &block) #todo: move to utils lib
	file = File.new(filename,'r')
	counter = 0
	while line = file.gets do
		yield line		
		counter = counter + 1
		break if counter > limit
		#puts counter.to_s if(counter%1023==1)
	end
end

#
def iterate_hits_of_single_hhp_query q, &block
	stringHandler = Mystringio.new(q)
	file = stringHandler.filehandle
	counter = 0
	hit = []
	h_name='nil'
	while line = file.gets
		#puts "!#{line.chomp}"
		if line.include? '>' #starting new hit
			#puts '!! start'
			yield hit.join(""),h_name  unless counter < 1
			h_name= line.split()[0].split('>')[-1] # -- not in domains! .gsub('_','')
			#puts h_name
			counter += 1
			hit = []
		end
		hit << line.to_s
	end #while
	stringHandler.kill
	yield hit.join(""),h_name
end
#---------------------------------------------------
#Hit may contain several parts. Every part starts with
#Probab=1.00  E-value=1  Score=311.74  Aligned_cols=129  Identities=100%  Similarity=1.392  Sum_probs=123.3 
#and has an alignment block (see parser for alignment blocks)
def parse_hhp_hit(r_name, h)
	stringHandler = Mystringio.new(h)
        file = stringHandler.filehandle
	array_of_hits = []
	while true
		hit_hash = {}
		hit_hash[:r_name] = r_name
		
		line = skip_io_until(file,'Identities')
		if line == nil
			stringHandler.kill
			return array_of_hits
		end
		hit_hash[:evalue] = line.split("E-value=")[1].split()[0]
		hit_hash[:gaps] = 0
		hit_hash[:ident] = line.split("Identities=")[1].split("%")[0] 
		hit_hash[:similar] = line.split("Similarity=")[1].split()[0]
		hit_hash[:score] = line.split("Score=")[1].split()[0]
		hit_hash[:probab]=line.split("Probab=")[1].split()[0]
			
		align_data = parse_hhp_alignment_block(file)
		hit_hash[:left_q]=align_data[0]
		hit_hash[:right_q]=align_data[1]
		hit_hash[:left_s]=align_data[2]
		hit_hash[:right_s]=align_data[3]
		hit_hash[:seq_q] = align_data[4]
		hit_hash[:seq_s]=align_data[5]
		hit_hash[:length] = length_of_alignment(hit_hash[:seq_q].split(''),hit_hash[:seq_s].split(''))
		array_of_hits << hit_hash
	end
	stringHandler.kill
end

#---------------------------------------------------
#Q 155c_A            2 NEGDAAKGEKEFNKCKACHMIQAPDGTD-IKGGKTGPNLYGVVGRKIASEEGFKYGEGILEVAEKNPDLTWTEANLIEYV   80 (135)
#                      +.||+++|+++|++|++||+|+.+++.. ..++++||+|+|||||++|+++||+||++|...  +.++++||+++|++||
#T 1c2n_A           20 FAGDAAKGEKEFNKCKTCHSIIAPDGTEIVKGAKTGPNLYGVVGRTAGTYPEFKYKDSIVAL--GASGFAWTEEDIATYV   97 (137)
#
#
#Q 155c_A           81 TDPKPLVKKMTDDKGAKTKMTFKMGKNQADVVAFLAQDD  119 (135)
#                      +||++||+...++..|||+|+|++++||+||||||+|.+
#T 1c2n_A           98 KDPGAFLKEKLDDKKAKTGMAFKLAKGGEDVAAYLASVV  136 (137)

def parse_hhp_alignment_block(file)
	seq_q = []
	seq_s = []
	line = skip_io_until(file,'Q ') 
	while line do
		parts = line.split
		left_q = parts[2].to_i if !left_q
		right_q = parts[4].to_i 
		seq_q << parts[3] 
		
		line = skip_io_until(file,'T ') 
		parts = line.split
		left_s = parts[2].to_i if !left_s
		right_s = parts[4].to_i 
		seq_s << parts[3]
		
		line = skip_io_until(file,'Q ','Score') 
		line = nil if !line or line.include? 'Score'
	end
	return [left_q.to_s, right_q.to_s, left_s.to_s, right_s.to_s, seq_q.join(""),seq_s.join("")]
end

def length_of_alignment(q,s)
	counter = 0
	i = 0
	q.size.times do 
		counter +=1 if q[i]!= '-' and s[i]!='-'
		i+=1
	end
	return counter
end

#-------------------------------------------------------------------
def print_pair(query_name,hit_hash)
	pair = [query_name,hit_hash[:r_name]].sort #sort lex. the names of chains
	return if pair[0]==pair[1] #we don't want self-similarity
	#return unless $global_names[pair[0]] and $global_names[pair[1]]
	#return if hit_hash[:evalue].to_f >1
	#return if hit_hash[:probab].to_f < 30
	left_q = hit_hash[:left_q]
	right_q = hit_hash[:right_q]
	left_s = hit_hash[:left_s]
	right_s = hit_hash[:right_s]
	seq_q = hit_hash[:seq_q]
	seq_s = hit_hash[:seq_s]
	#but, alas, the sorting creates the following problem - we maybe have to swap q and s data:
	if query_name==pair[1] #swap
		left_q = hit_hash[:left_s]
		right_q = hit_hash[:right_s]
		left_s = hit_hash[:left_q]
		right_s = hit_hash[:right_q]
		seq_q = hit_hash[:seq_s]
		seq_s = hit_hash[:seq_q]
	end
	#1rz2A,1rz2A,2e-99,214,100,100,0,1,214,1,214,
	log_me "#{pair[0]},#{pair[1]},#{hit_hash[:evalue]},#{hit_hash[:length]},#{hit_hash[:ident].to_s},#{hit_hash[:similar].to_s},#{hit_hash[:gaps].to_s},#{left_q},#{right_q},#{left_s},#{right_s},#{seq_q},#{seq_s},#{hit_hash[:probab]},#{hit_hash[:score]}"
end
#-------------------------------------------------------------------

def parse_hhp(fname,threshold=15)
	q = %x[cat #{fname}]
	q_name = File.basename(fname).gsub('.hhr','') 
	iterate_hits_of_single_hhp_query(q) { |h,p_name| 
		hit_hash = parse_hhp_hit(p_name,h)[0]
		#begin
		print_pair(q_name,hit_hash) #if hit_hash[:ident].to_i>threshold and hit_hash[:length].to_i>30 
		#rescue => e
		#	puts "#{e} #{q_name} #{hit_hash.inspect}"
#		throw 'errr'	
		#	exit
		#end
	}
end

# main
#files = Dir['/shared/home/snepomny/runs/hhms/*.hhm'].sort()
#thread_id = ENV['PBS_ARRAYID'].to_i
#set_l_to_file("/shared/home/snepomny/runs/hhpflat#{thread_id}")
#((files.size()/40*thread_id)..(files.size()/40*(thread_id+1))).each { |fnum|
#	fname =  files[fnum] 
#	break unless fname
#	%x[/shared/home/snepomny/runs/hhpred/hhsearch -i #{fname} -d /shared/home/snepomny/runs/big.hhm -b 23123 -nocons -nopred -nodssp]
#	parse_hhp fname.gsub('.hhm','.hhr')
#	log_me fname
#}
