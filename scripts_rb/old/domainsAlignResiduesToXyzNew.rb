scripts = '/shared/home/snepomny/scripts_rb/'
hhfiles = '/shared/home/snepomny/hhmDomains/'
entfiles = '/shared/home/snepomny/domainData/'

load '/shared/home/snepomny/scripts_rb/astrals.rb'
#input is an .ent file with lines:
#ATOM      2  CA  ALA A   4      12.501  39.048  28.539  1.00 30.68           C 

runningResidue = 0
lastIndex = ''
residues = []
indices = []
File.new(ARGV[0]).each { |l|
        break if l.include? 'ENDMDL'
        next unless l.include? 'ATOM' or l.include? 'HETATM'
        if(lastIndex!=l[22..25]) #new index
                runningResidue+=1
                lastIndex=l[22..25]
                indices << l[22..25].to_i 
                residues << $astrals[l[17..19]]
        end
}

#read the residues from .hhm header
f = ARGV[0].split('/')[-1].split('.ent')[0]
start = false
hhRes = []
File.new("#{hhfiles}#{f}.hhm").each { |l| 
        break if start and l.include? '>' or l.include? '#'
        hhRes << l.chomp if start
        start = true if start==false and l.include?('>'+f)
}

#get masks from .hhm header:
start = false
hhMaskRes = []
File.new("#{hhfiles}#{f}.hhm").each { |l| 
        break if start and l.include? '>' 
        hhMaskRes << l.chomp if start
        start = true if start==false and l.include?('>ss_dssp')
}
hhMaskRes = hhMaskRes.join('').split('')
hhRes = hhRes.join('').split('')
exit unless hhMaskRes.size == hhRes.size

maskedRes = []
indicesF = []
count = 0
(0..hhRes.size).each {|i|
        unless hhMaskRes[i] == '-'
			maskedRes << hhRes[i].to_s 
			indicesF << indices[count]
			count+=1
	else
		indicesF << '-'
	end
}

exit unless residues.join('') == maskedRes.join('')
#puts residues.join('')
File.new(ARGV[0]+'.aln','w').puts(indicesF.join("\n")) 
