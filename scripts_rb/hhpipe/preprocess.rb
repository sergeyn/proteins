#!/usr/bin/env ruby

kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "globals"
require kernel_path + "naming"
require kernel_path + "pdb_get_xyz_fasta"
require kernel_path + "hhm_get_aln"

require "optparse"
require "ostruct"

# rubocop:disable Metrics/AbcSize

def do_pdb(filepath, dest_dir, id, chain_arr = [])
  xyzni, fasta = xyz_and_fasta(filepath, chain_arr)
  xyznipath = File.expand_path(id + Const.xyzsuffix, dest_dir)
  File.open(xyznipath, "w") do |out|
    out.puts xyzni.join("\n")
  end
  File.open(File.expand_path(id + Const.fastasuffix, dest_dir), "w") do |out|
    out.puts ">" + id # fasta convention
    out.puts fasta.join("")
  end
  xyznipath
end

def do_hhm(filepath, xyznipath, dest_dir, id, threshold)
  arr_hhm, arr_mask = seq_and_mask(filepath, id)
  xyz = read_xyzni xyznipath
  aligned = align_masked_hhm_to_pdb(xyz, arr_hhm, arr_mask, threshold)
  if aligned.empty?
    puts "Empty alignment for #{id}"
  else
    File.open(File.expand_path(id + Const.alnsuffix, dest_dir), "w") do |out|
      out.puts aligned.join("\n")
    end
  end
end

# rubocop:disable MethodLength
def arg_parse(args)
  options = OpenStruct.new
  options.pdb_file = nil
  options.hhm_file = nil
  options.guess_pdb = nil
  options.ecod = nil
  options.dest_dir = "./"
  options.replacement = nil
  options.threshold = 2

  opt_parser = OptionParser.new do |opts|
    opts.banner =
      ["Note, we are going to use the entity name from .hhm as the id.",
       "It needn't match the one in .pdb"].join
    opts.on(
      "-p", "--pdb_file PDB_FILE",
      "A PDB_FILE path. Can be gzipped or plain."
    ) do |v|
      options.pdb_file = v
    end
    opts.on(
      "-r", "--replace_pdb_ext EXT",
      "Replace current extension of file give in -p with EXT"
    ) do |v|
      options.replacement = v
    end
    opts.on(
      "-g", "--guess_pdb GUESS_PDB",
      "If set will try to guess the path of pdb under this dir"
    ) do |v|
      options.guess_pdb = v
    end
    opts.on(
      "-t", "--threshold THRESHOLD",
      "If we have 0 > misaligned > THRESHOLD <= 1.0, don't write .aln"
    ) do |v|
      options.threshold = v.to_f
    end
    opts.on(
      "--hhm_file HHM_FILE",
      "An HHM_FILE path."
    ) do |v|
      options.hhm_file = v
    end
    opts.on(
      "-d", "--dest_dir [DEST_DIR]",
      "A [DEST_DIR] dir to write the results to. Default: #{options.dest_dir}."
    ) do |v|
      options.dest_dir = v
    end
    opts.on(
      "--skip_chains_arr",
      "Replace the one returned by name discovery with empty."
    ) do |v|
      options.chains_arr = v
    end
    opts.on(
     "-x", "--xyzni [XYZNI]",
     "A [XYZNI] path to struct. data digest"
    ) do |v|
      options.xyzni = v
    end
    opts.on_tail("-h", "--help", "Show this message") do
      puts opts
      exit
    end
  end
  opt_parser.parse!(args)
  unless options.hhm_file
    puts "hhm_file is a mandatory argument!"
    puts opt_parser
    return nil
  end
  options
end

def main
  options = arg_parse ARGV
  # sanity checks
  return unless options
  unless File.exist? options.hhm_file
    puts "'#{options.hhm_file}' is wrong path for hhm file"
    return
  end

  id, chain_arr, pdbid = Naming.get_name_from_hhm_file options.hhm_file
  if id.empty?
    puts "Could not extract NAME from #{options.hhm_file}"
    return
  end
  chain_arr = [] if options.chains_arr
  pdb_file = options.replacement ? options.pdb_file.sub(options.replacement, "pdb") : options.pdb_file
  pdb_file = Naming.guess_pdb(pdbid, options.guess_pdb) unless pdb_file
  unless File.exist? pdb_file
    puts "'#{pdb_file}' is wrong path for pdb file"
    return
  end

  if options.xyzni
    xyznipath = options.xyzni
  else
    xyznipath = do_pdb pdb_file, options.dest_dir, id, chain_arr
  end
  do_hhm options.hhm_file, xyznipath, options.dest_dir, id, options.threshold
end

main if $PROGRAM_NAME == __FILE__
