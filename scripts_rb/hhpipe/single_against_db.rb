#!/usr/bin/env ruby

kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "globals"
require kernel_path + "naming"
require kernel_path + "utils"
require kernel_path + "target"
require kernel_path + "flat_helpers"
require kernel_path + "flatres_to_network"

require "optparse"
require "ostruct"
require "set"

def arg_parse(args)
  options = OpenStruct.new
  opts = nil
  opt_parser = OptionParser.new do |opt_scope|
    opts = opt_scope
    opts.banner = ""
    options.config = ""
    options.dbsdb = "db.yaml"
    options.eval = 0.01
    options.seqlen = 25

    opts.on("--logger LEVEL", "A LEVEL for the logger") do |v|
      options.level = v
    end
    opts.on("-t --target TARGET", "An id of TARGET in database") do |v|
      options.target = v
    end
    opts.on("-d", "--db DB", "The selected DB") do |v|
      options.db = v
    end
    opts.on("--alldbs ALLDBS", "Manifest of all dbs [db.yaml]") do |v|
      options.dbsdb = v
    end
    opts.on("-e --eval EVAL", Float, "An EVALUE threshold, take those that <=") do |v|
      options.eval = v
    end
    opts.on("-l --seqlen SEQLEN", Integer, "A SEQLEN threshold, take those that >=") do |v|
      options.seqlen = v
    end
    opts.on_tail("-h", "--help", "Show this message") do
      puts opts
      exit
    end
  end
  opt_parser.parse!(args)
  unless options.target && options.db
    STDERR.puts "Whoa, -t and -d are mandatory!"
    puts opts
    exit
  end
  options
end

# use dbid to fetch db config from db.yaml,
def load_database(fname_dbsdb, dbid)
  # returns a map from dbid to db config
  # # can't fail, but may return an empty hash
  dbs = HHpipe::Target.load_all_targets_and_map_by(fname_dbsdb, "id")
  db = dbs[dbid]
  db ? ExpOK.new(db) : ExpErr.new("Unknown dbid #{dbid}")
end

# returns the values of the given key for yaml_path file,
# or nil if the file is missing, or key is missing
def get_from_target_by_key(yaml_path, key)
  return ExpErr.new("Bad yaml path #{yaml_path}") unless File.exist? yaml_path

  HHpipe::Target.load_all_targets(yaml_path)[0][key]
end

def each_unique_id_from_flat(flat_file, target = nil, eval = 0.01, alilen = 25)
  ids = Set.new
  HHpipe::Flat.filtered_lines_generator(flat_file, true, eval, alilen) do |arr|
    [0, 1].each do |i|
      yield arr[i] if arr[i] != target && ids.add?(arr[i])
    end
  end
end

def add_to_seeds_if_found(db, id, seeds)
  # we need a target
  target = HHpipe::Target.load_target_for_id(id, [db["yamls_path"]])
  # with at least flat file, but better res
  return nil unless target # probably can protect against it on higher levels...

  if target["res"] || target["flat"]
    seeds[id] = target
    return true
  end
  false # caller may consider creating a batch
end

def each_edge_in_degree_one(db, id, eval = 0.01, alilen = 25)
  seeds = {}
  return unless add_to_seeds_if_found(db, id, seeds)

  flat_file = seeds[id]["res"] || seeds[id]["flat"]

  # populate seeds with union of left/right in target
  each_unique_id_from_flat(flat_file, id, eval, alilen) do |nid|
    add_to_seeds_if_found(db, nid, seeds)
  end

  seeds.each do |_nid, target|
    flat_file = target["res"] || target["flat"]
    HHpipe::Flat.filtered_lines_generator(flat_file, true, eval, alilen) do |arr|
      yield arr if seeds[arr[0]] && seeds[arr[1]]
    end
  end

end

def handle_target(db, ids, offset_index, eval = 0.01, alilen = 25)
  edge_array = []
  nodes = Set.new
  ids.each do |id|
    each_edge_in_degree_one(db, id, eval, alilen) do |arr|
      nodes.add(arr[0])
      nodes.add(arr[1])
      edge_array << HHpipe.flat_arr_to_edge_element(arr, offset_index)
    end
  end
  HHpipe.js_from_edges_and_nodes(edge_array, nodes, ids.to_s)
end

def main_impl(options)
  HHpipe.logger.level = Logger::DEBUG if options.level && options.level == "DEBUG"
  HHpipe.logger.debug "Logger set to debug"

  load_database(options["dbsdb"], options["db"])
    .then { |db| p handle_target(db, options["target"], options["eval"], options["seqlen"]) }
    .fail { |e| HHpipe.logger.error("single_vs_db") { e } }
end

def main
  main_impl arg_parse(ARGV)
end

main if $PROGRAM_NAME == __FILE__


