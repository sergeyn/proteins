# frozen_string_literal: true

kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "astrals"
require kernel_path + "utils"

# tmalign helpers
module TmAlign
  GAP = "-"

  # 17 Aligned length=  105, RMSD=   2.10, Seq_ID=n_identical/n_aligned= 0.276
  # ...
  # 22 ... denotes aligned residue pairs ...
  # 23 DIQLTQSPDSLA
  # 24 :: ::::
  # 25 -VQLVESG-GGL
  # return ^ [105, 2.10, 0.276, "DIQLTQSPDSLA", "-VQLVESG-GGL"]
  # handle is either open file or stringio
  def self._parse_tm_align(handle)
    arr = skip_io_until(handle, "Aligned length=").split(",")
    res = arr.map { |part| part.split("= ")[-1].to_f }
    skip_io_until(handle, "denotes aligned residue pairs")
    res << handle.gets.chomp # 23
    dots = handle.gets().chomp # 24
    sz = dots.each_char.count {|c| c != " " }
    res << handle.gets.chomp # 25
    puts "Mismatch #{path}: #{res[-1].size} =?= #{res[-2].size} && #{sz}=?=#{res[0]}" unless res[-1].size == res[-2].size && sz == res[0].to_i
    res
  end

  # read xyzni file and return it as an array of arrays
  def self._read_xyzni(fname)
    File.readlines(fname).each.map { |l| l.chomp.split(" ") }
  end

  def self._get_aligned_start(xyzni_arr, aligned)
    pattern = aligned.each_char.reject { |c| c == "-" }.join("") # remove gaps
    target = xyzni_arr.map { |xyzni| tla_to_one(xyzni[3]) }.join("")
    where = target.index(pattern) # easy case (no missing CAs and such)
    return where + 1 if where

    puts "Not an easy case, giving up!"
  end

  # similar to the method get_res_compressed_indices in flat_helpers
  def self.get_ranges(aligned_left, aligned_right, lstart = 0, rstart = 0)
    # 01 234   567 => 0-7
    # ab-cde---fgh
    # dddddddddddd
    # 01.345...901 => 0-1, 3-5, 9-11
    #
    #  01 234   567 => 0-7
    # -ab-cde---fgh
    # ddddddddddddd
    # .12.456...01 => 1-2, 4-6, 10-12
    raise "Aligned lengths mismatch" unless aligned_left.size == aligned_right.size

    li = lstart
    ri = rstart
    res_l = []
    res_r = []
    aligned_left.zip(aligned_right).each do |l, r|
      if l == GAP
        res_r << "#{rstart}-#{ri - 1}" unless ri - rstart <= 1
        rstart = ri + 1
      end
      if r == GAP
        res_l << "#{lstart}-#{li - 1}" unless li - lstart <= 1
        lstart = li + 1
      end
      li += 1 unless l == GAP
      ri += 1 unless r == GAP
    end
    res_r << "#{rstart}-#{ri - 1}" unless ri - rstart <= 1
    res_l << "#{lstart}-#{li - 1}" unless li - lstart <= 1
    [res_l.join(","), res_r.join(",")]
  end

  # given 3 file paths tmalign res and xyzni files for chain1 and chain2
  # figure out the metrics and aligned ranges
  # handle is either open tmaligh res file or stringio
  def self.tmalign_to_ranges(handle, xyzni1, xyzni2)
    data = _parse_tm_align(handle)
    aligned1 = data[-2]
    aligned2 = data[-1]
    xyzni1 = _read_xyzni(xyzni1)
    xyzni2 = _read_xyzni(xyzni2)

    data << ((100.0 * data[1]) / data[0]).round(2) # calc SAS
    start1 = _get_aligned_start(xyzni1, aligned1)
    start2 = _get_aligned_start(xyzni2, aligned2)
    data.concat(get_ranges(aligned1.split(""), aligned2.split(""), start1, start2))
  end
end

# p TmAlign.tmalign_to_ranges(*ARGV)
#335-342,358-358,363-366,368-374,376-383,387-400,406-412,426-426,429-429,431-436,509-513,515-515,524-525
#335-342,        363-366,368-374,376-383,387-400,406-412,                431-436,509-513,        524-525
#3-15,22-26,31-45,47-78
#3-15,22-26,31-45,47-78


#    56789012q
#TN--LCPFGEVFNATRFASVYAWNRKRISNCVADYSV------LYNSA----SFSTFKCYGVSPTKLNDLC-FTNVYADSFVIRGDEVRQIAPGQTGKIADYNYKLPDDFTGCVIAWNSNNLDSKVGGNYNYLYRLFRKSNLKPFERDISTEIYQAGSTPCNGVEGFNCYFPLQSYGFQPTNGVGYQPYRVVVLSFELLHAPATVCGP---
#--FHLTTRNGEP---------------H----MIVS-RQEKGKSLLFKTEDGVN-MCTLMAMD---LGELCEDTITYKCPL-----LRQNEPE-------------D--I-DCWCNS------------------------------------------------------------------------TSTWV-T--------YG--TCT
#....xxxxxxxx...............x....xxxx.......xxxxx....xx.xxxxxxxx...xxxxx.xxxxxxxxx.....xxxxxxx.............x..x.xxxxxx........................................................................xxxxx.x........xx.....
#    34567890              11   12345      223456   312 34567890  412345 78901...