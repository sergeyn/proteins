kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "globals"
require kernel_path + "utils"
require kernel_path + "fileopen"

# Part of hhpipe
module HHpipe
  # 0   1   2      3      4     5   6    7      8       9     10       11   12
  # p0,p1,evalue,length,ident,simlr,gps,left_q,right_q,left_s,right_s,seq_q,seq_s
  module Flat
    F_LEFT = 0
    F_RIGHT = 1
    F_EVAL = 2
    F_LEN = 3
    F_IDENT = 4
    F_SIMILAR = 5
    F_GAPS = 6
    F_START_LEFT = 7 # left_q
    F_END_LEFT = 8 # right_q
    F_START_RIGHT = 9 # left_s
    F_END_RIGHT = 10 # right_s
    F_SEQ_LEFT = 11 # seq_q
    F_SEQ_RIGHT = 12 # seq_s
    F_PROBAB = 13
    F_SCORE = 14
    F_RMSD = 21

    def self.passing_line?(arr, threshold_eval, threshold_seq_len)
      return false if threshold_eval && arr[F_EVAL].to_f > threshold_eval

      return false if threshold_seq_len && arr[F_LEN].to_i < threshold_seq_len

      true
    end

    def self.each_flat_line_arr(fname)
      any_open_b(fname) do |hndl|
        hndl.each { |line| yield line.split(Const.flatdelim) }
      end
    end

    def self.filtered_lines_generator(fname, should_dedup, eval_thr, len_thr)
      hsh = {}
      each_flat_line_arr(fname) do |arr|
        names = [arr[0], arr[1]].sort.join
        next if should_dedup && hsh[names] # also remove duplicates

        hsh[names] = true
        yield arr if passing_line?(arr, eval_thr, len_thr)
      end
    end

    # extract alignment residues from flat arr and convert to compressed indices representation
    def self.get_res_compressed_indices(flat_arr, offset_hash = {})
      # 01 234   567 => 0-7
      # ab-cde---fgh
      # dddddddddddd
      # 01.345...901 => 0-1, 3-5, 9-11
      #
      #  01 234   567 => 0-7
      # -ab-cde---fgh
      # ddddddddddddd
      # .12.456...01 => 1-2, 4-6, 10-12

      # use offset information to correct for ATOM residues
      loffset = offset_hash[flat_arr[Flat::F_LEFT]] || 1
      roffset = offset_hash[flat_arr[Flat::F_RIGHT]] || 1

      lstart = flat_arr[Flat::F_START_LEFT].to_i + loffset - 1
      li = lstart
      rstart = flat_arr[Flat::F_START_RIGHT].to_i + roffset - 1
      ri = rstart
      res_l = []
      res_r = []
      flat_arr[Flat::F_SEQ_LEFT].split("").zip(flat_arr[Flat::F_SEQ_RIGHT].split("")).each do |l, r|
        if l == GAP
          res_r << "#{rstart}-#{ri - 1}" unless ri - rstart <= 1
          rstart = ri + 1
        end
        if r == GAP
          res_l << "#{lstart}-#{li - 1}" unless li - lstart <= 1
          lstart = li + 1
        end
        li += 1 unless l == GAP
        ri += 1 unless r == GAP
      end
      res_r << "#{rstart}-#{ri - 1}" unless ri - rstart <= 1
      res_l << "#{lstart}-#{li - 1}" unless li - lstart <= 1
      [res_l.join(","), res_r.join(",")]
    end
  end
end
