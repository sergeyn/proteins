#!/usr/bin/env ruby
kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "single_against_db"
require kernel_path + "flatres_to_network"

require "set"
require "webrick"

# returns a map from dbid to db config
def load_databases(fname)
  HHpipe::Target.load_all_targets_and_map_by(fname, "id")
end

def load_offsets(fname)
  offset_index = {}
  begin
    File.new(fname).each do |l|
      arr = l.split
      offset_index[arr[0]] = arr[1].to_i
    end
  rescue StandardError
    puts "Error parsing #{fname}"
  end
  offset_index
end

# We wish to distinguish between the following cases:
# 1) no such target in db -- error 501
# 2) target has no flat/res file -- error 502, suggest running
# 3) there is a flat, and we run it -- OK, 200, show the returned JSON

# @return the json for the given target in given database
# it is obtained by running flatres_to_network methods on res or flat file
def fetch(req_hash)
  target = req_hash["target"]
  config = { should_dedup: true, evalue: req_hash["eval"], len_thr: req_hash["len"] }
  db_config = HServer.dbases[req_hash["db"]] || {}
  path = File.expand_path(target + ".yaml", db_config["yamls_path"])
  return {} unless File.exist? path

  offset_index = load_offsets(db_config["offset_index"])

  res_path = get_from_target_by_key(path, "res")
  return HHpipe.js_from_flat(res_path, config, [target].to_set, offset_index) if res_path

  flat_path = get_from_target_by_key(path, "flat")
  return HHpipe.js_from_flat(flat_path, config, [target].to_set, offset_index) if flat_path

  HHpipe.logger.info("#{__FILE__}:#{__LINE__}") { "Can't fetch #{target} from #{path}" }
  nil
end

def ecod(req_hash, offsets_cache)
  ids = req_hash["target"].split(",")
  dbname = req_hash["db"]
  db = HServer.dbases[dbname]
  offsets_cache[dbname] = load_offsets(db["offset_index"]) unless offsets_cache[dbname]
  handle_target(db, ids, offsets_cache[dbname], req_hash["eval"].to_f, req_hash["len"].to_i)
end

def handle_post(req_hash, offsets_cache)
  HHpipe.logger.debug("#{__FILE__}:#{__LINE__}") { "do_POST #{req_hash}" }
  fetch(req_hash) if req_hash["action"] == "fetch"
  ecod(req_hash, offsets_cache) if req_hash["action"] == "ecod"
end

# rest server helpers
module HServer
  include WEBrick

  @@databases = {}
  def self.dbases
    @@databases
  end

  # Rest servlet to handle requests from web server on cluster (could be same machine)
  class RestServlet < HTTPServlet::AbstractServlet
    @@offsets_cache

    def do_POST(req, resp)
      resp.body = handle_post(req.query, @@offsets_cache)
      raise WEBrick::HTTPStatus::NotImplemented if resp.body == {}
      raise WEBrick::HTTPStatus::OK if resp.body
      raise WEBrick::HTTPStatus::BadGateway
    end
  end

  def self.start_webrick(config = { Port: 9955 })
    server = HTTPServer.new(config)
    HHpipe.logger.debug("#{__FILE__}:#{__LINE__}") { "Server is up with #{config}" }
    yield server if block_given?
    %w[INT TERM].each do |signal|
      trap(signal) { server.shutdown }
    end
    server.start
  end

  def self.run(_config = {})
    dbase_path = ARGV[0] || "db.yaml"
    @@databases = load_databases(dbase_path).freeze
    start_webrick do |server|
      server.mount("/", RestServlet)
    end
  end
end

HServer.run if $PROGRAM_NAME == __FILE__
