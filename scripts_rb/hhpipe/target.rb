kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "globals"
require kernel_path + "naming"

require "yaml/store"

module HHpipe
  # db-like helpers
  module YaDb
    def self.write(fname, key, value)
      store = YAML::Store.new(fname)
      store.transaction do
        store[key] = value
      end
    end

    def self.read(fname, key)
      store = YAML::Store.new(fname)
      store.transaction { store[key] }
    end
  end

  # expose target related helpers and constants
  # schema for the config:
  # root element is :targets -- array of targets
  # target should/shall contain
  # 1. strings: id, hhm, hhr, pdb, aln, xyz, flat, res
  # 2. array: data_dirs
  module Target
    KEYS = { id: ["", "normally, what is given in NAME of .hhm"],
             hhm: [".hhm", "path to hhm file"],
             hhr: [".hhr", "path to hhr file"],
             pdb: [".pdb", "path to pdb file"],
             aln: [Const.alnsuffix, "path to aln file"],
             xyz: [Const.xyzsuffix, "path to xyzni file"],
             flat: [Const.flatsuffix, "path to flat hhr file"],
             res: [Const.ressuffix, "path to result of tmscore"],
             data_dirs: ["", "array of directories for file lookup"] }.freeze

    def self.name_for_id(id)
      id + Const.dbsuffix
    end

    def self.load_all_targets(fname, key = :targets)
      YaDb.read(fname, key)
    end

    def self.load_all_targets_and_map_by(fname, id)
      target_hash = {}
      all_targets = load_all_targets(fname)
      return target_hash unless all_targets

      all_targets.each do |target|
        target_hash[target[id]] = target
      end
      target_hash
    end

    # will try to fetch a yaml for id by looking for candidates in data_dirs array members
    def self.load_target_for_id(id, data_dirs)
      HHpipe.logger.debug "load_target_for_id: #{id} in #{data_dirs.join ';'}"
      HHpipe.logger.debug { Naming.file_in_dirs(name_for_id(id), data_dirs) }
      load_all_targets(Naming.file_in_dirs(name_for_id(id), data_dirs)).to_a[0]
    end

    def self.path_from_target(target, key)
      raise "Invalid key #{key} for target" unless KEYS.include? key

      HHpipe.logger.debug { "#{key} in #{target}" }
      unless target[key]
        return Naming.file_in_dirs(target["id"] + KEYS[key][0],
                                   target["data_dirs"])
      end
      target[key]
    end

    def self.target_for_hhm(hhm_path, yaml_path)
      id = Naming.get_name_from_hhm_file(hhm_path)[0]
      target = { id: id, hhm: hhm_path,
                 hhr: hhm_path.sub(".hhm", ".hhr"),
                 data_dirs: [File.dirname(hhm_path)] }
      YaDb.write(File.join(yaml_path, id + Const.dbsuffix),
                 :targets, [target])
    end
  end
end
