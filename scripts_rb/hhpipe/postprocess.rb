#!/usr/bin/env ruby

kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "globals"
require kernel_path + "naming"
require kernel_path + "utils"
require kernel_path + "target"
require kernel_path + "hhr_flatten"
require kernel_path + "flat_hhm_tmscore"

require "optparse"
require "ostruct"

# opens HHpipe namespace
module HHpipe
  # postproc helpers
  module Postprocess
    def self.flatten_hhr(target)
      # TODO: hack
      target["hhr"] = target["hhm"] + ".hhr" unless target["hhr"]
      target["flat"] = File.expand_path(target["id"] + Const.flatsuffix,
                                        target["data_dirs"][-1])
      HHpipe.logger.debug "flatten_hhr: #{target['hhr']}"
      HHrFlat.parse_hhr_to_flat(target["hhr"], target["flat"])
      target
    end

    # convention, if more than one data_dir, writes output to the last one!
    def self.tmscore_file(target, tmscore)
      HHpipe.logger.debug "tmscore_file: #{target['flat']}"
      xyz_query, aln_query = get_xyz_aln_for_target(target)
      return target if aln_query.empty?

      target["res"] = File.expand_path(target["id"] + Const.ressuffix,
                                       target["data_dirs"][-1])
      File.open(target["res"], "w") do |outf|
        IO.readlines(target["flat"]).each do |line|
          begin
            arr = line.split(Const.flatdelim)
            outf.puts handle_single_flat_line(arr,
                                              target["data_dirs"],
                                              target["id"],
                                              [xyz_query, aln_query],
                                              tmscore)
          rescue StandardError => e
            outf.puts line.chomp + " ERR " + e.message
            HHpipe.logger.error e.message
            HHpipe.logger.error e.backtrace
          end
        end
      end
      target
    end
  end
end

# returns a modified (maybe) target object + obvious side-effects
def run_target(target, tmscore)
  target = target["flat"] ? target : HHpipe::Postprocess.flatten_hhr(target)
  !tmscore.empty? ? HHpipe::Postprocess.tmscore_file(target, tmscore) : target
end

def arg_parse(args)
  options = OpenStruct.new
  opt_parser = OptionParser.new do |opts|
    opts.banner = ""
    options.config = ""

    opts.on("--logger LEVEL", "A LEVEL for the logger") do |v|
      options.level = v
    end
    opts.on("-y --yaml CONFIG", "A CONFIG for the target") do |v|
      options.config = v
    end
    opts.on(
      "-t", "--tmscore [TMSCORE]",
      "A path to TMscore binary. Defaults to #{Const.tmscore} and assumes it is in $PATH"
    ) do |v|
      options.tmscore = v
    end
    opts.on_tail("-h", "--help", "Show this message") do
      puts opts
      exit
    end
  end
  opt_parser.parse!(args)
  options
end

def main
  options = arg_parse ARGV
  return unless options && options.config

  HHpipe.logger.level = Logger::DEBUG if options.level && options.level == "DEBUG"
  HHpipe.logger.debug "Logger set to debug"
  targets = HHpipe::Target.load_all_targets(options.config).each do |target|
    run_target(target, options.tmscore)
  end
  HHpipe::YaDb.write(options.config, :targets, targets)
end

main
