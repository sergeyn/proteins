kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "astrals"
require kernel_path + "naming"
require kernel_path + "globals"
require kernel_path + "utils"

# rubocop:disable Metrics/AbcSize

ATOM = "ATOM".freeze

def atoms_of_chains(fname)
  # return "chain" -> [ca atom lines] hash

  # pdb ranges
  range_ca = 13..14
  index_chain = 21

  hsh = {}
  fopen_gz(fname).each do |line|
    next unless line[range_ca] == "CA" && line.start_with?(ATOM)
    chain = line[index_chain]
    hsh[chain] = [] unless hsh[chain]
    hsh[chain] << line.chomp
  end
  hsh
end

# good test: 4rjw_A.pdb
# 012345678901234567890123456789012345678901234567890123456789012345678901234567
# ATOM      1  N   ALA A   4      11.751  37.846  29.016  1.00 44.65           N
# ATOM      2  CA  ALA A   4      12.501  39.048  28.539  1.00 30.68           C
# return an array of xyz_n_i strings and seqres array
def xyz_and_fasta(fname, chain_arr = [])
  # pdb ranges
  range_ca = 13..14
  range_tla_residue = 17..19
  range_index = 22..25
  range_x = 30..37
  range_y = 38..45
  range_z = 46..53

  lines = any_readlines fname
  puts "No lines in #{fname}" if lines.empty?
  return [[], []] if lines.empty?

  unless chain_arr.empty?
    chain_hash = atoms_of_chains(fname)
    lines_ = []
    chain_arr.each { |chain| lines_.concat(chain_hash[chain] || []) }
    lines = lines_ unless lines_.empty?
    puts "No matching chains #{chain_arr.join} in #{fname}" if lines_.empty?
  end

  atoms = []
  seqs_from_atoms = []
  i = lines.find_index { |l| l.include? ATOM }
  puts "no atoms in #{fname}" unless i
  return [[], []] unless i

  prev = ""
  lines[i..-1].each do |line|
    next unless line[range_ca] == "CA" && line.start_with?(ATOM)
    next if line[range_index] == prev
    prev = line[range_index]
    atoms << [line[range_x], line[range_y], line[range_z],
              line[range_tla_residue],
              line[range_index].to_i].join(Const.xyzrdelim)
    seqs_from_atoms << tla_to_one(line[range_tla_residue])
  end
  [atoms, seqs_from_atoms]
end

# read xyzni file and return it as an array of arrays
def read_xyzni(fname, delim = Const.xyzrdelim)
  File.readlines(fname).each.map { |l| l.chomp.split(delim) }
end

if $PROGRAM_NAME == __FILE__
  if ARGV.size < 2
    puts "Usage: [file name or a blob] [suffix]"
  else
    Dir[ARGV[0]].each do |f|
      # next if File.exist?(f + Const.xyzsuffix)
      pair = xyz_and_fasta(f)
      File.open(f + Const.xyzsuffix, "w") do |out|
        out.puts pair[0].join("\n")
      end
      File.open(f + Const.fastasuffix, "w") do |out|
        # fasta name
        out.puts ">" + Naming.get_name_from_file_dot_suffix(f, ARGV[1])
        out.puts pair[1].join("")
      end
    end
  end
end
