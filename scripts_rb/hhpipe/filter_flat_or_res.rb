# Filter flat/res file
kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "flat_helpers"

require "optparse"
require "ostruct"

def arg_parse(args)
  options = OpenStruct.new
  opt_parser = OptionParser.new do |opts|
    opts.banner = "Usage:"
    opts.on("--logger LEVEL", "A LEVEL for the logger") do |v|
      options.level = v
    end
    opts.on("-d --dedup", "Whether should deduplicate (def: false)") do |v|
      options.dedup = v
    end
    opts.on("-f --flat FLAT", "A path to the FLAT file") do |v|
      options.flat = v
    end
    opts.on("-e --eval EVAL", Float, "An EVALUE threshold, take those that <=") do |v|
      options.eval = v
    end
    opts.on("-l --seqlen SEQLEN", Integer, "A SEQLEN threshold, take those that >=") do |v|
      options.seqlen = v
    end
    opts.on_tail("-h", "--help", "Show this message") do
      puts opts
      exit
    end
  end
  opt_parser.parse!(args)
  unless options.flat
    puts opt_parser
    exit
  end
  options
end

def main
  options = arg_parse(ARGV)
  HHpipe::Flat.filtered_lines_generator(options.flat, options.dedup, options.eval, options.seqlen, &method(:puts))
end[]

main if $PROGRAM_NAME == __FILE__
