kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "globals"
require kernel_path + "fileopen"
require kernel_path + "flat_helpers"

require "set"
require "json"

# part of hhpipe
module HHpipe

  def self.js_from_edges_and_nodes(edge_arr, nodes_set, highlighted_nodes = Set.new)
    format(JS_TEMPLATE_SMALL,
           elements: HHpipe.elements_from(nodes_set, edge_arr, highlighted_nodes), style: CSS)
  end

  # @return require-loadable js string with elements and style
  def self.js_from_flat(flat_path, config, highlighted_nodes = Set.new, offset_hash = {})
    edge_array = []
    nodes = Set.new
    Flat.filtered_lines_generator(flat_path, config[:should_dedup], config[:evalue].to_f, config[:len_thr].to_i) do |l|
      arr = l.split(Const.flatdelim)
      edge_array << HHpipe.flat_arr_to_edge_element(arr, offset_hash)
      nodes.add(arr[0])
      nodes.add(arr[1])
    end
    js_from_edges_and_nodes(edge_array, nodes, highlighted_nodes)
  end

  # create .sif and .edges files for the given flat_path
  def self.sif_from_flat(flat_path, config = { should_dedup: true, evalue: 0.1, len_thr: 20 })
    sif = File.new(flat_path + ".sif", "w")
    edges = File.new(flat_path + ".edges", "w")
    edges.puts("edge\tevalue\tseqlen\trmsd\tRes1\tRes2\tAli1\tAli2")
    Flat.filtered_lines_generator(flat_path, config[:should_dedup], config[:evalue], config[:len_thr]) do |line|
      arr = line.split
      sif.puts [arr[Flat::F_LEFT], "x", arr[Flat::F_RIGHT]].join(" ")
      res1, res2 = Flat.get_res_compressed_indices(arr)
      edges.puts sif_printable_line(arr, res1, res2)
    end
    sif.close
    edges.close
  end

  include Flat

  JS_TEMPLATE_REQUIRE = "define(function(require) { return { \"elements\" : %<elements>s, %<style>s };});".freeze
  JS_TEMPLATE_SMALL = "{ \"elements\" : %<elements>s, %<style>s }".freeze

  CSS = <<-CSS_D
  "style" :
  [
    {
      "selector" : "node",
      "css" : {
        "content": "data(name)",
        "background-color": "mapData(hl, 0, 1, silver, orange)",
        "text-valign": "center",
        "width" : 55,
        "height" : 50,
        "font-size": "8px"
      }
    },
    {"selector" : "edge",
     "css" : {
        "width": "mapData(evalue, 0, 500, 8, 1)",
        "line-color": "mapData(evalue, 0, 500, rgb(20,20,20), rgb(200,200,200))",
        "curve-style": "haystack"
      }
    },
    {
     "selector": "node:selected",
     "style": {
        "border-width": "6px",
        "border-color": "#AAD8FF",
    	"border-opacity": "0.5",
        "background-color": "#77828C",
        "text-outline-color": "#77828C"
     }
    }
  ]
  CSS_D
        .freeze

  def self.sif_printable_line(arr, res1, res2)
    [[arr[F_LEFT], "(x)", arr[F_RIGHT]].join(" "), arr[F_EVAL], arr[F_LEN], arr[F_RMSD], res1, res2, arr[F_SEQ_LEFT], arr[F_SEQ_RIGHT]].join("\t")
  end

  def self.node_element(name, highlighted)
    # { data: { id: 'j', name: 'Jerry' } }
    return { data: { id: name, name: name, hl: 1 } } if highlighted

    { data: { id: name, name: name } }
  end

  def self.flat_arr_to_edge_element(arr, offset_hash = {})
    # { data: { source: 'j', target: 'e' } }
    res1, res2 = Flat.get_res_compressed_indices(arr, offset_hash)
    { data: {
      source: arr[F_LEFT],
      target: arr[F_RIGHT],
      evalue: arr[F_EVAL].to_f,
      Ali1: arr[F_SEQ_LEFT],
      Ali2: arr[F_SEQ_RIGHT],
      Res1: res1,
      Res2: res2
    } }
  end

  def self.elements_from(node_set, edges_arr, highlighted_nodes)
    nodes = node_set.map { |v| node_element(v, highlighted_nodes.include?(v)) }
    JSON.pretty_generate(nodes: nodes, edges: edges_arr)
  end
end

if $PROGRAM_NAME == __FILE__
  flat_file = ARGV[0]
  config = { should_dedup: true, evalue: 0.1, len_thr: 20 }
  puts HHpipe.js_from_flat(flat_file, config, ["3_1754"].to_set)
  # HHpipe.sif_from_flat(flat_file, config)
end
