kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "utils"
require kernel_path + "globals"
require kernel_path + "flat_helpers"

require "stringio"

module HHpipe
  # used to process hhr to flat files
  module HHrFlat
    EVAL_THRSH = 500.01
    LENGTH_THRSH = 20

    include Flat

    def self.iterate_hits_of_single_hhp_query(lines, &_block)
      counter = 0
      hit = []
      h_name = "nil"
      lines.each do |line|
        if line.start_with? ">" # starting new hit
          yield hit, h_name unless counter < 1
          h_name = line.split[0].split(">")[-1] # <-- not in domains! .gsub('_','')
          counter += 1
          hit = []
        end
        hit << line
      end
      yield hit, h_name
    end

    # Hit may contain several parts. Every part starts with
    #   0     1      2     3    4     5        6          7       8        9     10        11     12        13
    # Probab=1.00  E-value=1  Score=311.74  Aligned_cols=129  Identities=100%  Similarity=1.392  Sum_probs=123.3
    # and has an alignment block (see parser for alignment blocks)
    def self.parse_hhp_hit(r_name, hit_arr)
      file = StringIO.new(hit_arr.join("\n"))
      array_of_hits = []
      loop do
        line = skip_io_until(file, "Identities")
        return array_of_hits if line.nil?

        arr = line.split(/=| /).reject(&:empty?)
        align_data = parse_hhp_alignment_block(file)
        slen = length_of_alignment(align_data[4].split(""), align_data[5].split(""))
        array_of_hits << [nil, r_name, arr[3], slen, arr[9].sub("%", ""), arr[11], 0,
                          align_data[0], align_data[1], align_data[2], align_data[3],
                          align_data[4], align_data[5], arr[1], arr[5]]
      end
    end

    # Q 155c_A            2 NEGDAAKGEKEFNKCKACHMIQAPD ... ANLIEYV   80 (135)
    #                      +.||+++|+++|++|++||+|+.+++.. ..++|++||
    # T 1c2n_A           20 FAGDAAKGEKEFNKCKTCHSIIAPD ... EEDIATV   97 (137)
    #
    #
    # Q 155c_A           81 TDPKPLVKKMTDDKGAKTKMTFKMGKNQADVVAFLAQDD  119 (135)
    #                      +||++||+...++..|||+|+|++++||+||||||+|.+
    # T 1c2n_A           98 KDPGAFLKEKLDDKKAKTGMAFKLAKGGEDVAAYLASVV  136 (137)
    def self.parse_hhp_alignment_block(file)
      seq_q = []
      seq_s = []
      line = skip_io_until(file, "Q ")
      while line
        parts = line.split
        left_q ||= parts[2].to_i
        right_q = parts[4].to_i
        seq_q << parts[3]

        line = skip_io_until(file, "T ")
        parts = line.split
        left_s ||= parts[2].to_i
        right_s = parts[4].to_i
        seq_s << parts[3]

        line = skip_io_until(file, "Q ", "Score")
        line = nil if !line || line.include?("Score")
      end
      [left_q.to_s, right_q.to_s, left_s.to_s, right_s.to_s, seq_q.join(""), seq_s.join("")]
    end

    def self.swap_left_right!(hit)
      hit[F_LEFT], hit[F_RIGHT] = hit[F_RIGHT], hit[F_LEFT]
      hit[F_START_LEFT], hit[F_START_RIGHT] = hit[F_START_RIGHT], hit[F_START_LEFT]
      hit[F_END_LEFT], hit[F_END_RIGHT] = hit[F_END_RIGHT], hit[F_END_LEFT]
      hit[F_SEQ_LEFT], hit[F_SEQ_RIGHT] = hit[F_SEQ_RIGHT], hit[F_SEQ_LEFT]
    end

    def self.print_pair(query_name, hit_arr, where_to)
      pair = [query_name, hit_arr[F_RIGHT]].sort # sort lex. the names of chains
      return false if pair[0] == pair[1] || # no self similarity
                      hit_arr[F_EVAL].to_f > EVAL_THRSH || # no bad evalues
                      hit_arr[F_SEQ_LEFT].size < LENGTH_THRSH # no shorties

      hit_arr[F_LEFT] = query_name
      swap_left_right!(hit_arr) if query_name == pair[1]
      where_to.puts(hit_arr.join(Const.flatdelim))
      true
    end

    # Note: will read fname to memory!
    def self.parse_hhr_to_flat(fname, flat_file = nil)
      qlines = IO.readlines(fname)
      q = qlines.join("")
      return unless q.index("Query").zero?

      q_name = q[0..256].split[1]
      skipped = 0
      taken = 0
      flat_file ||= fname + Const.flatsuffix
      File.open(flat_file, "w") do |f|
        iterate_hits_of_single_hhp_query(qlines) do |h, p_name|
          hit_arr = parse_hhp_hit(p_name, h)[0] # NB: only using the first
          if print_pair(q_name, hit_arr, f)
            taken += 1
          else
            skipped += 1
          end
        end
      end
      puts "#{fname}: #{taken}/#{skipped + taken}"
    end
  end
end

#-------------------------------------------------------------------
if $PROGRAM_NAME == __FILE__
  if ARGV.empty?
    STDERR.puts "Usage: [hhr file name or a blob]"
  else
    Dir[ARGV[0]].each { |f| HHpipe::HHrFlat.parse_hhr_to_flat f }
  end
end
