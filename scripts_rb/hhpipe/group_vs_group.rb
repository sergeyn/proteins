# frozen_string_literal: true

require "optparse"
require "stringio"

kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "mmcif"
require kernel_path + "tmalign_parse"

SAS = 19.0

params = {}
OptionParser.new do |opts|
  opts.banner = "Usage: group_vs_group.rb [options]"

  opts.on("-sSOURCE", "--source=SOURCE",
          "Path to file with protein_chains for 'source' group") do |s|
    params[:source] = s
  end
  opts.on("-tTARGET", "--target=TARGET",
          "Path to file with protein_chains for 'target' group") do |t|
    params[:target] = t
  end
  opts.on("-f", "--forceoverwrite",
          "Repeat actions if results exists (including fetch)") do
    params[:forceoverwrite] = true
  end
  opts.on("-dDEST", "--destination=DEST",
          "Where to put the results in (default: cwd)") do |d|
    params[:destination] = d
  end
  opts.on("-aALIGN", "--tmalign=ALIGN", "Path to TM-Align") do |a|
    params[:tmalign] = a
  end
  opts.on("-fFILTER", "--filter=FILTER", "Secondary filter for SAS, useful for reducing conservative results") do |a|
    params[:filter] = a.to_f
  end
end.parse(ARGV)

params[:destination] = "." unless params[:destination]
params[:filter] = SAS unless params[:filter]
def _fetch_names_from_list(path)
  names = []
  File.open(path).each do |line|
    ar = line.chomp.split(" ").reject(&:empty?)
    names.concat(ar)
  end
  names
end
ignore_if_exist = !params[:forceoverwrite]

# read names from params[:source] and params[:target] and fetch the mmcif
# to destination, overriding according to params[:forceoverwrite]
# will also produce xyzni for the mmcif
src_names = _fetch_names_from_list(params[:source])
mmcif_get_all(src_names, params[:destination], ignore_if_exist) { |name, dest| download_mmcif(name, dest) }
tgt_names = _fetch_names_from_list(params[:target])
mmcif_get_all(tgt_names, params[:destination], ignore_if_exist) { |name, dest| download_mmcif(name, dest) }

# multiply the two groups
src_names.each do |s|
  align_res = "#{params[:destination]}/#{s}.tma"
  next if File.exist?(align_res) && ignore_if_exist

  File.open(align_res, "w") do |out|
    tgt_names.each do |t|
      next if s == t

      puts "Processing pair #{s} -- #{t}"
      res = `#{params[:tmalign]} #{params[:destination]}/#{s}.mmcif #{params[:destination]}/#{t}.mmcif`
      metrics = TmAlign.tmalign_to_ranges(
        StringIO.new(res),
        "#{params[:destination]}/#{s}.xyzni",
        "#{params[:destination]}/#{t}.xyzni"
      )
      if metrics[5] >= SAS
        puts "skipping #{s}--#{t} with #{metrics[5]}"
      else
        out.puts "#{s} #{t} " + metrics.join(" ")
      end
    end
  end
end

def _load_tma_result(path, filter=SAS)
  pairs = []
  File.new(path).each do |line|
    ar = line.chomp.split
    pairs << ar if ar[7].to_f < filter
  end
  pairs = nil if pairs.empty?
  pairs
end

# given result files for all source names,
# figure out the order of nodes for both source and target (greatest degree)
# and return list of edges + src nodes ordered + tgt nodes ordered
# the first two tokens in result line are always src and target
def _build_node_maps(src_names, result_files, filter=SAS)
  src_results = src_names
                  .zip(result_files)
                  .map { |src, fpath| _load_tma_result(fpath, filter)}
                  .compact

  tgt_hsh = {}
  src_results.sort_by! { |r| 0 - r.size }
  src_results.each do |res_lst|
    res_lst.each do |res| 
	tgt_hsh[res[1]] = tgt_hsh.fetch(res[1], 0) + 1
    end
  end
  tgt_results = tgt_hsh.sort_by {|k, v| 0 - v}
  tgt_nodes = tgt_results.map { |r| r[0] }
  src_nodes = src_results.map { |r| r[0][0] }

  [src_nodes, tgt_nodes, src_results]
end


# elements: {
#   nodes: [
#     { data: { id: "1trk" }, position: { x: -3180.1, y: -3790.2 } },
#     { data: { id: "1msn" } }
#   ],
#   edges: [
#     { data: { id: "ab", source: "1trk", target: "1msn" } }
#   ]
# }

result_files = src_names.map { |s| "#{params[:destination]}/#{s}.tma" }
src_nodes, tgt_nodes, src_results = _build_node_maps(src_names, result_files, params[:filter])

def _position_nodes(nodes)
  nodes.each_with_index.map do |node, i|
    "{ \"data\": { \"id\": \"#{node}\" }, \"position\": { \"x\": -1000, \"y\": #{-100.0 * i} } }"
  end 
end

p _position_nodes(tgt_nodes)
#[src_nodes.size, tgt_nodes.size, src_results]
# export tmalign=/home/rachel/nCOV19/TM_align/TMalign
# export grp1=/home/rachel/nCOV19/nCOV19_pdbs/new_chains/nCOV19_list_relevant
# export grp2=/home/rachel/nCOV19/Dengue_pdbs/chains/Dengue_list_relevant
# ruby ~/proteins/scripts_rb/hhpipe/group_vs_group.rb -s $grp1 -t $grp2 -a $tmalign -d ~/dest

