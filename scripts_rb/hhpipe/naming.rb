kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "fileopen"

# Helpers for getting names from file and filenames
module Naming
  PATTERN = "pattern".freeze

  def extract_pattern(pattern, str, ptS = PATTERN)
    s = str.clone
    pattern.split(ptS).each do |part|
      next if part.empty?
      s.gsub!(part, "")
    end
    s
  end

  def put_basename_in_pattern(basename, str, repP = PATTERN)
    str.gsub(repP, basename)
  end

  def get_name_from_file_dot_suffix(filename, suffix)
    extract_pattern(PATTERN + suffix, File.basename(filename))
  end

  def get_name_from_pdb(filename)
    get_name_from_file_dot_suffix(filename, ".pdb")
  end

  def get_name_from_ent(filename)
    get_name_from_file_dot_suffix(filename, ".ent")
  end

  def get_name_from_hhm(filename)
    get_name_from_file_dot_suffix(filename, ".hhm")
  end

  def guess_pdb(pdbid, pdb_dir)
    # 001297507 -> dir/12975/001297507.pdbnum.pdb.*
    return "" if pdbid.size != 9
    path = pdb_dir + "/" + pdbid[2...-2] + "/" + pdbid + "/" + pdbid + ".pdbnum.pdb*"
    files = Dir[path]
    files.empty? ? "" : files[0]
  end

  # TODO: we need a better way of extracting this for non ECOD
  def get_name_from_hhm_nameline(line)
    # NAME  e1htr.1 P:1-43,B:1-329 000000011
    ar = line.chomp.split
    return ar[1], [], "" if ar.size == 2
    chains = ar.size == 3 ? [] : ar[2].split(",").map { |c| c[0] }
    [ar[1], chains, ar[-1]]
  end

  def get_name_from_hhm_file(fname)
    # Use NAME - on second line, ar[1] is always name, but pdbid&chains ecod-only
    any_open_b(fname) do |f|
      f.each do |l|
        return get_name_from_hhm_nameline(l) if l.start_with? "NAME "
      end
    end
    ["", [], ""] # if got here: broken file
  end

  def get_name_from_hhr_file(fname)
    # Get the name from first line of hhr file
    # Query         e13pkB1 B:2-415 000109271
    any_open_b(fname) do |f|
      f.each { |l| return l.split[1] if l.include? "Query" }
    end
    "" # if got here: broken file
  end

  # tries to find basename in given dirs, returns path of first found or nil
  def file_in_dirs(basename, dir_list, case_sensitive = true)
    candidate = dir_list.find do |dir|
      File.exist?(File.expand_path(basename, dir))
    end
    return File.expand_path(basename, candidate) if case_sensitive

    candidate = file_in_dirs(basename.upcase, dir_list, true)
    return File.expand_path(candidate, basename) if candidate
    file_in_dirs(basename.downcase, dir_list, true)
  end

  module_function :extract_pattern, :put_basename_in_pattern,
                  :get_name_from_file_dot_suffix, :get_name_from_pdb,
                  :get_name_from_ent, :get_name_from_hhm,
                  :get_name_from_hhm_file, :get_name_from_hhr_file, :guess_pdb,
                  :get_name_from_hhm_nameline, :file_in_dirs
end
