#!/usr/bin/env ruby

kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "globals"
require kernel_path + "fileopen"
require kernel_path + "utils"

require "set"

targets = any_readlines(ARGV[0]).to_set

any_open_b(ARGV[1]) do |file|
  file.each do |line|
    ar = line.split
    puts line.chomp if targets.include?(ar[0]) || targets.include?(ar[1])
  end
end
