id = ARGV[0].split('.')[0]
puts(<<-EOT)
---
:targets:
- id: #{id}
  hhm: "/shared/home/snepomny/maya_nov_18/PDBs/#{id}.pdb.bseq.hhm"
  aln: "/shared/home/snepomny/maya_nov_18/PDBs/#{id}.aln"
  xyz: "/shared/home/snepomny/maya_nov_18/PDBs/#{id}.pdb.xyzni"
  data_dirs:
  - "/shared/home/snepomny/pdb_hhm_90/yaml/"
  - "/shared/home/snepomny/pdb_hhm_90/pdb90_mmCIF/"
  - "/shared/home/snepomny/maya_nov_18/yaml/"
  - "/shared/home/snepomny/maya_nov_18/PDBs/"
  hhr: "/shared/home/snepomny/maya_nov_18/PDBs/#{id}.pdb.bseq.hhr"
  flat: "/shared/home/snepomny/maya_nov_18/PDBs/#{id}.pdb.hhr.flat"
EOT
