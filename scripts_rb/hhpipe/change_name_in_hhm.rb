def replace_name(line, toup=true)
  name = line.split()[1]
  name_ar = name.split("_")
  if toup
    name_ar[0].upcase!
  else
    name_ar[0].downcase!
  end
  line.sub(name, name_ar.join("_"))
end

def usefname(fname)
  "NAME  " + File.basename(fname, ".hhm").gsub("c_theme_", "")
end

def change_name_for_file inp
  lines = IO.readlines(inp)
  lines.each { |line|
    line = yield(line) if line.start_with? "NAME"
    puts line
  }
end

change_name_for_file ARGV[0] { |line| usefname(ARGV[0]) }
