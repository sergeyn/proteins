kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "globals"
require kernel_path + "blosum"
GAP = "-".freeze

def get_alignment_metrics(seq_q, seq_s)
  raise "unequal alignment strings for #{ARGV[0]}\n#{seq_q}\n#{seq_s}\n" unless seq_q.size == seq_s.size

  blsm62 = Blosum.new(raw_blosum_ncbi_style)
  ident = 0
  similar = 0
  gaps = 0
  seq_q.zip(seq_s).each do |l, r|
    if l.include?(GAP) || r.include?(GAP) # if gap
      gaps += 1
      next
    end
    ident += l == r ? 1 : 0 # match
    # note blsm will throw exception on wrong letter
    similar += blsm62[l, r].positive? ? 1 : 0
    # print l,r
    # STDERR.puts ARGV[0].to_s if seq_q[i].include? '@' or seq_q[i].include? '#' or seq_s[i].include? '@' or seq_s[i].include? '#'
  end
  [seq_q.size, ident, similar, gaps]
end

# will construct TMScore friendly ATOMS line from raw xyz and a counter
def form_legal_tmscore_line(counter, xyz_arr)
  raise "xyz_arr is nil in form_legal_tmscore_line" unless xyz_arr

  xyz_arr = xyz_arr.split(Const.xyzrdelim) if xyz_arr.is_a?(String)
  x = format("%8.3f", xyz_arr[0].to_f)
  y = format("%8.3f", xyz_arr[1].to_f)
  z = format("%8.3f", xyz_arr[2].to_f)

  spaces_ = " " if counter < 1000
  spaces_ = "  " if counter < 100
  spaces_ = "   " if counter < 10
  format("ATOM      1  CA  ILE A%s%d    %s%s%s", spaces_, counter, x, y, z)
end

def skip_io_until(io, str, _ctrl_str = "stringthatcannverexistinmostofourtexts")
  while (line = io.gets) # && !line.include?(str) && !line.include?(ctrl_str)
    return line if line.include?(str)
  end
end

# read line by line until delim. and put to arr
def get_lines_until_delim_from_f(f, delim, delim2 = delim)
  arr = []
  line = f.gets
  return arr unless line && !line.empty?

  until line.include?(delim) || line.include?(delim2)
    arr << line.chomp
    line = f.gets
  end
  arr
end

def do_with_lines(filename = ARGV[0], limit = 123_456_789, &_block)
  File.open(filename) do |f|
    f.each_with_index do |line, counter|
      yield line
      break if counter > limit
    end
  end
end

# def should_filter_out?(hhmfile, entfile) # for .ents in domains experiment
#   return "missing file" unless File.exist?(hhmfile) && File.exist?(entfile)
#   File.open0(entfile).each do |l|
#     # if in .ent file the astral-spaci data is not good enough
#     return "ASTRAL-SPACI" if l.include?("ASTRAL-SPACI") && l.split[-1].to_f < 0
#     return "ASTRAL-AEROSPACI" if l.include?("ASTRAL-AEROSPACI") && l.split[-1].to_f < 0.2
#     return "" if l.include? "ATOMS" # got to the atoms section -- break
#   end
#   ""
# end

def length_of_alignment(q, s)
  raise "wrong alignment" unless q.size == s.size

  counter = 0
  q.each_with_index { |c, i| counter += 1 if c != GAP && s[i] != GAP }
  counter
end

# simplest parser of TMScore output
def parse_tmscore_lines(lines)
  lines.split("\n").map { |line| line.split(" ")[0] }.join Const.flatdelim
end

def needle(sequence, reference)
  fill_with = GAP
  gap = -1
  rows = reference.size + 1
  cols = sequence.size + 1
  maxsz = [rows, cols].max
  a = Array.new(maxsz) { Array.new(maxsz, 0) }
  d = Array.new(maxsz) { Array.new(maxsz, 0) }

  (1...rows).each do |i|
    (1...cols).each do |j|
      mismatch_pay = sequence[j - 1] == reference[i - 1] ? 1 : -1
      choice1 = a[i - 1][j - 1] + mismatch_pay
      choice2 = a[i - 1][j] + gap
      choice3 = a[i][j - 1] + gap
      a[i][j] = [choice1, choice2, choice3].max
      d[i][j] = 1 if choice1 == a[i][j]
      d[i][j] = 2 if choice2 == a[i][j]
      d[i][j] = 3 if choice3 == a[i][j]
    end
  end

  ref = []
  seq = []

  i = reference.length
  j = sequence.length
  while i > 0 && j > 0
    if d[i][j] == 1 # diagonal case
      ref << reference[i - 1]
      seq << sequence[j - 1]
      i -= 1
      j -= 1
    elsif d[i][j] == 2 # left case
      ref << reference[i - 1]
      seq << fill_with
      i -= 1
    elsif d[i][j] == 3 # up case
      ref << fill_with
      seq << sequence[j - 1]
      j -= 1
    end
  end

  # remaining stuff
  while i > 0
    ref << reference[i - 1]
    seq << fill_with
    i -= 1
  end
  while j > 0
    ref << fill_with
    seq << sequence[j - 1]
    j -= 1
  end

  throw "wrong alignment" unless ref.size == seq.size
  [seq.reverse, ref.reverse]
end

# Expected base class
class ExP
  def initialize(val)
    @value = if val && val.is_a?(ExP)
               val.get!
             else
               val
             end
  end

  def valid?
    false
  end

  def err
    raise ArgumentError, "Not an Err instnace"
  end

  # returns:
  # if valid: a new Exp wrapping the result of yielding a value
  # else: self
  def then
    if valid?
      from_block = yield(@value, self)
      return from_block if from_block.is_a?(ExP)

      return ExpOK.new(from_block)
    end
    self
  end

  def fail
    yield(err, self) unless valid?
    self
  end

  def get
    raise ArgumentError, "get on invalid Exp" unless valid?

    @value
  end

  # unlike get will return the value regardless of validity
  def get!
    @value
  end
end

# Happy path for expected
class ExpOK < ExP
  def initialize(val)
    if val && val.is_a?(ExP) # copy ctor
      raise ArgumentError, "Only OKs are allowed" unless val.valid?
    end
    super(val)
  end

  def valid?
    true
  end
end

# Error path for expected
class ExpErr < ExP
  def initialize(msg, val: nil)
    super(val)
    @errmsg = msg
  end

  def err
    @errmsg
  end
end
