kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "utils"
require kernel_path + "blosum"
require kernel_path + "astrals"
require kernel_path + "naming"
require kernel_path + "globals"
require kernel_path + "pdb_get_xyz_fasta"

DASH = "-"
STAR = "*"

# return an array of single-letter residues
def seq_and_mask(fname, basename)
  f = File.new(fname)
  skip_io_until(f, ">ss_dssp")
  mask = get_lines_until_delim_from_f(f, ">").join("").split("")
  f = File.new(fname) if mask.empty? # probably hhm without dssp section
  skip_io_until(f, ">" + basename)
  arr = get_lines_until_delim_from_f(f, ">", "#").join("").split("")
  if mask.empty?
    mask = ["c"] * arr.size
    # ERR.puts 'warning: empty mask'
  end
  puts "for #{fname} #{arr.size}!=#{mask.size}\n#{arr}\n#{mask}" unless arr.size == mask.size
  [arr, mask]
end

# apply mask to hhm residues, convert tlas of pdb
def prepare(arr_pdb, arr_hhm, arr_hhm_mask)
  return [], [] if (arr_pdb.size * arr_hhm.size).zero?
  return [], [] if arr_pdb[0].empty?
  res = arr_hhm_mask.each_with_index.map { |m, i| m == DASH ? STAR : arr_hhm[i] }
  [res, arr_pdb.map { |pdb| tla_to_one pdb[3] }]
end

def simple_case(res)
  ret_arr = []
  pdb_i = 0
  res.each do |r|
    ret_arr << (r == STAR ? r : pdb_i)
    pdb_i += 1 if r != STAR
  end
  ret_arr
end

def hard_case(res, pdb_res)
  #puts "hard case"
  res_a, pdb_a = needle(res, pdb_res)
  # after alignment possible cases match,gapH,gapP,mask,maskGap,mismatch
  # hhm: a-c**f
  # pdb: ab-d-e
  pdb_i = 0
  ret_arr = []
  res_a.each_with_index do |r, i|
    if pdb_a[i] == r # can't have a - vs - case, must be a match
      ret_arr << pdb_i
    elsif r == STAR  # mask hhm side
      ret_arr << STAR
    elsif pdb_a[i] == DASH # a gap from pdb side!
      ret_arr << DASH
    elsif r == DASH
      puts [pdb_i, pdb_a[i], r].join "~~" if false
    else # mismatch or a gap from hhm side
      puts [pdb_i, pdb_a[i], r].join "__" if false
      ret_arr << DASH
    end
    pdb_i += 1 unless pdb_a[i] == DASH
  end
  ret_arr
end

# example: 4dui
# ****HHHHHHGSDLGKKL <- is the masked residues
# an alignment starting from KKL... will be indexed as 16,17,18,...
# but in PDB ATOMs section KKL is indexed as  12,13,14 ...
def align_masked_hhm_to_pdb(arr_pdb, arr_hhm, arr_hhm_mask, threshold = 0.2)
  res, pdb_res = prepare(arr_pdb, arr_hhm, arr_hhm_mask)
  return [] if (res.size * pdb_res.size).zero?

  # usually it is the simple case
  ret_arr = res - [STAR] == pdb_res ? simple_case(res) : hard_case(res, pdb_res)

  throw "!!! invalid ret" if !ret_arr.empty? && !validate_ret(ret_arr, res, pdb_res)

  # turning this off for Miri:
  cntM = calculate_aln_sanity(res)
  if cntM > threshold
    ret_arr = []
    puts "Alignment sanity: #{cntM}"
  end
  ret_arr
end

def validate_ret(ret_arr, res, pdb_res)
  unless ret_arr.size == res.size
    puts "#{ret_arr.size} != #{res.size}"
    puts res.join ","
    puts ret_arr.join ","
    return false
  end
  ret_arr.each_with_index { |r, i| return false if r != DASH && r != STAR && res[i] != pdb_res[r] }
  true
end

# we count all cases of a residue followed by gap
# the metric is leter_gap_count / res.size
def calculate_aln_sanity(res_arr)
  letter_gap = 0
  letter = false
  res_arr.each do |r|
    if r == DASH && letter
      letter = false
      letter_gap += 1
    elsif r != DASH && r != STAR
      letter = true
    end
  end
  letter_gap.to_f / res_arr.size
end

def concat_hhms_if_aln(blob)
  Dir[blob].each do |f|
    next unless File.exist?(f + Const.alnsuffix)
    puts IO.readlines(f).join("")
  end
end

def rename_unless_aln(blob)
  Dir[blob].each { |f| File.rename(f, f + ".bad") unless File.exist?(f + Const.alnsuffix) }
end

# a single entry is between 'HHsearch' line and '//' line
def split_hhm_db(fname, dest = "./")
  entry = []
  File.new(fname).each do |line|
    if line.include? "HHsearch"
      entry = []
    elsif line.index("//").zero?
      entry << line
      name = dest + entry[1].split[-1] + ".hhm"
      File.open(name, "w") { |f| f.puts entry.join }
    end
    entry << line
  end
end

if $PROGRAM_NAME == __FILE__
  if ARGV.size < 3
    STDERR.puts "Usage: [hhm file name or a blob] [make xyzni from name pattern] [suffix for all hhm files]"
    puts "Example: #{$PROGRAM_NAME} 'testData/*hhm' 'testData/pattern.pdb.xyzni' '.hhm'"
  else
    Dir[ARGV[0]].each do |f|
      hhname = (Naming.get_name_from_hhm_file f)[0]
      next if hhname.empty?
      arr_hhm, arr_mask = seq_and_mask(f, hhname)
      base_f = Naming.get_name_from_file_dot_suffix(f, ARGV[2])
      puts base_f, Naming.put_basename_in_pattern(base_f, ARGV[1])
      xyz = read_xyzni Naming.put_basename_in_pattern(base_f, ARGV[1])
      aligned = align_masked_hhm_to_pdb(xyz, arr_hhm, arr_mask)
      File.open(f + Const.alnsuffix, "w") { |a| a.puts(aligned.join("\n")) } unless aligned.empty?
    end
  end
end
