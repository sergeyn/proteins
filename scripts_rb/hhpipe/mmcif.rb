# frozen_string_literal: true

require "net/http"
require "set"

kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "astrals"
require kernel_path + "fileopen"
require kernel_path + "globals"
require kernel_path + "utils"

ATOM = "ATOM"

# get mmcif as string from https server (may return almost empty mmcifs!)
def fetch_mmcif(protein = "1msn", chain = "A", use_ssl=false)
  suri = "://webchem.ncbr.muni.cz/CoordinateServer/#{protein}/chains?authAsymId=#{chain}"
  uri = URI((use_ssl ? "https" : "http" ) + suri)
  Net::HTTP.start(uri.host, uri.port, use_ssl: use_ssl) do |http|
    request = Net::HTTP::Get.new uri
    response = http.request request
    return response.body if response.code == "200"

    puts "#{uri} returned #{responce.code}"
  end
  nil
end

# name is case sensitive xxxx_Y where Y is chain
# fetch the mmcif from server and save in output_path file
def download_mmcif(name, output_path)
  protein, chain = name.chomp.split("_")
  data = fetch_mmcif(protein, chain)

  File.open(output_path, "w") do |f|
    f.puts data
  end

  File.open(output_path) do |f|
    File.open(output_path.sub(".mmcif", ".xyzni"), "w") do |out|
      out.puts cif_get_atoms(f).join("\n")
    end
  end
end

def _dest(dest_path, name)
  "#{dest_path}/#{name}.mmcif"
end

# each name is case sensitive xxxx_Y where Y is chain
# block is to call a getter function with signature (name, dest_file)
# download all proteins using &block and store in dest_path 
def mmcif_get_all(names, dest_path, ignore_if_exist = true, threads_count = 8, &block)
  names.to_set.each_slice(threads_count) do |lnames|
    ids = []
    lnames.each do |name|
      dest = _dest(dest_path, name)
      next if File.exist?(dest) && ignore_if_exist

      ids << Thread.new { block.call(name, dest) }
    end
    ids.each(&:join) # not ideal, but expect the tasks to be homogenic
  end
end

# make sure coordinates are proper floats
def _verify_coords(xyz, fname)
  xyz.each do |n|
    begin
      Float(n)
    rescue ArgumentError
      puts "Warning: #{fname} bad coord: #{n}"
    end
  end
end

#  0    1   2 3  4  5  6 7 8 9 10       11      12    13   14
# ATOM 2561 C CA . ALA B 1 4 ? 51.680 20.075 23.589 1.00 22.33 ? ? ? ? ? ? 4 ALA B CA 1
# ATOM 2566 C CA . TYR B 1 5 ? 48.618 20.774 21.443 1.00 6.52 ? ? ? ? ? ? 5 TYR B CA 1
# ATOM 2578 C CA . ILE B 1 6 ? 46.292 19.597 24.271 1.00 19.38 ? ? ? ? ? ? 6 ILE B CA 1
# 16.004   36.186   30.742 ALA 7
def cif_get_atoms(fname)
  lines = []
  any_open_b(fname) do |f|
    lines = f.each.map(&:chomp).select { |line| line.start_with?(ATOM) && line.include?("CA") }
  end
  ret = []
  seqid = ""
  lines.each do |line|
    ar = line.split(" ")
    _verify_coords(ar[10..12], fname)
    next if seqid == ar[8] # ignore 2nd CA in same seqid?

    seqid = ar[8]
    ret << [ar[10], ar[11], ar[12], ar[5], seqid].join(" ")
  end
  ret
end

if $PROGRAM_NAME == __FILE__
  if ARGV.size < 2
    puts "Usage: [file name or a blob] [suffix]"
  else
    Dir[ARGV[0]].each do |f|
      File.open(f.sub("." + ARGV[1], "") + Const.xyzsuffix, "w") do |out|
        out.puts cif_get_atoms(f).join("\n")
      end
    end
  end
end
