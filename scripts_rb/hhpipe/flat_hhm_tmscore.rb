kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "utils"
require kernel_path + "blosum"
require kernel_path + "astrals"
require kernel_path + "naming"
require kernel_path + "pdb_get_xyz_fasta"
require kernel_path + "globals"

require "optparse"
require "ostruct"

# rubocop:disable Metrics/AbcSize, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity

# typical line of .xyzni file:
#-82.462 -2.374 89.540 SER 1

# typical line of .hhr.flat file:
# 0      1      2   3  4   5    6  7 8  9  10   11         12          13    14
# q      s
# 1a34_A 2x5r_A 9.5 33 39 0.620 0 10 42 85 123 R--...PAIP AA...LSAIV 14.64 24.90

DASH = "-".freeze
STAR = "*".freeze
DASH_STAR = [DASH, STAR].freeze

def load_aln(fname)
  return [] unless File.exist? fname
  File.readlines(fname).each.map do |l|
    l.include?(DASH) || l.include?(STAR) ? l.chomp : l.to_i
  end
end

def convert_seq_to_xyz(offset, seq, aln, xyz) # the offset is minus-oned
  ret_arr = []
  aln_i = 0
  seq.each do |r|
    if r == DASH # gap
      ret_arr << DASH
    else
      index = aln[offset + aln_i]
      if DASH_STAR.include? index
        ret_arr << index
      else
        atoms = xyz[index]
        raise atoms.join(" ") + "|" + r unless tla_to_one(atoms[3]) == r
        ret_arr << atoms
      end
      aln_i += 1
    end
  end
  ret_arr
end

def name_temporaries(left_right)
  [Const.tmp + left_right + "_left", Const.tmp + left_right + "_right"]
end

# store xyzs in tmp files and return the new stats
def store_xyz_in_temp_files(left_right, xyz_l, xyz_r)
  new_seq_lr = [[], []]
  name_temporaries(left_right).each_with_index do |tempf, l_or_r|
    File.open(tempf, "w") do |f|
      xyz_l.zip(xyz_r).each.with_index(1) do |xyzlr, i|
        next if DASH_STAR.include?(xyzlr[0]) || DASH_STAR.include?(xyzlr[1])
        f.puts form_legal_tmscore_line(i, xyzlr[l_or_r])
        new_seq_lr[l_or_r] << tla_to_one(xyzlr[l_or_r][3])
      end # residues
    end # file
  end
  get_alignment_metrics(new_seq_lr[0], new_seq_lr[1]) # len,ident,sim,gaps
end

def get_xyz_aln_for_target(target)
  xyz = HHpipe::Target.path_from_target(target, "xyz")
  aln = HHpipe::Target.path_from_target(target, "aln")

  xyz_query = read_xyzni(xyz)
  aln_query = load_aln(aln)
  logger.warning "Missing aln for #{target}" if aln_query.empty?
  [xyz_query, aln_query]
end

def get_xyz_aln_for_id(id, data_dirs)
  target = HHpipe::Target.load_target_for_id(id, data_dirs)
  raise "Must have a yaml for #{id}" unless target
  get_xyz_aln_for_target target
end

def get_xyz_aln_for_both(ids_arr, data_dirs, query, xyz_query, aln_query)
  if ids_arr[0] == query
    [xyz_query, aln_query] + get_xyz_aln_for_id(ids_arr[1], data_dirs)
  else
    get_xyz_aln_for_id(ids_arr[0], data_dirs) + [xyz_query, aln_query]
  end
  # xyz_left = arr[0] == query ? xyz_query : read_xyzni(Naming.put_basename_in_pattern(arr[0], options.pdb_pattern)) # File.expand_path(arr[0] + Const.xyzsuffix, data_dir))
  # xyz_right = arr[1] == query ? xyz_query : read_xyzni(Naming.put_basename_in_pattern(arr[1], options.pdb_pattern))
  # aln_left = arr[0] == query ? aln_query : load_aln(Naming.put_basename_in_pattern(arr[0], options.aln_pattern))
  # aln_right = arr[1] == query ? aln_query : load_aln(Naming.put_basename_in_pattern(arr[1], options.aln_pattern))
end

def handle_single_flat_line(arr, data_dirs, query, xyz_aln, tmscore)
  # xyz_query = read_xyzni(Naming.put_basename_in_pattern(query, options.pdb_pattern))
  # query_aln = Naming.put_basename_in_pattern(query, options.aln_pattern)
  return "no query in pair" unless arr[0..1].include? query
  xyz_left, aln_left, xyz_right, aln_right = get_xyz_aln_for_both(arr, data_dirs, query, xyz_aln[0], xyz_aln[1])

  return "no_aln #{arr[0]}" if aln_left.empty?
  return "no_aln #{arr[1]}" if aln_right.empty?

  offset_left = arr[7].to_i - 1 # note: the offsets start from 1!
  offset_right = arr[9].to_i - 1 # note: the offsets start from 1!

  seq_l = arr[11].chomp.split("")
  seq_r = arr[12].chomp.split("")
  raise "seq.al length is wrong #{seq_l.size}!=#{seq_r.size}" unless seq_l.size == seq_r.size

  xyz_l = convert_seq_to_xyz(offset_left, seq_l, aln_left, xyz_left)
  xyz_r = convert_seq_to_xyz(offset_right, seq_r, aln_right, xyz_right)
  raise "xyz.alignment len is wrong" unless xyz_l.size == seq_l.size
  raise "wrong xyzs" unless xyz_l.size == xyz_r.size

  new_stats = store_xyz_in_temp_files(arr[0..1].join("_"), xyz_l, xyz_r)

  # get tmscore data
  prot1, prot2 = name_temporaries(arr[0..1].join("_"))
  tmscore_raw = `#{tmscore} #{prot1} #{prot2} | grep -e RMSD -e '-score' | grep -v RMSD= | grep -v predict | cut -d'=' -f2 `
  tm = parse_tmscore_lines tmscore_raw
  tm = "* * ERR empty intersection" if tm.size < 5
  [prot1, prot2].each { |f| File.delete(f) }
  # write new alignment,stats and tmscore data to result file, conviniently prepend tm with '* *'
  [arr.join(Const.flatdelim), new_stats.join(Const.flatdelim), tm].join Const.flatdelim
end

# TODO: merge with postprocess
def each_file(filename, options)
  query = Naming.get_name_from_file_dot_suffix(filename, options.suffix)
  xyz_query = read_xyzni(Naming.put_basename_in_pattern(query, options.pdb_pattern))
  query_aln = Naming.put_basename_in_pattern(query, options.aln_pattern)
  aln_query = load_aln(query_aln)
  if aln_query.empty?
    puts "skip -- missing #{query_aln}"
    return
  end

  IO.readlines(filename).each do |line|
    begin
      arr = line.split(Const.flatdelim)
      next if arr[4].to_f < options.pident
      puts handle_single_flat_line(arr, options.data_dirs, query, [xyz_query, aln_query], options.tmscore)
    rescue StandardError => e
      puts line.chomp + " ERR " + e.message
      STDERR.puts e.backtrace[0]
    end
  end # each line
end

def arg_parse(args)
  options = OpenStruct.new
  options.flat_blob = ""
  options.suffix = ".hhr.flat"
  options.pdb_pattern = ""
  options.aln_pattern = ""
  options.lookup_file = nil
  options.tmscore = Const.tmscore
  options.pident = 30
  options.data_dirs = "."

  opt_parser = OptionParser.new do |opts|
    opts.on(
      "-f", "--flats_blob BLOB",
      "A BLOB string for the flat files"
    ) do |v|
      options.flat_blob = v
    end
    opts.on(
      "-d", "--data_dirs DATA_DIRS",
      "DATA_DIRS for this target"
    ) do |v|
      options.data_dirs = v
    end
    opts.on(
      "-s", "--suffix [SUFFIX]",
      "A SUFFIX string, defaults to .hhr.flat"
    ) do |v|
      options.suffix = v
    end
    opts.on(
      "-p", "--pdbpattern PDBPATTERN",
      "A PDBPATTERN string, how to find a pdb file from protein name"
    ) do |v|
      options.pdb_pattern = v
    end
    opts.on(
      "-i", "--pident [PIDENT]",
      "Percent identity cutoff -- only take those above or equal PIDENT"
    ) do |v|
      options.pident = v.to_f
    end
    opts.on(
      "-a", "--alnpattern ALNPATTERN",
      "An ALNPATTERN string, how to find an aln file from protein name"
    ) do |v|
      options.aln_pattern = v
    end
    opts.on(
      "-L", "--lookup LOOKUPFILE",
      "A path to LOOKUPFILE for locating protein data. If used, pdb/aln patterns are ignored"
    ) do |v|
      options.lookup_file = v
    end
    opts.on(
      "-t", "--tmscore [TMSCORE]",
      "A path to TMscore binary. Defaults to #{Const.tmscore} and assumes it is in $PATH"
    ) do |v|
      options.tmscore = v
    end
    opts.on_tail("-h", "--help", "Show this message") do
      puts opts
      exit
    end
  end
  opt_parser.parse!(args)
  options
end

def main
  # note: we assume that there is only one <query> sequence in the file
  # we cache its data (xyzni, aln) and reuse it for each line.
  # if this is not the case, we skip the line!
  options = arg_parse ARGV
  Dir[options.flat_blob].each { |f| each_file f, options }
end

main if $PROGRAM_NAME == __FILE__
