# Constants
class Const
  @dbsuffix = ".yaml"
  @fastasuffix = ".bseq"
  @xyzsuffix = ".xyzni"
  @alnsuffix = ".aln"
  @flatsuffix = ".flat"
  @ressuffix = ".res"
  @xyzrdelim = " "
  @flatdelim = " "
  @tmp = "/tmp/"
  @tmscore = "TMscore_32"

  class << self
    attr_reader :fastasuffix, :xyzsuffix, :alnsuffix, :flatsuffix, :ressuffix,
                :xyzrdelim, :flatdelim, :tmp, :tmscore, :dbsuffix
  end
end

require "logger"

# opens HHpipe namespace and sets default log
module HHpipe
  class << self
    attr_writer :logger

    def logger
      return @logger if @logger

      @logger = Logger.new(STDOUT)
      @logger.level = ENV["RB_LOGGER"] ? Logger.const_get(ENV["RB_LOGGER"]) : Logger::INFO
      @logger.formatter = proc do |severity, datetime, progname, msg|
        date_format = datetime.strftime("%Y%m%d.%H:%M:%S")
          "#{severity[0]} #{date_format} #{progname}: #{msg}\n"
      end

      @logger
    end
  end
end

HHpipe.logger.debug("globals") { "Logger started" }
