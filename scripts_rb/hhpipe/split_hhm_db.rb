#!/usr/bin/env ruby

# todo, support index files!
kernel_path = File.expand_path(__FILE__).split("/")[0..-2].join("/") + "/"
require kernel_path + "globals"
require kernel_path + "utils"

name = nil
hhm_db = ARGV[0]
dest_dir = ARGV[1]
select_file = ARGV[2]

selected = {} 
if select_file
  File.new(select_file).each { |l| selected[l.chomp]=1}
end

f = File.new("/dev/null", "w")
File.new(hhm_db).each.each_with_index do |l, i|
  if l.start_with?("NAME ")
    name = l.split[1] # NAME  e1htr.1 P:1-43,B:1-329 000000011 <- for domains
    f.close
    #f = File.new("/dev/null", "w")
    #next if selected.size && !selected[name]
    #f.close
    f = File.open(File.expand_path(name + ".hhm", dest_dir), "w")
  end
  f.puts l
  puts i if (i % 100_000).zero?
end

