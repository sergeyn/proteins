def fopen_gz(fname)
  if fname =~ /\.gz$/
    require "zlib" # here because some installs don't have zlib (ikr)
    return Zlib::GzipReader.new(File.open(fname))
  end
  File.new(fname)
end

def any_open_b(fname)
  hndl = fopen_gz(fname)
  yield hndl
ensure
  hndl.close if hndl
end

def any_readlines(fname)
  lines = []
  any_open_b(fname) do |f|
    lines = f.each_line.map(&:chomp)
  end
  lines
end
