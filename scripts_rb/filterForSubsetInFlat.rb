require 'set'
$subset = File.new(ARGV[1]).each.map {|l| l.chomp }.to_set

def processFile f
	File.new(f).each { |line|
		pair = line.split()[0..1]
		puts line.chomp if $subset.include?(pair[0][1..5].upcase) and $subset.include?(pair[1][1..5].upcase)
	}
end

Dir[ARGV[0]].each do |flat|
	processFile(flat)		
end


