#require 'xmlsimple'
load '/var/lib/gems/1.8/gems/xml-simple-1.1.4/lib/xmlsimple.rb'

#interface
#  id
#  area
#  molecule    //left
#    id        //1
#    chain_id
#    class     //we need it to be 'Protein'
#    residues
#	residue
#	  solv_en //we need to be > 0.0
#	  name    //tla
#  	  seq_no  //pdb index
#	  bonds   // HS, etc.
#	  ser_num
#	residue
#	  ...
#  molecule //right
#    id //2
#    ...
$chainsFile = File.new(ARGV[0].to_s+"_chainid.sif","w")
$edgesFile = File.new(ARGV[0].to_s+"_edgeWeights.csv","w")
$edgesFile.puts "edge_id,int_area"

#---------------------------------------------------------
def chainFilter chain #For our case: remove the A (RNA) chain, the B (RNA) chain, the u (RNA) chain, the t (poly A) chain
#	chain!='u' and chain!='t' and chain !='A' and chain !='B' 
	true
end 
#---------------------------------------------------------
def edgeFilter rng1,rng2, area
	area>=100 and chainFilter(rng1) and chainFilter(rng2)
end
#---------------------------------------------------------
def xmlGetAllChildrenNamed element,name
	element[name]
end
#---------------------------------------------------------
def xmlGetFirstChildNamed element,name
	element[name][0]
end
#---------------------------------------------------------
def handleResidue chain,residue
#<ser_no>8</ser_no>
#<name>SER</name>
#<seq_num>3</seq_num>
#<bonds>H</bonds>
#<solv_en>0.083632066189</solv_en>
	if xmlGetFirstChildNamed(residue,'bonds').class == String || xmlGetFirstChildNamed(residue,'solv_en').to_f != 0
		#return [ chain, xmlGetFirstChildNamed(residue,'seq_num'), xmlGetFirstChildNamed(residue,'name') ].join ' '
		return  xmlGetFirstChildNamed(residue,'seq_num')
	end
	return nil
end
#---------------------------------------------------------
def handleMolecule molecule
	chain = xmlGetFirstChildNamed(molecule,'chain_id')
	return chain,[] unless  xmlGetFirstChildNamed(molecule,'class')=='Protein'
	res = xmlGetAllChildrenNamed(xmlGetFirstChildNamed(molecule,'residues'),'residue').map { |residue| handleResidue chain,residue }
	return chain,res
end
#---------------------------------------------------------
def handleOneInterface interface
	id = xmlGetFirstChildNamed(interface,'id')
	int_area = xmlGetFirstChildNamed(interface,'int_area').to_f

	molecules = xmlGetAllChildrenNamed(interface,'molecule')
	throw 'not enough molecules' unless molecules.size == 2

	chain1,s1 = handleMolecule(molecules[0])
	chain2,s2 = handleMolecule(molecules[1])
	s1.compact!
	s2.compact!
	
	return unless edgeFilter chain1,chain2,int_area
	return if s1.empty? or s2.empty?
	
	$chainsFile.puts [chain1, id, chain2].join " "
	$edgesFile.puts [id,int_area].join ',' 
	print [id,chain1,chain2,s1.join(','),s2.join(',')].join ' '
	puts "\n<br>\n"
end
#---------------------------------------------------------
# load, parse, and store parsed 
fnameP = ARGV[0]+'.parsed'
config = {}
if File.exists?(fnameP) 
	STDERR.puts "loading from parsed file"
	config = File.open(fnameP) do|file| Marshal.load(file) end 
else
	STDERR.puts "loading from file"
	config = XmlSimple.xml_in(ARGV[0])
	File.open(ARGV[0]+'.parsed','w') do|file| Marshal.dump(config, file) end
end	
#---------------------------------------------------------
interfacesCount = xmlGetFirstChildNamed(config,'n_interfaces').to_i
STDERR.puts "#{interfacesCount} interfaces"
interfaces = xmlGetAllChildrenNamed(config,'interface')
throw "interface data struct is broken" unless interfaces.size == interfacesCount
#loadTranslationTable
puts "edgeid chain1 chain2 residues1 residues2\n<br>"
interfaces.each { |interface|  handleOneInterface interface}
