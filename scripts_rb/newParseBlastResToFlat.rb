kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'
require kernel_path+'log_lib'
require kernel_path+'utils'

require 'stringio'

#-------------------------------------------------------------------
#       Query  1    MNIFEMLRIDEGLRLKIYKDTEGYYTIGIGHLLTKSPSLNSLDAAKSELDKAIGRNTNGV  60
#                   MNIFEMLRIDEGLRLKIYKDTEGYYTIGIGHLLTKSPSLN   AAKSELDKAIGRNTNGV
#       Sbjct  1    MNIFEMLRIDEGLRLKIYKDTEGYYTIGIGHLLTKSPSLN---AAKSELDKAIGRNTNGV  57
def parse_alignment_block(file)
        seq_q = []
        seq_s = []
        line = skip_io_until(file,'Query')
        while line do
                parts = line.split
                left_q = parts[1].to_i if !left_q
                right_q = parts[3].to_i
                seq_q << parts[2]

                line = skip_io_until(file,'Sbjct')
                parts = line.split
                left_s = parts[1].to_i if !left_s
                right_s = parts[3].to_i
                seq_s << parts[2]

                line = skip_io_until(file,'Query','Score')
                line = nil if !line or line.include? 'Score'
        end
        return [left_q.to_s, right_q.to_s, left_s.to_s, right_s.to_s, seq_q.join(""),seq_s.join("")]
end
#-------------------------------------------------------------------
def length_of_alignment(q,s)
        counter = 0
        i = 0
        q.size.times do
                counter +=1 if q[i]!= '-' and s[i]!='-'
                i+=1
        end
        return counter
end
#-------------------------------------------------------------------
def iterate_blast_queries(blast_file, limit, &block)
        limit = 1234565789 if (!limit or limit<1)
        file = File.new(blast_file,'r')
        counter = 0
        query = []
        while( line = file.gets )
                if line.include? 'Query=' #starting new chain
                        yield query.join("") unless ( query.empty? or counter < 1)
                        query = []
                        counter = counter+1
                        #puts counter.to_s if(counter%1023==1)  #heartbeat
                end
                        query << line.to_s

                break if counter > limit
        end #while
        yield query.join("")
        return counter
end
#-------------------------------------------------------------------
def iterate_hits_of_single_query q, &block
        strio = StringIO.new(q)
        file = strio.filehandle
        counter = 0
        hit = []
        h_name='nil'
        while line = file.gets
                if line.include? '>' #starting new hit
                        yield hit.join(""),h_name  unless counter < 1
                        h_name= line.split()[0].split('>')[-1]
                        counter = counter + 1
                        hit = []
                end
                hit << line.to_s
        end #while
        yield hit.join(""),h_name if h_name != 'nil'
end
#-------------------------------------------------------------------
def parse_hit(r_name, h)
        strio = StringIO.new(h)
        file = strio.filehandle
        array_of_hits = []
        while true
                hit_hash = {}
                hit_hash[:r_name] = r_name

                line = skip_io_until(file,'Score')
                return array_of_hits if line == nil
                #Score =  598 bits (1543),  Expect = 2e-171, Method: Compositional matrix adjust.
                hit_hash[:evalue] = line.split(",")[1].split(" ")[2]
                hit_hash[:scoreb] = line.split(',')[0].split()[2]
                hit_hash[:score] = line.split(',')[0].split('(')[1].split(')')[0]
                line = skip_io_until(file,'Identities')
                # Identities = 20/74 (27%), Positives = 33/74 (44%), Gaps = 1/74 (1%)
                hit_hash[:gaps] = 0
                arr = line.split(",")
                hit_hash[:ident] = arr[0].split("(")[1].split("%")[0] if line.include? 'Identities'
                hit_hash[:positives] = arr[1].split("(")[1].split("%")[0] if line.include? 'Positives'
                hit_hash[:gaps] = arr[2].split("(")[1].split("%")[0] if line.include? 'Gaps'

                align_data = parse_alignment_block(file) #hit_hash[:length].to_i)
                hit_hash[:left_q]=align_data[0]
                hit_hash[:right_q]=align_data[1]
                hit_hash[:left_s]=align_data[2]
                hit_hash[:right_s]=align_data[3]
                hit_hash[:seq_q] = align_data[4]
                hit_hash[:seq_s]=align_data[5]
                hit_hash[:length] = length_of_alignment(hit_hash[:seq_q].split(''),hit_hash[:seq_s].split(''))
                array_of_hits << hit_hash
        end
end
#-------------------------------------------------------------------
 def parseBlastToFlat(blast_output_fname, limit=123456789)
    suffix = '.hhm'
	counter = 0
	f = File.new(blast_output_fname + '.flat','w')
	iterate_blast_queries(blast_output_fname, -1) { |q|
      q_name = blast_output_fname.split('/')[-1].split(suffix)[0] #get the name of a protein from file name
		puts "q_name: #{q_name}"
		iterate_hits_of_single_query(q) { |h,s_name|
			counter += 1
			#log_me counter.to_s if (counter % 1024 == 0)
			hsh = parse_hit(s_name,h)[0]

	        unless hsh
 				STDERR.puts 'warning: empty hit in parse' + LINE
				next
			end
        hsh[:r_name]=s_name.split(suffix)[0]
	        hsh[:l_name]=q_name
        arr = [hsh[:l_name],hsh[:r_name],hsh[:evalue],hsh[:length],hsh[:ident],hsh[:positives],hsh[:gaps],hsh[:left_q],hsh[:right_q],hsh[:left_s],hsh[:right_s],hsh[:seq_q],hsh[:seq_s],hsh[:scoreb],hsh[:score]]
        f.puts arr.join ' '  if hsh[:evalue].to_f<=10.0 and hsh[:seq_s].size >= 25

			return counter if counter>limit
	} #end iterate_hits_of_single_query
} #end iterate_blast_queries
	f.flush
	f.close
	return counter
end
#==============================================================================

  if __FILE__ == $0
    parseBlastToFlat ARGV[0]
  end
