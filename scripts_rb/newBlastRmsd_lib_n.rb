kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'
require kernel_path+'log_lib'
require kernel_path+'utils'
require kernel_path+'localEnv'
require kernel_path +'naming.rb'
require kernel_path +'astrals.rb'

$tmp = '/tmp/'
$xyzng_dir = '/shared/home/snepomny/propellers/'
$tmscore = '/shared/home/snepomny/TMscore_32 '
$xyzrdelim = ','
#-------------------------------------------------------------------
def align_str2xyz(offset,str,xyz,sgap='-',xgap='@')
	offset = offset.to_i - 1 # ? why had to add it?
  arr = []
  str.each_with_index { |c,i| 
    if ( c.include? sgap)
      arr << xgap
    else
      arr << xyz[offset].chomp rescue arr<<'#' 
      tla = xyz[offset].split($xyzrdelim)[3]
      unless $astrals[tla] == c
        raise "@#{i}: #{c} <-> #{ xyz[offset].split($xyzrdelim)[3]}"
      end 
      offset += 1
    end
  }
  arr
end
#-------------------------------------------------------------------
def namexyz(left,right,n)
	"#{$tmp}#{left}#{right}#{n}.xyz"	
end
#-------------------------------------------------------------------
def construct_temp_not_hhp_xyz(xyz_q,xyz_s,offset_q,offset_s,seq_q,seq_s,l_name,r_name)
        throw "misaligned!input #{seq_q.size} - #{seq_s.size}\n #{seq_q}\n #{seq_s}" if seq_q.size != seq_s.size
        #first we align strings to xyzs
        aligned_q = align_str2xyz(offset_q,seq_q,xyz_q)
        aligned_s= align_str2xyz(offset_s,seq_s,xyz_s)

        throw "misaligned!aligned" if aligned_q.size != aligned_s.size
        n_xyz_q = []
        n_xyz_s = []
        final_q = []
        final_s = []
	rachel_l = []
	rachel_r = []
        
        ind_q = 0
        ind_s = 0
        while ind_q < aligned_q.size and ind_s < aligned_s.size do
                if ( aligned_q[ind_q].include? $xyzrdelim and aligned_s[ind_s].include? $xyzrdelim) #aa
                        final_q << aligned_q[ind_q].split($xyzrdelim)[3]
                        final_s << aligned_s[ind_s].split($xyzrdelim)[3]
                        n_xyz_q << form_legal_tmscore_line(n_xyz_q.size+1, aligned_q[ind_q].chomp)
                        n_xyz_s << form_legal_tmscore_line(n_xyz_q.size, aligned_s[ind_s].chomp)
			rachel_l << aligned_q[ind_q].split($xyzrdelim)[4]
			rachel_r << aligned_s[ind_s].split($xyzrdelim)[4]                       
                        ind_q += 1
                        ind_s += 1
                else 
                        ind_q += 1
                        ind_s += 1
                end
        end
        throw "misaligned!!output" if n_xyz_q.size != n_xyz_s.size
	begin
        	fs = File.new(namexyz(l_name,r_name,'s'),"w")
	        fq = File.new(namexyz(l_name,r_name,'q'),"w")
	rescue
        	sleep(1)
        	begin
        		fs = File.new(namexyz(l_name,r_name,'s'),"w")
		        fq = File.new(namexyz(l_name,r_name,'q'),"w")
	        rescue
        	        return
        	end
	end
	puts (rachel_l.zip(rachel_r).map { |p| "#{p[0]}:ALA #{p[1]}:ALA" }).join " "
        fs.puts n_xyz_s.join("\n")
        fq.puts n_xyz_q.join("\n")
        fq.close
        fs.close
        
        []
        #return getAlignmentMetrics(final_q,final_s)
end
#------------------------------------
def rmsd_one_not_hhp_hit(l_name,r_name,blast_offset_q,blast_offset_s,blast_q,blast_s)
	$hhm_files = '/shared/home/snepomny/propellers/' #d4pwza2.ent.xyzng
  
  xyz_q = File.new($hhm_files+l_name+'.ent.xyzng').each.map {|l| l.chomp} #unless r.include? '#'
  xyz_s = File.new($hhm_files+r_name+'.ent.xyzng').each.map {|l| l.chomp} #unless r.include? '#'

	#puts xyz_q.join; puts xyz_s.join
	tm=[]
	begin
    tm = construct_temp_not_hhp_xyz(xyz_q,xyz_s,blast_offset_q.to_i,blast_offset_s.to_i,blast_q.split(''),blast_s.split(''),l_name,r_name) #note the change in q,s
    cmd = " #{$tmscore}  #{namexyz(l_name,r_name,'q')} #{namexyz(l_name,r_name,'s')}  | grep -e RMSD -e '-score' |  tail -6 | head -5 | cut -d'=' -f2"       
    #puts cmd
    tm = tm.join(' ')  + ' ' + parse_tmscore_lines(%x[#{cmd}])
  rescue Exception => e
    STDERR.puts e.inspect + " in " + l_name + " " + r_name
	  tm = ''
  end
  %x[rm -f  #{namexyz(l_name,r_name,'q')} #{namexyz(l_name,r_name,'s')} ]
  return tm
end

#-------------------------------------------------------------------
def work_on_flat_not_hhp_pairs(flat_fname)
  f = File.new(flat_fname)
  out = File.new(flat_fname+'.res','w')
  while (line=f.gets)
    arr = line.split(' ')
    next if arr[0] == arr[1] #skip self alignments
    tmscore_arr = rmsd_one_not_hhp_hit(arr[0], arr[1],arr[7],arr[9],arr[11].strip,arr[12].strip)
    out.puts "#{line.chomp} #{tmscore_arr.split().join($flatdelim)}"
  end
  out.flush
  out.close
end

if __FILE__ == $0
  work_on_flat_not_hhp_pairs(ARGV[0])
end 

