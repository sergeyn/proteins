kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'
require kernel_path+'/hhpipe/fileopen'

#                 0    1      2      3      4    5        6    7      8       9       10     11    12    13    14
#hhsearch flat: pair0,pair1,evalue,length,ident,similar,gaps,left_q,right_q,left_s,right_s,seq_q,seq_s,probab,score
#d1nr0a1 d1erja_ 2e-72 276 13 30 8 16 308 61 345 ARGTAVVLGNTPAGDKIQYCNGTSVYTVPVGSLTDTEIYTEHSH-QTTVAKTSPSGYYCASGDVHGNVRIWDTTQTTHILKTTIPVFSGPVKDISWDSESKRIAAVGEGRERFGHVFLFDTGTSNGNLTGQARAMNSVDFKP-SRPFRIISGSDDNTVAIFEGPPFKFKSTF-GEHTKFVHSVRYNPDGSLFASTGGDGTIVLYNGVDGTKTGVFEDDSLKNVAHSGSVFGLTWSPDGTKIASASADKTIKIWNVATLKVEKTIPVGTRIEDQQLGII------WTKQALVSISANGFINFV HTSVVCCVKFSNDGEYLATGCNKTTQVYRVSDGSLVARLSDSSDLYIRSVCFSPDGKFLATGAEDRLIRIWDIENRKIVMI--LQGHEQDIYSLDYFPSGDKLVSGSGDR--TVRIWDLRTGQCSLTLS-IEDGVTTVAVSPGDGKY-IAAGSLDRAVRVWDSETGFLVERLDTGHKDSVYSVVFTRDGQSVVSGSLDRSVKLWNLT----------CEVTYIGHKDFVLSVATTQNDEYILSGSKDRGVLFWDKKSGNPLLMLQGHRN-SVISVAVANGSSLGPEYNVFATGSGDCKARIW
#d4pwza2 d2ymua2 3.1863 232 22 39 R:164 G:315 T:165 Q:316 R:166 T:317 I:167 I:318 A:168 A:319 Y:169 S:320 V:170 A:321 V:171 S:322 K:172 D:323 P:179 D:324 H:180 K           :325 E:181 T:326 L:182 V:327 R:183 K:328 V:184 W:330 S:185 N:331 D:186 R:332 Y:1           90 N:333 N:191 G:334 Q:192 Q:335 F:193 H:336 V:194 L:337 V:195 Q:338 H:196 T:339 R:197 L:340 S:198 T:341 E:200 S:345 P:201 S:346 L:202 V:347 M:203 W:348 S:204 G           :349 P:205 V:350 A:206 A:351 W:207 F:352 S:208 S:353 P:209 P:354 D:210 D:355 G:211 G:356 S:212 Q:357 K:213 T:358

def convert offsetL, strL, offsetR, strR
	raise 'wrong sz' unless strL.size == strR.size
	buf = []

	i = 0
	j = 0
	strL.zip(strR).each { |p|
		buf << "#{p[0]}:#{offsetL+i} #{p[1]}:#{offsetR+j}"
		i += 1 unless p[0] == '-'
		j += 1 unless p[1] == '-'
	}
	buf.join ' '
end

def processOne fname
  any_open_b(fname) { |f| f.each { |l|
	ar = l.split
	left = (ARGV[1] || 11).to_i
	iL = (ARGV[2] || 7).to_i
	pairs = convert ar[iL].to_i, ar[left].split(''), ar[iL+2].to_i, ar[left+1].split('')
	puts "#{ar[0]},#{ar[1]},#{pairs}"
	#puts "#{ar[0...left].join ' '} #{ar[left+2..-1].join ' '} #{pairs}"
      }
  }
end

Dir[ARGV[0]].each { |f| processOne f }
