kernel_path =  (File.expand_path(__FILE__).split("/"))[0..-2].join("/") + '/'
require kernel_path+'astrals'
require kernel_path+'blosum'

#//section rotations:
#        Rx         Ry         Rz           T
#     -0.401      0.907     -0.132      -17.833
#     -0.583     -0.142      0.800       -7.490
#      0.706      0.397      0.586      -46.203

#section scores:
#   quality Q:  0.0142  (normalised to [0...1])
#     r.m.s.d:  3.6604  (A)
#      Nalign:  79      (residues)

#section alignment:
#|    Query    | Dist.(A) |   Target    |
#|-------------+----------+-------------|
#|S- A:ILE 363 |          |             |
#|S- A:VAL 364 |          | - A:ALA   4 |
#| - A:VAL 396 | . 2.29.. | + A:PRO  35 |


#Markers: 
#$TEXT:Alignment results: //then query and target 
#Rx         Ry         Rz           T //then 3 rows
#quality Q: //1 row
#r.m.s.d: //1 row
#Nalign: //1 row
#|-------------+----------+-------------| //then large table until:
 #`-------------'----------'-------------' //end of table

def skipToContaining(pattern,lines, start)
	#puts "s: #{start} ls: #{lines.size}"
	(start...lines.size).each { |i| return i if lines[i].include? pattern }
	return lines.size-1
end

def get_prefix data
	$astrals[data[5..7]] || 'X'
end

def cvtColumn data
#examples:
#|S- A:ILE 363 |          |             |
#|S- A:VAL 364 |          | - A:ALA   4 |
#| - A:VAL 396 | . 2.29.. | + A:PRO  35 |

#|0123456789abc|
#| -   MET2001 |
#|S- B:GLY1528 |
#|H.   THR2014 |
	index = data[8..-1].strip
	return nil if index.empty?
	"#{get_prefix data}:#{index}"
end

STARTSTR = '$TEXT:Alignment results:'
QUALITYQ = 'quality Q:'
RMSDSTR = 'r.m.s.d:'
NALIGN = 'Nalign:'
STARTTABLSTR = '|-------------+----------+-------------|'
ENDTABLSTR = "`-------------'----------'-------------'"
RESILIMIT = 35
RMSDLIMIT = 4.5
$b = Blosum.new(getRawBlosumNCBIStyle)

def parseGesamtPair lines, out=STDOUT
	lines << 'elephant in cairo'
	index = 0
	rmsd = 0
	while index<lines.size-1 do #every iteration treats a single pair
		similarity = 0
		index = 1+skipToContaining(STARTSTR,lines,index)
		break if index>=lines.size-1
		query = lines[index].split('/')[-1].split('.ent')[0].chomp
		target = lines[index+1].split('/')[-1].split('.ent')[0].chomp
		index = skipToContaining(QUALITYQ,lines,index+1)
		quality = (lines[index].split(':')[-1]).split()[0]
		index += 1 # skipToContaining(RMSDSTR,lines,index+2)
		rmsd = (lines[index].split(':')[-1]).split()[0]
		index += 1
		nalign = ((lines[index].split(':')[-1]).split()[0]).to_i
		next if nalign<RESILIMIT or rmsd.to_f>RMSDLIMIT
		index = 1 + skipToContaining(STARTTABLSTR,lines,index)
		aln = []
		#work on the alignment table
		last_match = nil
		while(not lines[index].include? ENDTABLSTR) do
			a = lines[index].split('|')
			left_a,right_a = cvtColumn(a[1]),cvtColumn(a[3])
			is_match = a[2].strip.size > 0
			last_match = index if is_match and nil == last_match
			if is_match #aa	
				aln << "#{left_a} #{right_a}" 
				similarity += $b[get_prefix(a[1]), get_prefix(a[3])] < 0 ? 0 : 1
				last_match = aln.size
			elsif last_match != nil
				if left_a == nil and right_a == nil #--
					raise 'Err' 
				elsif  left_a and right_a #aa
					aln << left_a << "-:-" << "-:-" << right_a
				else
					aln << (left_a || "-:-") << (right_a || "-:-")
				end
			end
			index+=1
		end
		aln.slice!(last_match..-1)
		logMsg = 		([query,target,rmsd,(similarity.to_f / nalign.to_f).round(3),aln].join ' ')
		# : ('ERR ' + query+target)
		#	aln.size == nalign ? 
		#throw 'mismatch in aln.size ' +ARGV[0] unless aln.size == nalign 
		out.puts logMsg
	end
end

BIN_PATH = '/shared/home/snepomny/gesamt_1.2/bin/gesamt'

def runPairs pairs
        pairs.each { |p|
                cmd = "#{BIN_PATH} #{p[0].chomp} #{p[1].chomp}"
                gesamtRaw = %x[#{cmd}].split("\n")
                parseGesamtPair gesamtRaw
        }
end

#def runASlice singlesListFileName, sliceNumber, rootPath=ENT_PATH
#	#read domain names
#	lines = File.readlines(singlesListFileName)
#	en = (0...lines.size).to_a.combination(2) #prepare all pairs
#	pairs = []
#	en.each_slice(TOTAL_ELEMENTS).each_with_index { |v,i| pairs=v if i==sliceNumber } #slice out your pairs
#	return if pairs.empty?
#	
#	runPairs pairs,rootPath
#end

def runAllVsOne one,allBlob
        #read domain names
        pairs = Dir[allBlob].map { |f| [one,f] }
	runPairs pairs
end
#parseGesamtPair ARGV[0]
#runASlice (ARGV[1] || SN_LIST), (ARGV[0].to_i || 0)
parseGesamtPair File.readlines(ARGV[0])
#runAllVsOne ARGV[0],ARGV[1]
