# README #

## All sorts of scripts for protein papers (some deprecated). ##

Latest pipeline works with *hhm* and *pdb* files pair.    
For each pair we perform the following preprocessing:  
* Extract atoms data from pdb: {x,y,z}, name, index. This is done using `pdb_get_xyz_fasta.rb`   
* Align the above data to residues of the hhm profile. Using `hhm_get_aln.rb`. Succeeds in most cases. Produces .aln files.    

We run hhsearch of every hhm file against a database (usually, just concatenation of all hhms).  
`hhsearch -i $1 -d hhmDomainsDB -b 30000 -nocons -nopred -nodssp`

Post-processing:  
* We use `hhr_flatten.rb` on the result files to produce flat stats files.  
* Finally, we run `flat_hhm_tmscore` on the flat files to get the scores of structural similarity.  

For updates — (adding new and altering exisiting files) simply rerun all steps on the relevant hhms against an up to date db.  


To get TMscore:
Download [source](http://zhanglab.ccmb.med.umich.edu/TM-score/TMscore.f)  
and run: `g77 -static -O3 -lm -o TMscore TMscore.f`

### Lookup file:

id pdb hhm
for pdb we also expect to have pdb.xyzni
for hhm we expect to have hhm.aln`

### Barrels

We use HHSearch [*] to find significant alignments of β-barrels
with proteins from the set of 31,417 70% non-redundant PDB chains (September 2014).
The HMM profiles for the PDBs were pre-computed by the Soding group (and downloaded from http://wwwuser.gwdg.de/~compbiol/data/hhsuite/databases/hhsearch_dbs/).
The search yields 311,219 alignments with length > 20 and E-value < 500.

For each aligned pair we calculate the structural similarity
using Root Mean Square Deviation (RMSD) of the optimally superimposed Cα atoms of the aligned residues.
When calculating RMSD, we exclude the residues for which atom coordinates are not found in the PDB file,
as well as residues against a gap. We report percent identity and similarity
calculated with BLOSUM62 for the relevant residues alongside with the alignment metrics reported by HHSearch:
E-value, length, percent identity and similarity.  

We visualize the beta-barrels and their alignments as a network using Cytoscape [*] and CyToStruct [*]:
the nodes are the chains, colored by their classification, and edges connect pairs of chains that we aligned.  
By clicking on the node, one can see the structure of the chain within the molecular viewer (pyMOL),
by clicking on the edge, one can see the structures of the two aligned chains, superimposed based on the alignment,
and with the aligned residues highlighted.  

[*] Soding J (2005) Protein homology detection by HMM-HMM comparison. Bioinformatics 21(7):951-960.
[*] Saito, R., Smoot, M.E., Ono, K., Ruscheinski, J., Wang, P.-L., Lotia, S., Pico, A.R., Bader, G.D., and Ideker, T. A travel guide to Cytoscape plugins. Nat. Methods. 2012; 9: 1069–1076
[*] Nepomnyachiy S, Ben-Tal N, & Kolodny R (2015) CyToStruct: augmenting the network visualization of Cytoscape with the power of molecular viewers. Structure 23(5):941-948.
