This folder contains a preconfigured Cytoscape session: pisa.cys.
Make sure that Cytostruct plug-in is installed in your Cytoscape.

Load the session and import the network:
In the menu select: File->Import->Network->File 
Choose 'network.txt'

In the import wizard select:
Column 2 as source interatcion
Column 1 as interaction type
Column 3 as target interatcion

You may also import the area (column 4) by clicking in the header of the import table.

