#!/bin/sh

#obtain-lock

#transform network file to sif file
cat network.txt | grep -v 'edgeid chain1' | awk -F' ' '{print $2,$1,$3}' > network.sif
#tell cytoscape to delete the current session and load a new sif
curl -X DELETE localhost:1234/v1/session
curl -X DELETE localhost:1234/v1/networks
python /home/wwwdata/restJson.py #will also bring a new session in as save.cys

mv /tmp/save.cys ./
unzip save.cys
rm -f save.cys
#inject cytostruct 
cp -r /home/wwwdata/R4M_DATA_PATH Cyto*/apps/
zip -o session.zip -r Cyto*
mv session.zip $1.cys
rm -rf Cyto*

#release-lock
