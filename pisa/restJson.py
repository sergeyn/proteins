import requests
import json
import os

PORT_NUMBER = 1234
BASE = 'http://localhost:' + str(PORT_NUMBER) + '/v1/'
HEADERS = {'Content-Type': 'application/json'}

# Small utility function to create networks from list of URLs
def create_from_list(network_list):
    server_res = requests.post(BASE + 'networks?source=url&collection=ppi', data=json.dumps(network_list), headers=HEADERS)
    return json.loads(server_res.content)

def store_file(file):
    server_res = requests.get(BASE + 'session?file='+file, headers=HEADERS)
    return json.loads(server_res.content)

def get_net_id():
    server_res = requests.get(BASE + 'networks', headers=HEADERS)
    return json.loads(server_res.content)[0]

def apply_layout():
    server_res = requests.get(BASE + 'apply/layouts/force-directed/' + str(get_net_id()) )
    return json.loads(server_res.content)

 
filepath = os.path.abspath('network.sif')
network_files = [ 'file://' + filepath ]
print(json.dumps(create_from_list(network_files), indent=4))
print(json.dumps(apply_layout() ))
print(json.dumps(store_file('/tmp/save.cys')))
