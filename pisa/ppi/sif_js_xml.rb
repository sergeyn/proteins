TEMPLATE_SESSION = '/tmp/CytoscapeSession'
# get the locations from a cyjs export: grep position locations.cyjs -A2  | grep -v posi | grep ':' | cut -d':' -f2
LOCATIONS = '/tmp/locations.txt'

#=======================================================================
def sif2js(sifArr)
	require 'set'
	nodes = Set.new()
	#source edge target
	edges = []
	sifArr.each do |t|
		edge = t.split
		nodes.add(edge[0])
		nodes.add(edge[2])
		edges << "{ \"data\": { \"id\": \"#{edge[1]}\", \"source\": \"#{edge[0]}\", \"target\": \"#{edge[2]}\" } }"
	end
	nodesS = nodes.map { |n| "{ \"data\": { \"id\": \"#{n}\" } }" }
	return [ "{ \"nodes\": [\n", nodesS.join(",\n"), "],\n\"edges\": [\n", edges.join(",\n"), "]\n}" ].join
end	
#=======================================================================
def getPositions()
	ret = []
	#(1..10).each { |x|
	#	(1..10).each { |y| 
	#		ret << [(5-x)*40,(5-y)*40]
	#		}
	#}
	data = IO.readlines(LOCATIONS).map { |v| v.to_f }
	data.each_slice(2).to_a
end
#=======================================================================
def sif2xgmml(sifArr)
#network node:
#<node id="969" label="J"/>	  
#network edge:
#<edge id="979" label="J (158) t" source="969" target="902" cy:directed="1"/>
#view node:
#<node id="1031" label="W" cy:nodeId="831"><graphics y="350.4068915446599" x="171.5265907794237" z="0.0"/></node>
# table nodes:
#"id","label","false"
# table edges:
#"id","edge-id","source (edge-id) target","false"
    nid = 3
	nodes = {}
	posArr = getPositions().shuffle
	#source edge target
	edges = []
	edgesT = []
	sifArr.each do |t|
		edge = t.split
		unless nodes[edge[0]]
			nodes[edge[0]] = nid
			nid +=1
		end
		unless nodes[edge[2]]
			nodes[edge[2]] = nid
			nid +=1
		end		
		edges << "<edge id=\"#{nid}\" label=\"#{edge[0]} (#{edge[1]}) #{edge[2]}\" source=\"#{nodes[edge[0]]}\" target=\"#{nodes[edge[2]]}\" cy:directed=\"1\"/>"
		edgesT << "\"#{nid}\",\"#{edge[1]}\",\"#{edge[0]} (#{edge[1]}) #{edge[2]}\",\"false\""
		nid += 1
	end
	nodesN = nodes.keys.map { |node| "<node id=\"#{nodes[node]}\" label=\"#{node}\"/>" }
	nodesV = []
	nodesT = []
	nodes.keys.each do |node| 
		nid += 1
		pos = posArr[nid % posArr.size()]
		nodesV << "<node id=\"#{nid}\" label=\"#{node}\" cy:nodeId=\"#{nodes[node]}\"><graphics y=\"#{pos[1]}\" x=\"#{pos[0]}\" z=\"0.0\"/></node>" 
		nodesT << "\"#{nodes[node]}\",\"#{node}\",\"false\""
	end	
	return nodesN.join("\n"), edges.join("\n"), nodesV.join("\n"), nodesT.join("\n"), edgesT.join("\n")
end	
#=======================================================================
def fixDefaultFiles(destDir, sourceSif)
#default files:
#-------------
#networks/796-ppi.xgmml -- network = nodes+edges
#views/812-988-network.sif.xgmml = nodes with location
#tables/812-network.sif/LOCAL_ATTRS-org.cytoscape.model.CyNode-network.sif+default+node.cytable
#tables/812-network.sif/LOCAL_ATTRS-org.cytoscape.model.CyEdge-network.sif+default+edge.cytable

	sffx = "</graph>"
	netXgmmlSuffix = "   #{sffx}\n  </att>\n#{sffx}"

	nodesN, edges, nodesV, nodesT, edgesT = sif2xgmml(IO.readlines(sourceSif))
	
	f = File.new("#{destDir}/networks/796-ppi.xgmml",'a')
	f.puts nodesN
	f.puts edges
	f.puts netXgmmlSuffix
	f.close
	
	f = File.new("#{destDir}/views/812-988-network.sif.xgmml",'a')
	f.puts nodesV
	f.puts sffx
	f.close	
	
	f = File.new("#{destDir}/tables/812-network.sif/LOCAL_ATTRS-org.cytoscape.model.CyNode-network.sif+default+node.cytable",'a')
	f.puts nodesT
	f.close		

	f = File.new("#{destDir}/tables/812-network.sif/LOCAL_ATTRS-org.cytoscape.model.CyEdge-network.sif+default+edge.cytable",'a')
	f.puts edgesT
	f.close		
	
end
#=======================================================================
require 'fileutils'
def sif2session sourceSif, templateDirPath, destName
	tempDir = "/tmp/#{destName}_session"
	FileUtils.rm_rf tempDir
	FileUtils.mkdir tempDir
	FileUtils.cp_r templateDirPath+'/.', tempDir
	
	fixDefaultFiles(tempDir,sourceSif)
	%x[cd /tmp/ && zip -o #{destName}.zip -r #{destName}_session && mv /tmp/#{destName}.zip  /tmp/#{destName}.cys]
	FileUtils.rm_rf tempDir
end
#=======================================================================
 
if ARGV.size == 1
	puts sif2js(IO.readlines(ARGV[0]))
elsif ARGV.size == 2	
	sif2session ARGV[0], TEMPLATE_SESSION, ARGV[1]
else
	puts "Usage:\n1. sif2js -- path to network.sif file\n2. sif2cys -- path to network.sif and pdbID"
end
