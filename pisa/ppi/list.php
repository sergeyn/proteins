<?php
function humanFileSize($size,$unit="") {
  if( (!$unit && $size >= 1<<30) || $unit == "GB")
    return number_format($size/(1<<30),0)." GB";
  if( (!$unit && $size >= 1<<20) || $unit == "MB")
    return number_format($size/(1<<20),0)." MB";
  if( (!$unit && $size >= 1<<10) || $unit == "KB")
    return number_format($size/(1<<10),0)." KB";
  return ceil(number_format($size))." bytes";
}

$target_dir = "/home/wwwdata/";
echo "<html><body><table border=0 cellspacing=10> ";
echo "<font face='monospace'>";
foreach (glob("$target_dir/*.pdb") as $filename) {
    $name = basename($filename,".pdb");
    $sz = humanFileSize(filesize($filename));
    echo "<tr><td align='left'><b>$name</b></td><td align='right'>$sz</td></tr>\n";
}

echo "</font></table></body></html>"
?>
