<?php

function send_zip_file( $file )
{
    if ( !is_file( $file ) )
    {
        header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
        //require_once("404.php");
        exit;
    }
    elseif ( !is_readable( $file ) )
    {
        header($_SERVER['SERVER_PROTOCOL'].' 403 Forbidden');
        //require_once("403.php");
        exit;
    }
    else
    {
	header($_SERVER['SERVER_PROTOCOL'].' 200 OK');
        header("Pragma: public");
        header("Expires: 0");
        header("Accept-Ranges: bytes");
        header("Connection: keep-alive");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public");
        header("Content-type: application/zip");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=\"".basename($file)."\"");
	header('Content-Length: '.filesize($file));
        header("Content-Transfer-Encoding: binary");
	ob_clean(); //Fix to solve "corrupted compressed file" error!
        readfile($file);
    }
}
	$name = $_GET["name"];
	$name = strtoupper(preg_replace('/[^a-zA-Z0-9-_\.]/','',$name)); //sanitize
        $attachment_location = "/tmp/$name.zip";
	send_zip_file($attachment_location);
	exit;
?>
